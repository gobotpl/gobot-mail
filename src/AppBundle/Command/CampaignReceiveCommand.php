<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\Message;
use AppBundle\Entity\Keyword;
use AppBundle\Entity\KeywordMail;
use AppBundle\Entity\MessageReply;
use AppBundle\Entity\ClientSettings;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\RetriverDataService;

//
// CRON TASK:
//
// * * * * * php /Users/michal/www/gobot-mail/bin/console app:campaign_receive
// * * * * * php /var/www/panel.gobot.pl/bin/console app:campaign_receive
//

class CampaignReceiveCommand extends ContainerAwareCommand {

	/**
	 * @var EntityManager
	 */
	private $entityManager;

	public function __construct(EntityManagerInterface $entityManager, RetriverDataService $retriverData){
		parent::__construct();
		$this->entityManager = $entityManager;
		$this->retriverData = $retriverData;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure(){
		$this
			->setName("app:campaign_receive")
			->setDescription("Polecenie do sprawdzenia i odbioru dostępynch wiadomości");
	}

	public function execute(InputInterface $input, OutputInterface $output){
		// find all Clients
		$query = $this->entityManager
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.roles NOT LIKE :notRole'
            )->setParameter('role', '%"ROLE_CLIENT"%')
			->setParameter('notRole', '%"ROLE_ADMIN"%');
		$clients = $query->getResult();

		foreach ($clients as $client) {
			// get bot settings from client
			$botSettings = $this->entityManager->getRepository(ClientSettings::class)->findOneBy(["client" => $client->getId()]);

			// check bot settings is not empty
			if(!empty($botSettings->getBotEmail()) && !empty($botSettings->getBotPassword()) && !empty($botSettings->getBotImapPort()) && !empty($botSettings->getBotServer()) && !empty($botSettings->getBotImapEncryption())){
				/* try to connect with Inbox */
				$inbox = imap_open("{".$botSettings->getBotServer().":".$botSettings->getBotImapPort()."/imap/".$botSettings->getBotImapEncryption()."}INBOX", $botSettings->getBotEmail(), $botSettings->getBotPassword());
				if(!$inbox){
					print_r('Cannot connect to mail: ' . imap_last_error());
				} else {
					// if connection is ready and Bot Settings are correct get all Campaigns
					// $campaigns = $this->entityManager->getRepository(Campaign::class)->findBy(["client" => $client->getId(), "type" => Campaign::TYPE_DELETED]);
					$query = $this->entityManager
			            ->createQuery(
			                'SELECT u FROM AppBundle:Campaign u WHERE u.client = :client AND u.type != :type'
			            )->setParameter('client', $client->getId())
						->setParameter('type', Campaign::TYPE_DELETED);
					$campaigns = $query->getResult();

					// each campaign things ..
					foreach ($campaigns as $campaign) {
						// get email which in topic has name of campaign

						// variable to set sent error for only One send, not multiple with the same
						$sentError = 0;

						// $emails = imap_search($inbox, 'SUBJECT "Re: '.$campaign->getName().'"');
						// $emails = imap_search($inbox, 'SUBJECT "Re: '.$campaign->getName().'"');
						// $emails = imap_search($inbox, 'SUBJECT "Re: '.$campaign->getName().'"');
						$emails = imap_search($inbox, 'SUBJECT "'.$campaign->getName().'"');
						// $emails = imap_search($inbox, 'SUBJECT "Re: '.$campaign->getName().'"', SE_UID);
						// $emails = imap_search($inbox,'ALL');
						// if emails are exists - show array
						if($emails) {
							/* begin output var */
							/* put the newest emails on top */
							rsort($emails);
							/* for every email... */
							foreach($emails as $email_number) {
								/* get information specific to this email */
								$overview = imap_fetch_overview($inbox,$email_number,0);
								$message = imap_fetchbody($inbox,$email_number,1);

								// attachments
								$structure = imap_fetchstructure($inbox,$email_number);

								$attachments = array();
								if(isset($structure->parts) && count($structure->parts)) {
									for($i = 0; $i < count($structure->parts); $i++) {

								   		$partsVal = 0;
										if(isset($structure->parts[$i]->parts)){
											foreach($structure->parts[$i]->parts as $part){
												$attachments[$partsVal] = array(
												   'is_attachment' => false,
												   'filename' => '',
												   'name' => '',
												   'attachment' => '');


												if($part->ifdparameters) {
													foreach($part->dparameters as $object) {
														if(strtolower($object->attribute) == 'filename') {
															$attachments[$partsVal]['is_attachment'] = true;
															$attachments[$partsVal]['filename'] = $object->value;
														}
													}
												}

												if($part->ifparameters) {
													foreach($part->parameters as $object) {
														if(strtolower($object->attribute) == 'name') {
															$attachments[$partsVal]['is_attachment'] = true;
															$attachments[$partsVal]['name'] = $object->value;
														}
													}
												}

												if($attachments[$partsVal]['is_attachment']) {
													$attachments[$partsVal]['attachment'] = imap_fetchbody($inbox, $email_number, 2.2);
													if($part->encoding == 3) { // 3 = BASE64
														$attachments[$partsVal]['attachment'] = base64_decode($attachments[$partsVal]['attachment']);
													} elseif($part->encoding == 4) { // 4 = QUOTED-PRINTABLE
														$attachments[$partsVal]['attachment'] = quoted_printable_decode($attachments[$partsVal]['attachment']);
													}
												}

												$partsVal++;
											}
										}
									}
								}

								if(count($attachments)!=0){
									$numOfAttachement = 0;
									foreach($attachments as $at){
										if($at['is_attachment'] == 1){
											// print_r($at['filename']);

											$filename = $at['name'];
											$ext = explode(".", $filename);
											$ext = $ext[count($ext) - 1];

											$allowedExt = ['jpg', 'png', 'pdf', 'doc', 'docx'];
											if (in_array($ext, $allowedExt)) {
												// if extension of file is allowed do some stuff below!
												// print_r($ext);
												// print_r($at['filename']);

								                if(empty($filename)) $filename = $at['filename'];

								                if(empty($filename)) $filename = time() . ".dat";
								                $folder = "web/attachment";
								                if(!is_dir($folder))
								                {
								                     mkdir($folder);
								                }
												// file_put_contents("./". $folder ."/". $email_number . "-" . $filename, $at['attachment']);
								                // $fp = fopen("./". $folder ."/". $email_number . "-" . $filename, "w+");
								                $fp = fopen("./". $folder ."/". $email_number . "-" . $numOfAttachement .".". $ext, "w+");
								                fwrite($fp, $at['attachment']);
								                fclose($fp);
											}
										}
										$numOfAttachement++;
									}
								}





								// PLUS checker for reply messages, if it has e.g "Re: " or "ODP: " - system catch this
								$topicMail = explode(": ", $overview[0]->subject);
								if(count($topicMail) > 1){
									$topicMail = $topicMail[1];


									// check received message for subject is equels to ALL Campaign Name not WORD
									// if(strtolower(str_replace("Re: ", "", $overview[0]->subject)) == strtolower($campaign->getName())){
									if(strtolower($topicMail) == strtolower($campaign->getName())) {

										// check email is exists now in panel!
										$email_in_panel = $this->entityManager->getRepository(Message::class)->findOneBy(["messageId" => $overview[0]->message_id]);
										if(count($email_in_panel) <= 0){
											// clean HTML to show clean message in panel
											$html_clean = $this->decode_qprint($message);
											$html_clean = preg_replace('/(^\w.+:\n)?(^>.*(\n|$))+/mi', '', $html_clean);

											// create email date format
											$email_date = new \DateTime($overview[0]->date);
											$email_date = $email_date->format('Y-m-d H:i:s');

											// prepare mail address from
											preg_match_all('/<[^}]*>/', $this->decode_header($overview[0]->from), $matches);
											if(count($matches[0]) > 0){
												$from_mail = str_replace("<", "", $matches[0][0]);
												$from_mail = str_replace(">", "", $from_mail);
											} else {
												$from_mail = $this->decode_header($overview[0]->from);
											}

											// find message sent to this Lead by Email and campaign
											$message_in_db = $this->entityManager->getRepository(Message::class)->findOneBy(["campaign" => $campaign->getId(), "messageTo" => $from_mail]);
											// if found sent message in panel add received - if not.. set this message to other folder
											if(count($message_in_db) > 0){

												// create new var for Assigned Agent to message
												$assignedAgent = "";
												$nullAgentAssigned = 0;

												// find received messages from this Lead email to this Client
												$receivedMessagesLead = $this->entityManager->getRepository(Message::class)->findBy(["campaign" => $campaign->getId(), "messageFrom" => $from_mail]);
												// if is more than 0 set next messages to the same Agent
												if(count($receivedMessagesLead) > 0){
													$assignedAgent = $receivedMessagesLead[0]->getAssignedAgent();
												} else {

													// get campaign by ID for agents list
													$campaignAgents = $this->entityManager->getRepository(Campaign::class)->findOneBy(array("id" => $campaign->getId()));
													// get agents from campaign join table
													$campaignAgents = $campaignAgents->getAgent();
													$clientAgents = array();

													// check agent is OFF from receive messages - if no, add him to list
													foreach ($campaignAgents as $key => $agent) {
														$offUser = $this->entityManager->getRepository(User::class)->findOneBy(["id" => $agent->getId(), "typeUser" => User::TYPE_USER_AGENT]);
														if($offUser->getOffUser() == User::OFF_USER_NO){
															$clientAgents[] = $agent;
														}
													}

													// check agents are only 1 or more
													$howAgent = count($clientAgents);
													if($howAgent > 1){

													// get Agents for Client
													// $clientAgents = $this->entityManager->getRepository(User::class)->findBy(["client" => $client->getId(), "typeUser" => User::TYPE_USER_AGENT, "offUser" => USER::OFF_USER_NO]);
													// // check agents are only 1 or more
													// $howAgent = count($clientAgents);
													// if($howAgent > 1){

														// if last Assigned Agent field is not empty
														if(!empty($botSettings->getLastAssignedAgent())){
															// for all agents check which was assigned last
															foreach ($clientAgents as $key => $agent) {
																// if this agent was last
																if($botSettings->getLastAssignedAgent() == $agent){
																	// check which is this agent in list of agents
																	if($key == $howAgent-1){
																		// if this agent was last - assign first agent
																		$assignedAgent = $clientAgents[0];
																	} else {
																		// if was first or in middle of bigger list assign next
																		$assignedAgent = $clientAgents[$key+1];
																	}
																}
															}
															// if actuall agents are not in list of last assigned set first
															if(empty($assignedAgent)){
																$assignedAgent = $clientAgents[0];
															}
														} else {
															// set first agent to this message
															$assignedAgent = $clientAgents[0];
														}

													} elseif($howAgent == 1){
														// set this agent to this message
														$assignedAgent = $clientAgents[0];
													} else {
														// send error to Client and stop receive this message
														$nullAgentAssigned = 1;
													}
												}
												// if Agents are 0, send error!
												if($nullAgentAssigned == 0){
													// save to database last Assigned Agent for client
													$botSettings->setLastAssignedAgent($assignedAgent);
													$this->entityManager->persist($botSettings);
													// clean database
													$this->entityManager->flush();



													// verify message for keywords and check posibility to set auto-reply ___ ONLY FOR REPLY, NOT OTHERS
													$mainKeywords = $this->entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => null, "campaign" => $campaign->getId(), "client" => $client->getId()));
													$keyID = 0;
													$keyMailID = 0;
													$keyCount = 0;
													foreach ($mainKeywords as $key => $keyword) {
														// if MainKeyword is in mail text - check subKeywords for check which is better to reply
														if (strpos(strtolower($html_clean), strtolower($keyword->getName())) !== false) {
															// count strike in mails for this mainKeyword
															$strike = 0;

															// get all mails for this keyword
															$mailsKeyword = $this->entityManager->getRepository(KeywordMail::class)->findBy(array("keyword" => $keyword->getId()));
															foreach ($mailsKeyword as $key => $mailKeyword) {

																// get keywords set to this mail
																foreach ($mailKeyword->getKeywords() as $key => $mailKey) {
																	// check mailKeyword - count keywords strike
																	if (strpos(strtolower($html_clean), strtolower($mailKey->getName())) !== false) {
																		$strike++;
																	}
																}
																// if bigger strike in this keyword set this to auto-reply
																if($strike > $keyCount){
																	$keyCount = $strike;
																	$keyID = $keyword->getId();
																	$keyMailID = $mailKeyword->getId();
																}
																$strike = 0;
															}
														}
													}

													// check auto-reply is prepare for this mail
													if($keyMailID > 0 && $keyID > 0){
														// check messageReply is exists for this MessageGroup
														$messageReply = $this->entityManager->getRepository(MessageReply::class)->findOneBy(array("messagesGroup" => $message_in_db->getMessagesGroup()));
														// if yes.. if not - create obejct of MessageReply
														if(count($messageReply) > 0){

															$messageReply->setKeywordID($keyID);
															$messageReply->setKeyMailID($keyMailID);
															$messageReply->setDraftReady("1");
															// save to database
															$this->entityManager->persist($messageReply);
															// clean database
															$this->entityManager->flush();

														} else {
															$messageReplyNew = new MessageReply();
															// set fields in messageReply
															$messageReplyNew->setMessagesGroup($message_in_db->getMessagesGroup());
															$messageReplyNew->setKeywordID($keyID);
															$messageReplyNew->setKeyMailID($keyMailID);
															$messageReplyNew->setDraftReady("1");
															// save to database
															$this->entityManager->persist($messageReplyNew);
															// clean database
															$this->entityManager->flush();
														}
													}



													// create new message in panel
													$message_db = new Message();
													// set fields in message
													$message_db->setMessageId($overview[0]->message_id);
													$message_db->setCampaign($campaign);
													$message_db->setAssignedAgent($assignedAgent);
													$message_db->setMessagesGroup($message_in_db->getMessagesGroup());
													// $message_db->setMessageText($html->getText());
													$message_db->setMessageText($html_clean);
													$message_db->setMessageDate(new \DateTime($email_date));
													$message_db->setMessageFrom($from_mail);
													$message_db->setMessageTo($botSettings->getBotEmail());
													$message_db->setInboxType(Message::INBOX_TYPE_INBOX);
													$message_db->setStatus(Message::STATUS_RECEIVED);

													// check for number exists in received message
													if(count($this->retriverData->checkPhoneNumberIn($html_clean)) > 0){
														$message_db->setNumberIn(True);
													}

													// save to database
													$this->entityManager->persist($message_db);
													// clean database
													$this->entityManager->flush();
												} else {
													// check error Raport was send -if not send this
													if($sentError == 0){
														// send error message to Client
														$this->sendRaport($client->getEmail(), "Uzupełnij Agentów dla Kampanii ".$campaign->getName().", aby odbierać wiadomości!");
														$sentError = 1;
													}
												}

											} else {
												// add to others messages
												// create new message in panel
												$message_db = new Message();
												// set fields in message
												$message_db->setMessageId($overview[0]->message_id);
												$message_db->setCampaign($campaign);
												$message_db->setMessagesGroup(md5(uniqid()));
												// $message_db->setMessageText($html->getText());
												$message_db->setMessageText($html_clean);
												$message_db->setMessageDate(new \DateTime());
												$message_db->setMessageFrom($from_mail);
												$message_db->setMessageTo($botSettings->getBotEmail());
												$message_db->setInboxType(Message::INBOX_TYPE_OTHER);
												$message_db->setStatus(Message::STATUS_RECEIVED);

												// check for number exists in received message
												if(count($this->retriverData->checkPhoneNumberIn($html_clean)) > 0){
													$message_db->setNumberIn(True);
												}
												// save to database
												$this->entityManager->persist($message_db);
												// clean database
												$this->entityManager->flush();
											}


											// 		/*
											// 		subject - the messages subject
											// 		from - who sent it
											// 		to - recipient
											// 		date - when was it sent
											// 		message_id - Message-ID
											// 		references - is a reference to this message id
											// 		in_reply_to - is a reply to this message id
											// 		size - size in bytes
											// 		uid - UID the message has in the mailbox
											// 		msgno - message sequence number in the mailbox
											// 		recent - this message is flagged as recent
											// 		flagged - this message is flagged
											// 		answered - this message is flagged as answered
											// 		deleted - this message is flagged for deletion
											// 		seen - this message is flagged as already read
											// 		draft - this message is flagged as being a draft
											// 		 */
										}
									}
								}
							}
						}
					}

					/* close the connection */
					imap_close($inbox);
				}
			}
		}

		// clean database
		// $this->entityManager->flush();
		$output->writeln("Sprawdzono i odebrano odpowiednie wiadomości");
	}

	public function decode_qprint($str) {
	    return quoted_printable_decode($str);
	    // return iconv('ISO-8859-2', 'UTF-8', $str);
	}

	public function decode_header($str){
		return mb_decode_mimeheader($str);
		// return imap_utf8($str);
	}

	public function sendRaport($campaignClientMail, $text){
		// get from user - settings Admin
		$from = $this->getContainer()->getParameter('mailer_user');
		// get mailer
		$mailer = $this->getContainer()->get('mailer');
		// send message
		$message = \Swift_Message::newInstance()
			->setSubject('Raport z Kampanii')
	        ->setFrom($from)
	        ->setTo([$campaignClientMail, $from])
	        ->setBody($text);
		$mailer->send($message);
	}

}

?>
