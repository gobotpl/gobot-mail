<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\Message;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\ClientSettingsSMTP;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Doctrine\ORM\EntityManagerInterface;
// use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\RetriverDataService;

// use Psr\Log\LoggerInterface;
//
//
// CRON TASK:
//
// * * * * * php /Users/michal/www/gobot-mail/bin/console app:campaign_send
// * * * * * php /var/www/panel.gobot.pl/bin/console app:campaign_send
//

// class CampaignSendCommand extends Command {
class CampaignSendCommand extends ContainerAwareCommand {

	/**
	 * @var EntityManager
	 */
	private $entityManager;
	// private $logger;

	// public function __construct(EntityManagerInterface $entityManager, RetriverDataService $retriverData, LoggerInterface $logger){
	public function __construct(EntityManagerInterface $entityManager, RetriverDataService $retriverData){
		parent::__construct();
		$this->entityManager = $entityManager;
		$this->retriverData = $retriverData;
		// $this->logger = $logger;
	}

	/**
	 * {@inheritdoc}
	 */
	protected function configure(){
		$this
			->setName("app:campaign_send")
			->setDescription("Polecenie do sprawdzenia i wysyłki dostępynch wiadomości");
	}

	public function execute(InputInterface $input, OutputInterface $output){
		// find all active campaigns
		$active_campaigns = $this->entityManager->getRepository(Campaign::class)->findBy(["type" => Campaign::TYPE_ACTIVE]);


		// $this->logger->info("auaaaa");
		// $this->logger->error("auaaaa");

		// now Date
		$newTZ = new \DateTimeZone("Europe/Warsaw");
		$now_date = new \DateTime();
		$now_date->setTimezone( $newTZ );
		$now_date = $now_date->format('Y-m-d H:i');

		// find all campaign messages from this date (minute)
		foreach ($active_campaigns as $campaign) {
			// #mail 1
			if($campaign->getMailActive1() == 1){
				if($now_date == $campaign->getMailDate1()->format('Y-m-d H:i')){
					// run function to send message to Leads with conditions
					if($this->sendMessageToLeads($campaign, 1)){
						// turn off this message
						$campaign->setMailActive1(0);
						// say output about message sent
						$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Wysłano wiadomość 1 dla kampanii o nazwie %s", $campaign->getName()));
						$output->writeln(sprintf("Wysłano wiadomość 1 dla kampanii o nazwie <info>%s</info>", $campaign->getName()));
					}
				}
			}
			// #mail 2
			if($campaign->getMailActive2() == 1 && !empty($campaign->getMailDate2Full())){
				if($now_date == $campaign->getMailDate2Full()->format('Y-m-d H:i')){
					// run function to send message to Leads with conditions
					if($this->sendMessageToLeads($campaign, 2)){
						// turn off this message
						$campaign->setMailActive2(0);
						// say output about message sent
						$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Wysłano wiadomość 2 dla kampanii o nazwie %s", $campaign->getName()));
						$output->writeln(sprintf("Wysłano wiadomość 2 dla kampanii o nazwie <info>%s</info>", $campaign->getName()));
					}
				}
			}
			// #mail 3
			if($campaign->getMailActive3() == 1 && !empty($campaign->getMailDate3Full())){
				if($now_date == $campaign->getMailDate3Full()->format('Y-m-d H:i')){
					// run function to send message to Leads with conditions
					if($this->sendMessageToLeads($campaign, 3)){
						// turn off this message
						$campaign->setMailActive3(0);
						// say output about message sent
						$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Wysłano wiadomość 3 dla kampanii o nazwie %s", $campaign->getName()));
						$output->writeln(sprintf("Wysłano wiadomość 3 dla kampanii o nazwie <info>%s</info>", $campaign->getName()));
					}
				}
			}
			// #mail 4
			if($campaign->getMailActive4() == 1 && !empty($campaign->getMailDate4Full())){
				if($now_date == $campaign->getMailDate4Full()->format('Y-m-d H:i')){
					// run function to send message to Leads with conditions
					if($this->sendMessageToLeads($campaign, 4)){
						// turn off this message
						$campaign->setMailActive4(0);
						// say output about message sent
						$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Wysłano wiadomość 4 dla kampanii o nazwie %s", $campaign->getName()));
						$output->writeln(sprintf("Wysłano wiadomość 4 dla kampanii o nazwie <info>%s</info>", $campaign->getName()));
					}
				}
			}
			// #mail 5
			if($campaign->getMailActive5() == 1 && !empty($campaign->getMailDate5Full())){
				if($now_date == $campaign->getMailDate5Full()->format('Y-m-d H:i')){
					// run function to send message to Leads with conditions
					if($this->sendMessageToLeads($campaign, 5)){
						// turn off this message
						$campaign->setMailActive5(0);
						// say output about message sent
						$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Wysłano wiadomość 5 dla kampanii o nazwie %s", $campaign->getName()));
						$output->writeln(sprintf("Wysłano wiadomość 5 dla kampanii o nazwie <info>%s</info>", $campaign->getName()));
					}
				}
			}

			// add if all mails are sent type = campaign finish
			if($campaign->getMailActive1() == 0 && $campaign->getMailActive2() == 0 && $campaign->getMailActive3() == 0 && $campaign->getMailActive4() == 0 && $campaign->getMailActive5() == 0){
				$campaign->setType(Campaign::TYPE_FINISHED);
				$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Kampania o nazwie %s została zakończona", $campaign->getName()));
			}
			// save to database
			$this->entityManager->persist($campaign);
		}


        // show count of sent messages
		// $output->writeln(sprintf("Znaleziono <info>%d</info> transakcji do realizacji", count($pending_transactions)));

		// clean database
		$this->entityManager->flush();
		$output->writeln("Sprawdzono i wysłano odpowiednie wiadomości");
	}

	public function sendMessageToLeads(Campaign $campaign, $keyOfMail){
		// from campaign get Leads for conditions and run sending mails

		// set message text
		$message = "";
		if($keyOfMail == 1){
			$message = $campaign->getMailText1();
		} elseif($keyOfMail == 2){
			$message = $campaign->getMailText2();
		} elseif($keyOfMail == 3){
			$message = $campaign->getMailText3();
		} elseif($keyOfMail == 4){
			$message = $campaign->getMailText4();
		} else {
			$message = $campaign->getMailText5();
		}


			// Last assigned server
			$lastAssignedServer = NULL;

			$failedRecipients = [];
			/*********** GET LEADS DATA AND SEND ***********/

			// get Leads Data
			$leads = $this->retriverData->getCountLeads($campaign->getConditions(), $campaign->getLeadImport()->getId(), 1, false);
			// how many sent
			$sent = 0;
			foreach ($leads as $lead) {
				// get Lead data array
				$lead_data = $this->retriverData->getLeadData($lead);
				$message_this_lead = $message;

				// get all Data from message to $matches variable
				preg_match_all('/{{[^}]*}}/', $message_this_lead, $matches);
				// check message has fields with Lead data
				if(count($matches[0]) > 0) {
					// change message with Lead data and send
					// get matches and remove {{ }} from them
					foreach ($matches as $match_item) {
						$match_base = str_replace("{{", "", $match_item);
						$match_base = str_replace("}}", "", $match_base);
						// get all names of matches
						foreach ($match_base as $match) {
							// if match equels Lead Label name set this match to all message for this Lead
							foreach ($lead_data[0]['labels'] as $key => $value) {
								if($key == $match){
									$message_this_lead = str_replace("{{".$match."}}", $value, $message_this_lead);
								}
							}
						}
					}
				} else {
					// send normal message - not change message
					$message_this_lead = $message;
				}

				// if lead has email
				if(!empty($lead_data[0]['labels']['email'])){

					// if this Lead received for this campaign - not send next message
					$messageReceivedFromThisLead = $this->entityManager->getRepository(Message::class)->findBy(["campaign" => $campaign->getId(), "status" => Message::STATUS_RECEIVED, "messageFrom" => $lead_data[0]['labels']['email']]);
					if(count($messageReceivedFromThisLead) <= 0){

						/*********** GET AND CHECK BOT SETTINGS ***********/

						// get all bot settings servers from client
						// $botSettings = $this->entityManager->getRepository(ClientSettings::class)->findOneBy(["client" => $campaign->getClient()]);
						$botSettingsServers = $this->entityManager->getRepository(ClientSettingsSMTP::class)->findBy(["client" => $campaign->getClient()]);
						// check agents are only 1 or more
						$howServers = count($botSettingsServers);
						if($howServers > 1){
							// if last Assigned server field is not empty
							if(!empty($lastAssignedServer)){
								// for all servers check which was assigned last
								foreach ($botSettingsServers as $key => $server) {
									// if this server was last
									if($lastAssignedServer == $server){
										// check which is this server in list of servers
										if($key == $howServers-1){
											// if this server was last - assign first server
											$botSettings = $botSettingsServers[0];
										} else {
											// if was first or in middle of bigger list assign next
											$botSettings = $botSettingsServers[$key+1];
										}
									}
								}
								// if actuall servers are not in list of last assigned set first
								if(empty($botSettings)){
									$botSettings = $botSettingsServers[0];
								}
							} else {
								// set first server to this message
								$botSettings = $botSettingsServers[0];
							}

						} else {
							// set this server to this message
							$botSettings = $botSettingsServers[0];
						}
						// set last Assigned Server
						$lastAssignedServer = $botSettings;



						// check bot settings is not empty
						if(!empty($botSettings->getBotEmail()) && !empty($botSettings->getBotPassword()) && !empty($botSettings->getBotSmtpPort()) && !empty($botSettings->getBotServer()) && !empty($botSettings->getBotSmtpEncryption())){

							// set SwiftMailer to Bot Client Settings and send message
							// set SMTP Connection
							$transport = \Swift_SmtpTransport::newInstance($botSettings->getBotServer(), $botSettings->getBotSmtpPort(), $botSettings->getBotSmtpEncryption())
								->setUsername($botSettings->getBotEmail())
								->setPassword($botSettings->getBotPassword())
							;
							// create mailer from connection
							$mailer = \Swift_Mailer::newInstance($transport);


							// change text to HTML spaces
							$message_this_lead_html = trim(nl2br($message_this_lead, false));
							$footer_message = nl2br($campaign->getFooter(), false);

							// send message to Lead
							$mail_message = \Swift_Message::newInstance()
								->setSubject($campaign->getName())
						        ->setFrom($botSettings->getBotEmail())
						        ->setTo($lead_data[0]['labels']['email'])
						        ->setBody($message_this_lead_html."<br>".$footer_message, 'text/html');
						        // ->setBody($message_this_lead."<br>".$campaign->getFooter(), 'text/html');
							// $mailer->send($mail_message, $failedRecipients);

							try {
								// send mail
								$mailer->send($mail_message, $failedRecipients);

								// create new message in panel
								$message_db = new Message();
								// set fields in message
								$message_db->setCampaign($campaign);
								$message_db->setMessagesGroup(md5(uniqid()));
								$message_db->setMessageText($message_this_lead."\n\r".$campaign->getFooter());
								// $message_db->setMessageText($message_this_lead_html."<br>".$footer_message);
								$message_db->setMessageDate(new \DateTime());
								$message_db->setMessageFrom($botSettings->getBotEmail());
								$message_db->setMessageTo($lead_data[0]['labels']['email']);
								$message_db->setInboxType(Message::INBOX_TYPE_INBOX);
								$message_db->setStatus(Message::STATUS_SENT);
								// save to database
								$this->entityManager->persist($message_db);
								// clean database
								$this->entityManager->flush();
							}
							catch (\Swift_TransportException $e) {
								print_r("error - ".$e->getMessage());
								$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Błąd kampanii o nazwie %s - %s", $campaign->getName(), $e->getMessage()));
							}

						} else {
							print_r("error - empty bot settings");
							$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Błąd kampanii o nazwie %s - ustawienia bota są błędnie ustawione", $campaign->getName()));
						}
					}

				} else {
					print_r("error - mail field");
					$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Błąd kampanii o nazwie %s - wykryto możliwe puste pola w adresach email", $campaign->getName()));
				}

				$sent++;
			}

			if(count($failedRecipients) > 0){
				print_r("info - failed:");
				print_r($failedRecipients);

				// $list_of_failed_email = "";
				// foreach ($failedRecipients as $key => $value) {
				// 	$list_of_failed_email .= $value.", ";
				// }
				$this->sendRaport($campaign->getClient()->getEmail(), sprintf("Błąd kampanii o nazwie %s - nie dostarczono maili do kilku adresów", $campaign->getName()));
			}
			if($sent == count($leads)){

				// if sent email equels with leads - check next message is active, if yes - set time to next message
				$isNextActive = 0;
				$nextDays = 0;
				$nextHours = 0;
				if($keyOfMail == 1){
					$isNextActive = $campaign->getMailActive2();
					$nextDays = $campaign->getMailDate2Days();
					$nextHours = $campaign->getMailDate2Hours();
				} elseif($keyOfMail == 2){
					$isNextActive = $campaign->getMailActive3();
					$nextDays = $campaign->getMailDate3Days();
					$nextHours = $campaign->getMailDate3Hours();
				} elseif($keyOfMail == 3){
					$isNextActive = $campaign->getMailActive4();
					$nextDays = $campaign->getMailDate4Days();
					$nextHours = $campaign->getMailDate4Hours();
				} elseif($keyOfMail == 4){
					$isNextActive = $campaign->getMailActive5();
					$nextDays = $campaign->getMailDate5Days();
					$nextHours = $campaign->getMailDate5Hours();
				}
				// if next is active set next date and stage
				if($isNextActive == 1){
					// check mail and calculate next date
					if($keyOfMail == 1){
						// create email date format
						$full_date = new \DateTime($campaign->getMailDate1()->format('Y-m-d H:i'));
						$zeros = 0;
						if(!empty($nextDays)){
							$full_date->add(new \DateInterval('P'.$nextDays.'D'));
							$zeros++;
						}
						if(!empty($nextHours)){
							$full_date->add(new \DateInterval('PT'.$nextHours.'H'));
							$zeros++;
						}
						// if not edited by days and hours set now + 1 hour
						if($zeros <= 0){
							$full_date = new \DateTime();
							$full_date->add(new \DateInterval('PT1H'));
							// set format
							$full_date = $full_date->format('Y-m-d H:i:s');
							// create object of DateTime
							$full_date = new \DateTime($full_date);
						}
						// set format
						// $full_date = $full_date->format('Y-m-d H:i:s');
						// set next message date to database
						$campaign->setMailDate2Full($full_date);
					} elseif($keyOfMail == 2){
						// create email date format
						$full_date = new \DateTime($campaign->getMailDate2Full()->format('Y-m-d H:i'));
						$zeros = 0;
						if(!empty($nextDays)){
							$full_date->add(new \DateInterval('P'.$nextDays.'D'));
							$zeros++;
						}
						if(!empty($nextHours)){
							$full_date->add(new \DateInterval('PT'.$nextHours.'H'));
							$zeros++;
						}
						// if not edited by days and hours set now + 1 hour
						if($zeros <= 0){
							$full_date = new \DateTime();
							$full_date->add(new \DateInterval('PT1H'));
							// set format
							$full_date = $full_date->format('Y-m-d H:i:s');
							// create object of DateTime
							$full_date = new \DateTime($full_date);
						}
						// set format
						// $full_date = $full_date->format('Y-m-d H:i:s');
						// set next message date to database
						$campaign->setMailDate3Full($full_date);
					} elseif($keyOfMail == 3){
						// create email date format
						$full_date = new \DateTime($campaign->getMailDate3Full()->format('Y-m-d H:i'));
						$zeros = 0;
						if(!empty($nextDays)){
							$full_date->add(new \DateInterval('P'.$nextDays.'D'));
							$zeros++;
						}
						if(!empty($nextHours)){
							$full_date->add(new \DateInterval('PT'.$nextHours.'H'));
							$zeros++;
						}
						// if not edited by days and hours set now + 1 hour
						if($zeros <= 0){
							$full_date = new \DateTime();
							$full_date->add(new \DateInterval('PT1H'));
							// set format
							$full_date = $full_date->format('Y-m-d H:i:s');
							// create object of DateTime
							$full_date = new \DateTime($full_date);
						}
						// set format
						// $full_date = $full_date->format('Y-m-d H:i:s');
						// set next message date to database
						$campaign->setMailDate4Full($full_date);
					} elseif($keyOfMail == 4){
						// create email date format
						$full_date = new \DateTime($campaign->getMailDate4Full()->format('Y-m-d H:i'));
						$zeros = 0;
						if(!empty($nextDays)){
							$full_date->add(new \DateInterval('P'.$nextDays.'D'));
							$zeros++;
						}
						if(!empty($nextHours)){
							$full_date->add(new \DateInterval('PT'.$nextHours.'H'));
							$zeros++;
						}
						// if not edited by days and hours set now + 1 hour
						if($zeros <= 0){
							$full_date = new \DateTime();
							$full_date->add(new \DateInterval('PT1H'));
							// set format
							$full_date = $full_date->format('Y-m-d H:i:s');
							// create object of DateTime
							$full_date = new \DateTime($full_date);
						}
						// set format
						// $full_date = $full_date->format('Y-m-d H:i:s');
						// set next message date to database
						$campaign->setMailDate5Full($full_date);
					}
					// set stage of mailing
					$campaign->setMailingStage($keyOfMail+1);

					// save to database
					$this->entityManager->persist($campaign);
					// clean database
					$this->entityManager->flush();
				}

				print_r("info - send: ". $sent);
				return True;
			}
		return False;
	}

	public function sendRaport($campaignClientMail, $text){
		// get from user - settings Admin
		$from = $this->getContainer()->getParameter('mailer_user');
		// get mailer
		$mailer = $this->getContainer()->get('mailer');
		// send message
		$message = \Swift_Message::newInstance()
			->setSubject('Raport z Kampanii')
	        ->setFrom($from)
	        ->setTo([$campaignClientMail, $from])
	        ->setBody($text);
		$mailer->send($message);
	}

}

?>
