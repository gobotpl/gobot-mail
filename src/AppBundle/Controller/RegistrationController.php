<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\User;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


class RegistrationController extends BaseController
{

    public function __construct(UrlGeneratorInterface $router){
        $this->router = $router;
    }

    public function registerAction(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_ADMIN");

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                // Mod - add Client Role
                $user->addRole("ROLE_CLIENT");

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);
                $userManager->updateUser($user);

                // Mod2 - add Client ID after create Client
                $user->setClient($user->getId());

                // Mod - set MessageReply for no && OffUser for no
                $user->setOffUser(User::OFF_USER_NO);
                $user->setMessagesReply(User::MESSAGES_REPLY_STOP);

                $userManager->updateUser($user);

                // Mod3 - add ClientSettings after create
                // add Client to ClientSettings
                $entityManager = $this->getDoctrine()->getManager();
                $clientSettings = new ClientSettings();
                $clientSettings->setClient($user);
                $entityManager->persist($clientSettings);
                $entityManager->flush();
                // add ClientSettings to User
                $user->setClientSettings($clientSettings);
                $userManager->updateUser($user);

                /*** Manually send recovery password link ***/
                // check Confirmation is empty and generate Token
                if (null === $user->getConfirmationToken()) {
                    // $user->setConfirmationToken($this->tokenGenerator->generateToken());
                    $tokenGenerator = $this->get('fos_user.util.token_generator');
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                }
                // send email you requested
                // $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
                $this->sendResettingEmailMessageFIX($user);
                // $this->mailer->sendResettingEmailMessage($user);
                // this depends on requirements
                $user->setPasswordRequestedAt(new \DateTime());
                $userManager->updateUser($user);
                /*** Manually send recovery password link ***/


                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                // $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

                $this->addFlash("success", "Udało się dodać klienta");
                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function sendResettingEmailMessageFIX(User $user)
    {
        $template = $this->getParameter('register_template');
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $rendered = \Swift_Message::newInstance()
            ->setSubject('Pierwsze logowanie')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($user->getEmail())
            ->setBody($this->renderView($template, array('user' => $user, 'confirmationUrl' => $url)), 'text/html');
        $this->get('mailer')->send($rendered);
    }

}

?>
