<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\DirectLead;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class DirectLeadController extends Controller
{

	/**
	 * @Route("/panel/directleads/campaign/{id}", name="direct_leads_index")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function indexAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}
		// get DirectedLeads
		$entityManager = $this->getDoctrine()->getManager();
		$directLeads = $entityManager->getRepository(DirectLead::class)->findBy(array("campaign" => $campaign->getId()));

		return $this->render("DirectLead/index.html.twig", [
			"directLeads" => $directLeads,
			"campaign_name" => $campaign->getName(),
		]);
	}

}
