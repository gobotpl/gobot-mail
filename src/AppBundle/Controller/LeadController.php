<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Lead;
use AppBundle\Entity\LeadImport;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class LeadController extends Controller
{

	/**
	 * @Route("/panel/lead", name="lead_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_CLIENT");

		return $this->render("Lead/index.html.twig", []);
	}

	/**
	 * @Route("/panel/lead/delete/{id}", name="lead_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  Lead $lead
	 */
	public function deleteAction(Request $request, Lead $lead) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $lead->getLeadImport()->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// id for mailbase_show return
		$id = $request->get("form")["LeadImportId"];
		// remove Lead form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($lead);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Lead'a");
		// return to mailbase_show
		return $this->redirectToRoute("mailbase_show", array('id' => $id));
	}

}
