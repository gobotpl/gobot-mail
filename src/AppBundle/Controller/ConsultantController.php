<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Consultant;
use AppBundle\Form\ConsultantType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ConsultantController extends Controller
{

	/**
 	 * @Route("/panel/client/register/consultant", name="user_register_consultant")
	 * @param  Request $request
	 * @return Response
	 */
	public function registerConsultantAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		// create new Consultant object
		$consultant = new Consultant();
		// create form to import
		$form = $this->createForm(ConsultantType::class, $consultant);
		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {
	            // save Consultant to database
	            $entityManager = $this->getDoctrine()->getManager();

				// find Client
				$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
				// add Client to Consultant
				$consultant->setClient($user);

	            $entityManager->persist($consultant);
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Dodanie Konsultanta udało się");
				return $this->redirectToRoute("panel_index", []);
	        }
			// add flash with error
			$this->addFlash("error", "Nie powiodło się dodawanie Konsultanta.");
		}
		return $this->render("Client/registerConsultant.html.twig", ["form" => $form->createView()]);
	}

	/**
	 * @Route("/panel/client/consultant/edit/{id}", name="consultant_edit")
	 * @param  Request $request
	 * @param Consultant $consultant
	 */
	public function editAction(Request $request, Consultant $consultant) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $consultant->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$form = $this->createForm(ConsultantType::class, $consultant);

		if($request->isMethod("POST")){
			$form->handleRequest($request);
			if($form->isValid()){

				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($consultant);
				$entityManager->flush();

				$this->addFlash("success", "Edycja Konsultanta udała się");
				return $this->redirectToRoute("client_users");
			}
			$this->addFlash("error", "Nie powiodło się edytowanie Konsultanta.");
		}

		return $this->render("Client/editConsultant.html.twig", ["form" => $form->createView()]);
	}


	/**
	 * @Route("/panel/client/consultant/delete/{id}", name="consultant_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  Consultant $consultant
	 */
	public function deleteAction(Request $request, Consultant $consultant) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $consultant->getClient()->getId()){
			throw new AccessDeniedException();
		}
		// remove Consultant form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($consultant);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Konsultanta");
		// return to client_users
		return $this->redirectToRoute("client_users");
	}


}
