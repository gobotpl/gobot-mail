<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\ClientSettingsSMTP;
use AppBundle\Form\ClientSettingsIMAPType;
use AppBundle\Form\ClientSettingsSMTPType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ClientSettingsController extends Controller
{

	/**
 	 * @Route("/panel/client/settings", name="client_settings_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_CLIENT");

		$entityManager = $this->getDoctrine()->getManager();
		// find ClientSettings
		$clientSettings = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()));
		$form = $this->createForm(ClientSettingsIMAPType::class, $clientSettings);

		// find all ClientSettingsSMTP
		$clientSettingsSMTPServers = $entityManager->getRepository(ClientSettingsSMTP::class)->findBy(array("client" => $this->getUser()));

		// create new SMTP form
		$clientSettingsSMTP = new ClientSettingsSMTP();
		// create form for SMTP
		$formSMTP = $this->createForm(ClientSettingsSMTPType::class, $clientSettingsSMTP);

		if($request->isMethod("POST")){
			$form->handleRequest($request);
			$formSMTP->handleRequest($request);

			if ($form->isSubmitted()) {

				if($form->isValid()){

					$entityManager->persist($clientSettings);
					$entityManager->flush();

					$this->addFlash("success", "Edycja ustawień udała się");
					return $this->redirectToRoute("client_settings_index");
				}
				$this->addFlash("error", "Nie powiodła się edycja ustawień.");

		    } else if ($formSMTP->isSubmitted()) {

				if($formSMTP->isValid()){
					$clientSettingsSMTP->setClient($this->getUser());
					$entityManager->persist($clientSettingsSMTP);
					$entityManager->flush();

					$this->addFlash("success", "Edycja ustawień udała się");
					return $this->redirectToRoute("client_settings_index");
				}
				$this->addFlash("error", "Nie powiodła się edycja ustawień.");

		    }
			// redirect to ClientSettings
			return $this->redirectToRoute("client_settings_index");
		}

		$editForms = array();
		$deleteForms = array();
		foreach ($clientSettingsSMTPServers as $server) {
			$editForm = $this->createForm(ClientSettingsSMTPType::class, $server);
			$editForms[$server->getId()] = $editForm->createView();

			$deleteForm = $this->createFormBuilder()
				->setAction($this->generateUrl("client_settings_smtp_delete", ["id" => $server->getId()]))
				->setMethod(Request::METHOD_DELETE)
				->add("submit", SubmitType::class, ["label" => "Usuń", "attr" => ["onclick" => "return confirm('Czy na pewno chcesz usunąć?');"]])
				->getForm();
			$deleteForms[$server->getId()] = $deleteForm->createView();
		}

		return $this->render("Panel/Client/settings.html.twig", [
			"form" => $form->createView(),
			"formSMTP" => $formSMTP->createView(),
			"clientSettingsSMTPServers" => $clientSettingsSMTPServers,
			"editForms" => $editForms,
			"deleteForms" => $deleteForms,
		]);
	}


	/**
 	 * @Route("/panel/client/settings/edit/{id}", name="client_settings_smtp_edit")
	 * @param  Request $request
	 * @param ClientSettingsSMTP $ClientSettingsSMTP
	 */
	public function editAction(Request $request, ClientSettingsSMTP $ClientSettingsSMTP) {
		$this->denyAccessUnlessGranted("ROLE_CLIENT");
		// security
		if($this->getUser()->getClient() != $ClientSettingsSMTP->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// create form for SMTP Server
		$form = $this->createForm(ClientSettingsSMTPType::class, $ClientSettingsSMTP);

		// if edit form is here
		if($request->isMethod("POST")){
			$form->handleRequest($request);

			if($form->isValid()){

				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($ClientSettingsSMTP);
				$entityManager->flush();

				$this->addFlash("success", "Edycja ustawień udała się");
				return $this->redirectToRoute("client_settings_index");
			}
			$this->addFlash("error", "Nie powiodła się edycja ustawień.");
		}

		// redirect to ClientSettings
		return $this->redirectToRoute("client_settings_index");
	}

	/**
	 * @Route("/panel/client/settings/delete/{id}", name="client_settings_smtp_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  ClientSettingsSMTP $ClientSettingsSMTP
	 */
	public function deleteAction(Request $request, ClientSettingsSMTP $ClientSettingsSMTP) {
		$this->denyAccessUnlessGranted("ROLE_CLIENT");
		// security
		if($this->getUser()->getClient() != $ClientSettingsSMTP->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// remove ClientSettingsSMTP form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($ClientSettingsSMTP);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć serwer z listy");
		// return to client_settings_index
		return $this->redirectToRoute("client_settings_index");
	}


	/**
 	 * @Route("/panel/client/settings/companyfooter", name="client_settings_companyfooter")
	 * @param  Request $request
	 */
	public function companyFooterAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		// find ClientSettings
		$clientSettings = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()->getClient()));
		// create form for Company Footer
		$form = $this->createFormBuilder($clientSettings)
		   ->add('companyFooter', TextareaType::class, array('label' => 'Stopka firmowa'))
		   ->getForm();

		// if edit form is here
		if($request->isMethod("POST")){
			$form->handleRequest($request);
			// if is valid
			if($form->isValid()){
				// save to database
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->persist($clientSettings);
				$entityManager->flush();

				$this->addFlash("success", "Edycja stopki firmowej udała się");
				return $this->redirectToRoute("client_settings_companyfooter");
			}
			$this->addFlash("error", "Nie powiodła się edycja stopki firmowej.");
		}

		return $this->render("Panel/Client/companyFooter.html.twig", [
			"form" => $form->createView(),
		]);
	}
}
