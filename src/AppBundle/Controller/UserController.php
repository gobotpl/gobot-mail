<?php

namespace AppBundle\Controller;


use AppBundle\Entity\User;
use AppBundle\Entity\Consultant;
use AppBundle\Entity\ClientSettings;
use AppBundle\Form\UserEditType;
use AppBundle\Form\UserClientEditType;
use AppBundle\Form\UserRegistrationType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class UserController extends Controller
{

    public function __construct(UrlGeneratorInterface $router){
        $this->router = $router;
    }

	/**
	 * @Route("/panel/client/register/agent", name="user_register_agent")
	 */
    public function registerAgentAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

        $entityManager = $this->getDoctrine()->getManager();

		// create new object of User and create Form
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);
		// handle and check valid
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                // check email is in database
                $emailExists = $entityManager->getRepository(User::class)->findOneBy(array("email" => $form->get('email')->getData()));
                if(count($emailExists) > 0){
                    $this->addFlash("error", "Użytkownik o takim adresie email już istnieje");
                } else {


    	            // Encode the password
    	            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
    	            $user->setPassword($password);
    				// set other data
    		        $user->setEnabled(true);
                    $user->addRole("ROLE_USER");
                    $user->setTypeUser(User::TYPE_USER_AGENT);
                    $user->setOffUser(User::OFF_USER_NO);
                    $user->setMessagesReply(User::MESSAGES_REPLY_STOP);
                    $user->setClient($this->getUser()->getClient());

                    // check companyName
                    $companyName = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
                    // set CompanyName
                    $user->setCompanyName($companyName->getCompanyName());

                    // check company Footer
                    $companyFooter = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()->getClient()));
                    // set Company Footer for default to user
                    $user->setFooter($companyFooter->getCompanyFooter());


                    /*** Manually send recovery password link ***/
                    // check Confirmation is empty and generate Token
                    if (null === $user->getConfirmationToken()) {
                        // $user->setConfirmationToken($this->tokenGenerator->generateToken());
                        $tokenGenerator = $this->get('fos_user.util.token_generator');
                        $user->setConfirmationToken($tokenGenerator->generateToken());
                    }
                    // send email you requested
                    // $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
                    $this->sendResettingEmailMessageFIX($user);
                    // $this->mailer->sendResettingEmailMessage($user);
                    // this depends on requirements
                    $user->setPasswordRequestedAt(new \DateTime());
                    // $userManager->updateUser($user);
                    /*** Manually send recovery password link ***/


    	            // save the User!
    	            // $entityManager = $this->getDoctrine()->getManager();
    	            $entityManager->persist($user);
    	            $entityManager->flush();

    				// add flash success and redirect to panel index
    				$this->addFlash('success', 'Dodanie Agenta udało się');
                }
            	return $this->redirectToRoute('client_users');
			}
			// add flash with error
			$this->addFlash("error", "Nie powiodło się dodawanie Agenta.");
        }

        return $this->render(
            "Client/registerAgent.html.twig", [
				"form" => $form->createView()
			]
        );
    }

	/**
	 * @Route("/panel/client/agent/edit/{id}", name="agent_edit")
	 * @param  Request $request
	 * @param User $user
	 */
	public function editAgentAction(Request $request, User $user) {
        $this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $user->getClient()){
			throw new AccessDeniedException();
		}
        // check actuall email of user
        $actualEmailOfUser = $user->getEmail();
        // create edit form
		$form = $this->createForm(UserEditType::class, $user);

		if($request->isMethod("POST")){
			$form->handleRequest($request);
			if($form->isValid()){
                $entityManager = $this->getDoctrine()->getManager();
                // check email is the same what user have
                if($form->get('email')->getData() == $actualEmailOfUser){
                    // save user
    				$entityManager->persist($user);
    				$entityManager->flush();
    				$this->addFlash("success", "Edycja Agenta udała się");
                } else {
                    // check email is in database
                    $emailExists = $entityManager->getRepository(User::class)->findOneBy(array("email" => $form->get('email')->getData()));
                    if(count($emailExists) > 0){
                        $this->addFlash("error", "Użytkownik o takim adresie email już istnieje");
                    } else {
                        // save user
        				$entityManager->persist($user);
        				$entityManager->flush();
        				$this->addFlash("success", "Edycja Agenta udała się");
                    }
                }

				return $this->redirectToRoute("client_users");
			}
			$this->addFlash("error", "Nie powiodło się edytowanie Agenta.");
		}

		return $this->render("Client/editAgent.html.twig", ["form" => $form->createView()]);
	}

	/**
	 * @Route("/panel/client/register/kp", name="user_register_kp")
	 */
    public function registerKPAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->denyAccessUnlessGranted("ROLE_CLIENT");

        $entityManager = $this->getDoctrine()->getManager();

		// create new object of User and create Form
        $user = new User();
        $form = $this->createForm(UserRegistrationType::class, $user);
		// handle and check valid
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                // check email is in database
                $emailExists = $entityManager->getRepository(User::class)->findOneBy(array("email" => $form->get('email')->getData()));
                if(count($emailExists) > 0){
                    $this->addFlash("error", "Użytkownik o takim adresie email już istnieje");
                } else {

    	            // Encode the password
    	            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
    	            $user->setPassword($password);
    				// set other data
    		        $user->setEnabled(true);
                    $user->addRole("ROLE_USER");
                    $user->setTypeUser(User::TYPE_USER_KP);
                    $user->setOffUser(User::OFF_USER_NO);
                    $user->setMessagesReply(User::MESSAGES_REPLY_STOP);
                    $user->setClient($this->getUser()->getClient());

                    // check companyName
                    $companyName = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
                    // set CompanyName
                    $user->setCompanyName($companyName->getCompanyName());

                    // check company Footer
                    $companyFooter = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()->getClient()));
                    // set Company Footer for default to user
                    $user->setFooter($companyFooter->getCompanyFooter());


                    /*** Manually send recovery password link ***/
                    // check Confirmation is empty and generate Token
                    if (null === $user->getConfirmationToken()) {
                        // $user->setConfirmationToken($this->tokenGenerator->generateToken());
                        $tokenGenerator = $this->get('fos_user.util.token_generator');
                        $user->setConfirmationToken($tokenGenerator->generateToken());
                    }
                    // send email you requested
                    // $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
                    $this->sendResettingEmailMessageFIX($user);
                    // $this->mailer->sendResettingEmailMessage($user);
                    // this depends on requirements
                    $user->setPasswordRequestedAt(new \DateTime());
                    // $userManager->updateUser($user);
                    /*** Manually send recovery password link ***/


    	            // save the User!
    	            // $entityManager = $this->getDoctrine()->getManager();
    	            $entityManager->persist($user);
    	            $entityManager->flush();
    				// add flash success and redirect to panel index
    				$this->addFlash('success', 'Dodanie Kierownika Produkcji udało się');
                }
            	return $this->redirectToRoute('client_users');
			}
			// add flash with error
			$this->addFlash("error", "Nie powiodło się dodawanie Kierownika Produkcji.");
        }

        return $this->render(
            "Client/registerKP.html.twig", [
				"form" => $form->createView()
			]
        );
    }

	/**
	 * @Route("/panel/client/kp/edit/{id}", name="kp_edit")
	 * @param  Request $request
	 * @param User $user
	 */
	public function editKPAction(Request $request, User $user) {
		$this->denyAccessUnlessGranted("ROLE_CLIENT");
        // security
		if($this->getUser()->getClient() != $user->getClient()){
			throw new AccessDeniedException();
		}

        // check actuall email of user
        $actualEmailOfUser = $user->getEmail();
        // create edit form
		$form = $this->createForm(UserEditType::class, $user);

		if($request->isMethod("POST")){
			$form->handleRequest($request);
			if($form->isValid()){
				$entityManager = $this->getDoctrine()->getManager();
                // check email is the same what user have
                if($form->get('email')->getData() == $actualEmailOfUser){
                    // save user
    				$entityManager->persist($user);
    				$entityManager->flush();
    				$this->addFlash("success", "Edycja Kierownika projektu udała się");
                } else {
                    // check email is in database
                    $emailExists = $entityManager->getRepository(User::class)->findOneBy(array("email" => $form->get('email')->getData()));
                    if(count($emailExists) > 0){
                        $this->addFlash("error", "Użytkownik o takim adresie email już istnieje");
                    } else {
                        // save user
        				$entityManager->persist($user);
        				$entityManager->flush();
        				$this->addFlash("success", "Edycja Kierownika projektu udała się");
                    }
                }

				return $this->redirectToRoute("client_users");
			}
			$this->addFlash("error", "Nie powiodło się edytowanie Kierownika projektu.");
		}

		return $this->render("Client/editKP.html.twig", ["form" => $form->createView()]);
	}

	/**
	 * @Route("/panel/client/user/delete/{id}", name="user_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  User $user
	 */
	public function deleteAction(Request $request, User $user) {
        $this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $user->getClient()){
			throw new AccessDeniedException();
		}
		// remove User form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($user);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Użytkownika");
		// return to client_users
		return $this->redirectToRoute("client_users");
	}

	/**
	 * @Route("/panel/client/kp/delete/{id}", name="user_kp_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  User $user
	 */
	public function deleteKPAction(Request $request, User $user) {
        $this->denyAccessUnlessGranted("ROLE_CLIENT");
        // security
		if($this->getUser()->getClient() != $user->getClient()){
			throw new AccessDeniedException();
		}
		// remove User form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($user);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Użytkownika");
		// return to client_users
		return $this->redirectToRoute("client_users");
	}

	/**
	 * @Route("/panel/admin/client/edit/{id}", name="admin_client_edit")
	 * @param  Request $request
	 * @param User $user
	 */
	public function adminEditClientAction(Request $request, User $user) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

        // check actuall email of user
        $actualEmailOfUser = $user->getEmail();
        // create edit form
		$form = $this->createForm(UserClientEditType::class, $user);

		if($request->isMethod("POST")){
			$form->handleRequest($request);
			if($form->isValid()){
				$entityManager = $this->getDoctrine()->getManager();
                // check email is the same what user have
                if($form->get('email')->getData() == $actualEmailOfUser){
                    // save user
    				$entityManager->persist($user);
    				$entityManager->flush();
    				$this->addFlash("success", "Edycja Klienta udała się");
                } else {
                    // check email is in database
                    $emailExists = $entityManager->getRepository(User::class)->findOneBy(array("email" => $form->get('email')->getData()));
                    if(count($emailExists) > 0){
                        $this->addFlash("error", "Użytkownik o takim adresie email już istnieje");
                    } else {
                        // save user
        				$entityManager->persist($user);
        				$entityManager->flush();
        				$this->addFlash("success", "Edycja Klienta udała się");
                    }
                }

				return $this->redirectToRoute("admin_users");
			}
			$this->addFlash("error", "Nie powiodło się edytowanie Klienta.");
		}

		return $this->render("Panel/Admin/editClient.html.twig", ["form" => $form->createView()]);
	}

    /**
	 * @Route("/panel/client/agent/{id}/{offUser}", name="agent_off_user")
	 * @param  Request $request
	 * @param  User $user
	 */
	public function offAgentAction(Request $request, User $user, $offUser) {
        $this->denyAccessUnlessGranted("ROLE_CLIENT");
        // security
		if($this->getUser()->getClient() != $user->getClient()){
			throw new AccessDeniedException();
		}
		// change offUser in database
		$entityManager = $this->getDoctrine()->getManager();
        if($offUser == "yes"){
            $user->setOffUser(User::OFF_USER_YES);
        } else {
            $user->setOffUser(User::OFF_USER_NO);
        }
		$entityManager->persist($user);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się zaktualizować Agenta");
		// return to client_users
		return $this->redirectToRoute("client_users");
	}

    /**
	 * @Route("/panel/client/consultant/{id}/{offUser}", name="consultant_off_user")
	 * @param  Request $request
	 * @param  Consultant $consultant
	 */
	public function offConsultantAction(Request $request, Consultant $consultant, $offUser) {
        $this->denyAccessUnlessGranted("ROLE_CLIENT");
        // security
		if($this->getUser()->getClient() != $consultant->getClient()->getId()){
			throw new AccessDeniedException();
		}
		// change offUser in database
		$entityManager = $this->getDoctrine()->getManager();
        if($offUser == "yes"){
            $consultant->setOffUser(Consultant::OFF_USER_YES);
        } else {
            $consultant->setOffUser(Consultant::OFF_USER_NO);
        }
		$entityManager->persist($consultant);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się zaktualizować Konsultanta");
		// return to client_users
		return $this->redirectToRoute("client_users");
	}


    public function sendResettingEmailMessageFIX(User $user)
    {
        $template = $this->getParameter('register_template');
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), UrlGeneratorInterface::ABSOLUTE_URL);

        $rendered = \Swift_Message::newInstance()
            ->setSubject('Pierwsze logowanie')
            ->setFrom($this->getParameter('mailer_user'))
            ->setTo($user->getEmail())
            ->setBody($this->renderView($template, array('user' => $user, 'confirmationUrl' => $url)), 'text/html');
        $this->get('mailer')->send($rendered);
    }

}

?>
