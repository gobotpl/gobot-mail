<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\ClientSettingsSMTP;
use AppBundle\Entity\Lead;
use AppBundle\Form\CampaignType;
use AppBundle\Form\CampaignEditType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Service\RetriverDataService;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;

class CampaignController extends Controller
{

	/**
	 * @Route("/panel/campaign/list", name="campaign_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request, RetriverDataService $retriverData) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		// $campaigns = $entityManager->getRepository(Campaign::class)->findBy(array("client" => $this->getUser()->getClient()));
		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:Campaign u WHERE u.client = :client AND u.type != :type'
            )->setParameter('client', $this->getUser()->getClient())
			->setParameter('type', Campaign::TYPE_DELETED);
		$campaigns = $query->getResult();


		$countLeads = array();
		foreach ($campaigns as $campaign) {
			// print_r($campaign->getLeadImport()->getId());
			$countLeads[$campaign->getId()] = $retriverData->getCountLeads($campaign->getConditions(), $campaign->getLeadImport()->getId(), 1);
		}

		$deleteForms = array();
		foreach ($campaigns as $campaign) {
			$deleteForm = $this->createFormBuilder()
				->setAction($this->generateUrl("campaign_delete", ["id" => $campaign->getId()]))
				->setMethod(Request::METHOD_DELETE)
				->add("submit", SubmitType::class, ["label" => "Usuń", "attr" => ["onclick" => "return confirm('Czy na pewno chcesz usunąć?');"]])
				->getForm();
			$deleteForms[$campaign->getId()] = $deleteForm->createView();
		}


		$receiveBot = 0;
		$sendBot = 0;
		// get bot settings from client
		$botSettings = $entityManager->getRepository(ClientSettings::class)->findOneBy(["client" => $this->getUser()->getClient()]);
		// check bot settings is not empty
		if(!empty($botSettings->getBotEmail()) && !empty($botSettings->getBotPassword()) && !empty($botSettings->getBotImapPort()) && !empty($botSettings->getBotServer()) && !empty($botSettings->getBotImapEncryption())){
			$receiveBot = 1;
		}
		// get bot settings SMTP from client - if once are good set OKEY
		$botSettingsSMTP = $entityManager->getRepository(ClientSettingsSMTP::class)->findBy(["client" => $this->getUser()->getClient()]);
		if(count($botSettingsSMTP) > 0){
			foreach ($botSettingsSMTP as $key => $botSMTP) {// check bot settings is not empty
				if(!empty($botSMTP->getBotEmail()) && !empty($botSMTP->getBotPassword()) && !empty($botSMTP->getBotSmtpPort()) && !empty($botSMTP->getBotServer()) && !empty($botSMTP->getBotSmtpEncryption())){
					$sendBot = 1;
				}
			}
		}
		// check bot settings are okey
		$BotSettings = 0;
		if($receiveBot == 1 && $sendBot == 1){
			$BotSettings = 1;
		}

		return $this->render("Campaign/index.html.twig", [
			"campaigns" => $campaigns,
			"countLeads" => $countLeads,
			"deleteForms" => $deleteForms,
			"BotSettings" => $BotSettings,
		]);
	}

	/**
	 * @Route("/panel/campaign/show/{id}", name="campaign_show")
	 * @param  Request $request
	 * @param Campaign $campaign
	 */
	public function showAction(Request $request, Campaign $campaign, RetriverDataService $retriverData) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		$campaign = $entityManager->getRepository(Campaign::class)->findOneBy(array("id" => $campaign->getId(), "client" => $this->getUser()->getClient()));
		// get Labels
		$labels = $retriverData->getLabelsData($campaign->getLeadImport()->getId());
		// get Leads Data
		$leads = $retriverData->getCountLeads($campaign->getConditions(), $campaign->getLeadImport()->getId(), 1, false);
		$leads_data = array();
		foreach ($leads as $key => $value) {
			$leads_data[$value] = $retriverData->getLeadData($value)[0];
		}

		return $this->render("Campaign/show.html.twig", [
			"campaignName" => $campaign->getName(),
			"leads" => $leads_data,
			"labels" => $labels,
		]);
	}

	/**
	 * @Route("/panel/campaign/add", name="campaign_add")
	 * @param  Request $request
	 */
	public function addAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create new Campaign object
		$campaign = new Campaign();

		// check company Footer
		$companyFooter = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()->getClient()));
		// set Company Footer for default to user
		$campaign->setFooter($companyFooter->getCompanyFooter());

		// create form to import
		$form = $this->createForm(CampaignType::class, $campaign);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				// get data from form
				$conditions = $form->get('conditions')->getData();
				// implode data to database format
				$data_conditions = "";
				$which = 0;
				foreach ($conditions as $key => $value) {
					if($which != 0){
						$data_conditions .= "{if}";
					}
					foreach ($value as $key_in => $value_in) {
						$data_conditions .= "[".$key_in."]".$value_in;
					}
					$which++;
				}
				// set parameter
				$campaign->setConditions($data_conditions);

	            // save parameters to database
				// find Client
				$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
				// add Client to Campaign
				$campaign->setClient($user);
				// add type of Campaign
				// $campaign->setType(Campaign::TYPE_ACTIVE);
				$campaign->setType(Campaign::TYPE_IN_BUILD);
				$campaign->setTypeCamp(Campaign::TYPE_CAMP_NORMAL);
				// set MailingStage to first
				$campaign->setMailingStage(1);

				// save to database
	            $entityManager->persist($campaign);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Kampania dodana poprawnie.");
				return $this->redirectToRoute("campaign_index", []);

	        }
			// add flash with error
			$this->addFlash("error", "Kampania nie została dodana");
		}
		return $this->render("Campaign/add.html.twig", [
			"form" => $form->createView(),
		]);
	}


	/**
	 * @Route("/panel/campaign/edit/{id}", name="campaign_edit")
	 * @param  Request $request
	 * @param Campaign $campaign
	 */
	public function editAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		// create form to import from Campaign
		$form = $this->createForm(CampaignEditType::class, $campaign);
		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				// get data from form
				$conditions = $form->get('conditions')->getData();
				// implode data to database format
				$data_conditions = "";
				$which = 0;
				foreach ($conditions as $key => $value) {
					if($which != 0){
						$data_conditions .= "{if}";
					}
					foreach ($value as $key_in => $value_in) {
						$data_conditions .= "[".$key_in."]".$value_in;
					}
					$which++;
				}
				// set parameter
				$campaign->setConditions($data_conditions);

	            // save parameters to database
	            $entityManager = $this->getDoctrine()->getManager();

				// find Client
				$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
				// add Client to Campaign
				$campaign->setClient($user);
				// add type of Campaign
				// $campaign->setType(Campaign::TYPE_ACTIVE);
				$campaign->setType(Campaign::TYPE_IN_BUILD);
				// clean MailingStage
				$campaign->setMailingStage(1);

				// save to database
	            $entityManager->persist($campaign);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Kampania edytowana poprawnie.");
				return $this->redirectToRoute("campaign_index", []);
	        }
			// add flash with error
			$this->addFlash("error", "Edycja kampanii nie udała się");
		}

		return $this->render("Campaign/edit.html.twig", [
			"form" => $form->createView(),
		]);
	}

	/**
	 * @Route("/panel/campaign/ajaxlabels/{id}", name="campaign_ajax_labels")
	 * @Method("POST")
	 * @param Request $request
	 * @param LeadImport $lead_import
	 */
	public function ajaxLabelsAction(Request $request, RetriverDataService $retriverData, LeadImport $leadImport)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
        // check request for using AJAX
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

		// get labelsData from retriver for LeadImport
		$labels = $retriverData->getLabelsData($leadImport->getId());
		if($labels){
            // return success
            return new JsonResponse(array('message' => json_encode($labels, JSON_UNESCAPED_UNICODE)), 200);
        }
        // return error
        return new JsonResponse(array('message' => 'error'), 400);
	}

	/**
	 * @Route("/panel/campaign/ajaxleads", name="campaign_ajax_leads")
	 * @Method("POST")
	 * @param Request $request
	 */
	public function ajaxLeadsAction(Request $request, RetriverDataService $retriverData)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
        // check request for using AJAX
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }
		// get data from AJAX data
		$content = $request->getContent();
        if (!empty($content)) {
            $data = json_decode($content, true);
			// get CountLeads from retriver
			$leads = $retriverData->getCountLeads($data);
			// if($leads){
	            // return success
	            return new JsonResponse(array('message' => json_encode($leads, JSON_UNESCAPED_UNICODE)), 200);
	        // }
		}
        // return error
        return new JsonResponse(array('message' => 'error'), 400);
	}


	/**
	 * @Route("/panel/campaign/ajaxcheckname/{name}", name="campaign_ajax_checkname")
	 * @Method("POST")
	 * @param Request $request
	 * @param $name
	 */
	public function ajaxCheckNameAction(Request $request, RetriverDataService $retriverData, $name)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
        // check request for using AJAX
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

		$entityManager = $this->getDoctrine()->getManager();
		$checkNameExists = $entityManager->getRepository(Campaign::class)->findBy(array("name" => $name));
		if(count($checkNameExists) > 0){
			return new JsonResponse(array('message' => json_encode("found", JSON_UNESCAPED_UNICODE)), 200);
		} else {
			return new JsonResponse(array('message' => json_encode("not-found", JSON_UNESCAPED_UNICODE)), 200);
		}
        // return error
        return new JsonResponse(array('message' => 'error'), 400);
	}


	/**
	 * @Route("/panel/campaign/delete/{id}", name="campaign_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function deleteAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// remove Campaign form database
		$entityManager = $this->getDoctrine()->getManager();
		$campaign->setType(Campaign::TYPE_DELETED);
		$entityManager->persist($campaign);
		// $entityManager->remove($campaign);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Kampanię");
		// return to campaign_index
		return $this->redirectToRoute("campaign_index");
	}

	/**
	 * @Route("/panel/campaign/active/{id}", name="campaign_active")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function activeAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// active Campaign in database
		$entityManager = $this->getDoctrine()->getManager();

		// check campaign is allright for active - if not say error
		$errCheck = 0;

		// check first email date is not empty
		if(!empty($campaign->getMailDate1())){
			// create now time and get set date
			$newTZ = new \DateTimeZone("Europe/Warsaw");
			$now_date = new \DateTime();
			$now_date->setTimezone( $newTZ );
			$now_date = $now_date->format('Y-m-d H:i:s');
			$campaign_date = $campaign->getMailDate1()->format('Y-m-d H:i:s');
			// check campaign date is greater than now
			if ($campaign_date > $now_date) {
				// if is - check how many minutes we have - if more than 1 = OK
				$start = date_create($campaign_date);
				$end = date_create($now_date);
				$diff=date_diff($end,$start);
				$minutes = $diff->d * 24 * 60;
				$minutes += $diff->h * 60;
				$minutes += $diff->i;
				if($minutes <= 1){ $errCheck = 2; }
			} else { $errCheck = 2; }
		} else { $errCheck = 1; }

		if($campaign->getMailActive1() == 1){
			if(strlen($campaign->getMailText1()) <= 0){ $errCheck = 1; }
		}
		if($campaign->getMailActive2() == 1){
			if(strlen($campaign->getMailText2()) <= 0){ $errCheck = 1; }
			if(empty($campaign->getMailDate2Days()) && empty($campaign->getMailDate2Hours())){ $errCheck = 1; }
		}
		if($campaign->getMailActive3() == 1){
			if(strlen($campaign->getMailText3()) <= 0){ $errCheck = 1; }
			if(empty($campaign->getMailDate3Days()) && empty($campaign->getMailDate3Hours())){ $errCheck = 1; }
		}
		if($campaign->getMailActive4() == 1){
			if(strlen($campaign->getMailText4()) <= 0){ $errCheck = 1; }
			if(empty($campaign->getMailDate4Days()) && empty($campaign->getMailDate4Hours())){ $errCheck = 1; }
		}
		if($campaign->getMailActive5() == 1){
			if(strlen($campaign->getMailText5()) <= 0){ $errCheck = 1; }
			if(empty($campaign->getMailDate5Days()) && empty($campaign->getMailDate5Hours())){ $errCheck = 1; }
		}

		// show Flash error
		if($errCheck == 1){
			$this->addFlash("error", "Uzupełnij poprawnie pola Kampanii");
		} elseif($errCheck == 2){
			$this->addFlash("error", "Data wysłania jest błędnie ustawiona!");
		} else {
			$campaign->setType(Campaign::TYPE_ACTIVE);
			$entityManager->persist($campaign);
			$entityManager->flush();
			// show Flash success
			$this->addFlash("success", "Aktywowano Kampanię");
		}
		// return to campaign_index
		return $this->redirectToRoute("campaign_index");
	}

	/**
	 * @Route("/panel/campaign/stop/{id}", name="campaign_stop")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function stopAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// stop Campaign in database
		$entityManager = $this->getDoctrine()->getManager();
		$campaign->setType(Campaign::TYPE_STOPED);
		$entityManager->persist($campaign);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Zatrzymano Kampanię");
		// return to campaign_index
		return $this->redirectToRoute("campaign_index");
	}

	/**
	 * @Route("/panel/campaign/copy/{id}", name="campaign_copy")
	 * @param  Request $request
	 * @param Campaign $campaign
	 */
	public function CopyAction(Request $request, Campaign $campaign) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		// count copies and set next
		$checkCopies = $entityManager->getRepository(Campaign::class)->findBy(array("typeCamp" => Campaign::TYPE_CAMP_COPY, "campParent" => $campaign->getId()));

		// duplicate with custom name, type = inBuild
		$duplicat = clone $campaign;
		$duplicat->setName($campaign->getName()." - kopia #".count($checkCopies));
		$duplicat->setType(Campaign::TYPE_IN_BUILD);
		$duplicat->setTypeCamp(Campaign::TYPE_CAMP_COPY);
		// add campParent to Duplicate Campaign
		$duplicat->setCampParent($campaign->getId());

		// save to database
		$entityManager->persist($duplicat);
		// clean EntityManager
		$entityManager->flush();

		$this->addFlash("success", "Kopię utworzono poprawnie");

		return $this->redirectToRoute("campaign_index");
	}

}
