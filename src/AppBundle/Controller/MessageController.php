<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Message;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\DirectLead;
use AppBundle\Entity\Consultant;
use AppBundle\Entity\KeywordMail;
use AppBundle\Entity\MessageReply;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\ClientSettingsSMTP;
use AppBundle\Form\MessageType;
use AppBundle\Service\RetriverDataService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MessageController extends Controller
{

	/**
	 * @Route("/panel/messages/campaigns", name="messages_campaigns_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");

		// if agent ask show him only campaigns for him
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){

			// get campaigns for agent
			$campaigns_check = $this->getDoctrine()->getEntityManager()
				->createQuery(
					'SELECT u FROM AppBundle:Campaign u WHERE u.client = :client AND u.type != :type'
				)
				->setParameter('client', $this->getUser()->getClient())
				->setParameter('type', Campaign::TYPE_DELETED)
				->getResult();

			$campaigns = array();
			// for each campaign check Agent is selected
			foreach ($campaigns_check as $key => $campaign) {
				// get Agents from campaigns join table
				$campaignsAgent = $campaign->getAgent();
				// check agent is selected for this campaign
				foreach ($campaignsAgent as $key => $agent) {
					if($agent->getId() == $this->getUser()->getId()){
						$campaigns[] = $campaign;
					}
				}
			}
		} else {
			$campaigns = $this->getDoctrine()->getEntityManager()
				->createQuery(
					'SELECT u FROM AppBundle:Campaign u WHERE u.client = :client AND u.type != :type'
				)
				->setParameter('client', $this->getUser()->getClient())
				->setParameter('type', Campaign::TYPE_DELETED)
				->getResult();
		}

		// $entityManager = $this->getDoctrine()->getManager();
		// $campaigns = $entityManager->getRepository(Campaign::class)->findBy(array("client" => $this->getUser()->getClient(), "type" => Campaign::TYPE_DELETED));

		return $this->render("Messages/index.html.twig", [
			"campaigns" => $campaigns,
		]);
	}

	/**
	 * @Route("/panel/messages/campaigns/{id}/{type}", name="messages_campaign_show")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function showCampaignAction(Request $request, Campaign $campaign, $type) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}
		// if agent ask show him only messages for him
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){
			$query = $this->getDoctrine()->getEntityManager()
	            ->createQuery(
	                'SELECT u FROM AppBundle:Message u WHERE u.inboxType = :type AND u.assignedAgent = :assignedAgent AND u.campaign = :campaign AND u.messageId is not NULL GROUP BY u.messagesGroup ORDER BY u.messageDate ASC'
	            )
				// ->setParameter('type', Message::INBOX_TYPE_INBOX)
				->setParameter('campaign', $campaign->getId())
				->setParameter('assignedAgent', $this->getUser());
		} else {
			// else ask other user show all messages
			$query = $this->getDoctrine()->getEntityManager()
	            ->createQuery(
	                'SELECT u FROM AppBundle:Message u WHERE u.inboxType = :type AND u.campaign = :campaign AND u.messageId is not NULL GROUP BY u.messagesGroup ORDER BY u.messageDate ASC'
	            )
				// ->setParameter('type', Message::INBOX_TYPE_INBOX)
				->setParameter('campaign', $campaign->getId());
		}

		if(empty($type)){
			$query->setParameter('type', Message::INBOX_TYPE_INBOX);
		} else {
			if($type == Message::INBOX_TYPE_OTHER){
				$query->setParameter('type', Message::INBOX_TYPE_OTHER);
			} else {
				$query->setParameter('type', Message::INBOX_TYPE_INBOX);
			}
		}
		// get Messages
		$messages = $query->getResult();

		// get Agents
		$clientAgents = $this->getDoctrine()->getEntityManager()
			->createQuery(
				'SELECT u FROM AppBundle:User u WHERE u.client = :client AND u.typeUser = :typeUser AND u.id != :user'
			)
			->setParameter('client', $campaign->getClient()->getId())
			->setParameter('typeUser', User::TYPE_USER_AGENT)
			->setParameter('user', $this->getUser()->getId())
			->getResult();



		$entityManager = $this->getDoctrine()->getManager();
		$draftsReady = array();
		foreach ($messages as $message) {
			$MessageReply = $entityManager->getRepository(MessageReply::class)->findOneBy(array("messagesGroup" => $message->getMessagesGroup()));
			if($MessageReply){
				$draftsReady[$message->getId()] = $MessageReply->getDraftReady();
			}
		}


		return $this->render("Messages/show.html.twig", [
			"campaign_id" => $campaign->getId(),
			"campaign_name" => $campaign->getName(),
			"messages" => $messages,
			"clientAgents" => $clientAgents,
			"draftsReady" => $draftsReady,
		]);
	}



	/**
	 * @Route("/panel/messages/campaigns/{id}/{type}/{agent}", name="messages_for_agent_campaign_show")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 * @param  User $agent
	 */
	public function showCampaignforAgentAction(Request $request, Campaign $campaign, $type, User $agent) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}
		// if agent ask show him only messages for him
		if($this->getUser()->getTypeUser() == User::TYPE_USER_AGENT){
			$query = $this->getDoctrine()->getEntityManager()
	            ->createQuery(
	                'SELECT u FROM AppBundle:Message u WHERE u.inboxType = :type AND u.assignedAgent = :assignedAgent AND u.campaign = :campaign AND u.messageId is not NULL GROUP BY u.messagesGroup ORDER BY u.messageDate ASC'
	            )
				// ->setParameter('type', Message::INBOX_TYPE_INBOX)
				->setParameter('campaign', $campaign->getId());
				$query->setParameter('assignedAgent', $agent);
		} else {
			// else ask other user show all messages
			$query = $this->getDoctrine()->getEntityManager()
	            ->createQuery(
	                'SELECT u FROM AppBundle:Message u WHERE u.inboxType = :type AND u.campaign = :campaign AND u.messageId is not NULL GROUP BY u.messagesGroup ORDER BY u.messageDate ASC'
	            )
				// ->setParameter('type', Message::INBOX_TYPE_INBOX)
				->setParameter('campaign', $campaign->getId());
		}

		if(empty($type)){
			$query->setParameter('type', Message::INBOX_TYPE_INBOX);
		} else {
			if($type == Message::INBOX_TYPE_OTHER){
				$query->setParameter('type', Message::INBOX_TYPE_OTHER);
			} else {
				$query->setParameter('type', Message::INBOX_TYPE_INBOX);
			}
		}
		// get Messages
		$messages = $query->getResult();

		// get Agents
		$clientAgents = $this->getDoctrine()->getEntityManager()
			->createQuery(
				'SELECT u FROM AppBundle:User u WHERE u.client = :client AND u.typeUser = :typeUser AND u.id != :user'
			)
			->setParameter('client', $campaign->getClient()->getId())
			->setParameter('typeUser', User::TYPE_USER_AGENT)
			->setParameter('user', $this->getUser()->getId())
			->getResult();

		// $entityManager = $this->getDoctrine()->getManager();
		// $clientAgents = $entityManager->getRepository(User::class)->findBy(["client" => $campaign->getClient()->getId(), "typeUser" => User::TYPE_USER_AGENT]);

		return $this->render("Messages/show.html.twig", [
			"campaign_id" => $campaign->getId(),
			"campaign_name" => $campaign->getName(),
			"messages" => $messages,
			"clientAgents" => $clientAgents,
		]);
	}


	/**
	 * @Route("/panel/messages/campaigns/{id}/messages/view/{group}", name="messages_campaign_show_tree")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function showMessagesAction(Request $request, Campaign $campaign, $group) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();

		// get all messages
		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:Message u WHERE u.campaign = :campaign AND u.messagesGroup = :group ORDER BY u.messageDate DESC'
            )
			->setParameter('campaign', $campaign->getId())
			->setParameter('group', $group);

		$messages = $query->getResult();

		// check NumberIn exists in messages
		$numberIn = 0;
		foreach ($messages as $message) {
			if($message->getNumberIn()){
				$numberIn = 1;
			}
		}

		// get Lead from (received message)
		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:Message u WHERE u.campaign = :campaign AND u.messagesGroup = :group AND u.messageId is not NULL'
            )->setMaxResults(1)
			->setParameter('campaign', $campaign->getId())
			->setParameter('group', $group);

		$lead = $query->getResult();


		// FORM TO SEND MESSAGE

		// create new Message object
		$newMessage = new Message();


		// if draft is Ready! - set this auto-reply in form
		$MessageReply = $entityManager->getRepository(MessageReply::class)->findOneBy(array("messagesGroup" => $group));
		if($MessageReply && $MessageReply->getDraftReady() == 1){
			$replyMail = $entityManager->getRepository(KeywordMail::class)->findOneBy(array("id" => $MessageReply->getKeyMailID()));
			$newMessage->setMessageText($replyMail->getMessageText());
		} else {
			// if user Footer is not empty else set company footer
			if(!empty($this->getUser()->getFooter())){
				$newMessage->setMessageText("<p></p><p></p>" . $this->getUser()->getFooter());
			} else {
				// check company Footer
				$companyFooter = $entityManager->getRepository(ClientSettings::class)->findOneBy(array("client" => $this->getUser()->getClient()));
				// add User Footer to Message
				$newMessage->setMessageText("<p></p><p></p>" . $companyFooter->getCompanyFooter());
			}
		}


		// create form to send message
		$form = $this->createForm(MessageType::class, $newMessage);
		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				$messageFrom = $form->get('messageText')->getData();

				// get bot settings from client
				// $botSettings = $entityManager->getRepository(ClientSettings::class)->findOneBy(["client" => $campaign->getClient()]);
				$botSettings = $entityManager->getRepository(ClientSettingsSMTP::class)->findOneBy(["client" => $campaign->getClient()]);
				// check bot settings is not empty
				if(!empty($botSettings->getBotEmail()) && !empty($botSettings->getBotPassword()) && !empty($botSettings->getBotSmtpPort()) && !empty($botSettings->getBotServer()) && !empty($botSettings->getBotSmtpEncryption())){
					// set SwiftMailer to Bot Client Settings and send message
					// set SMTP Connection
					$transport = \Swift_SmtpTransport::newInstance($botSettings->getBotServer(), $botSettings->getBotSmtpPort(), $botSettings->getBotSmtpEncryption())
						->setUsername($botSettings->getBotEmail())
						->setPassword($botSettings->getBotPassword())
					;
					// create mailer from connection
					$mailer = \Swift_Mailer::newInstance($transport);

					// send message to Lead
					$mail_message = \Swift_Message::newInstance()
						->setSubject($campaign->getName())
						->setFrom($this->getParameter('mailer_user'))
						->setTo($lead[0]->getMessageFrom())
						->setBody($messageFrom, 'text/html');
						// ->setBody($messageFrom."<br>".$campaign->getFooter(), 'text/html');
					// $mailer->send($mail_message, $failedRecipients);

					try {
						// send mail
						$mailer->send($mail_message);

						// set fields in message
						$newMessage->setCampaign($campaign);
						$newMessage->setAgent($this->getUser());
						$newMessage->setMessagesGroup($lead[0]->getMessagesGroup());
						$newMessage->setMessageText($messageFrom);
						// $newMessage->setMessageText($messageFrom."\n\r".$campaign->getFooter());
						$newMessage->setMessageDate(new \DateTime());
						$newMessage->setMessageFrom($this->getParameter('mailer_user'));
						$newMessage->setMessageTo($lead[0]->getMessageFrom());
						$newMessage->setInboxType(Message::INBOX_TYPE_INBOX);
						$newMessage->setStatus(Message::STATUS_SENT);
						// save to database
						$entityManager->persist($newMessage);
						// clean database
						$entityManager->flush();

			            $this->addFlash("success", "Wysłano wiadomość do Lead'a");
					}
					catch (\Swift_TransportException $e) {
						$this->addFlash("error", "Problem z wysyłką maila");
					}

				} else {
					$this->addFlash("error", "Problem z ustawieniami Bota");
				}

				return $this->redirectToRoute("messages_campaign_show_tree", ["id" => $campaign->getId(), "group" => $group]);
	        }
			// add flash with error
			$this->addFlash("error", "Nie udało się wysłać wiadomości");
		}

		// END FORM

		return $this->render("Messages/messages.html.twig", [
			"lead_email" => $lead[0]->getMessageFrom(),
			"campaign_id" => $campaign->getId(),
			"campaign_name" => $campaign->getName(),
			"messages" => $messages,
			"form" => $form->createView(),
			"numberIn" => $numberIn,
			"groupId" => $group,
		]);
	}

	/**
	 * @Route("/panel/messages/campaigns/{id}/messages/view/{group}/direct", name="messages_campaign_consultant_direct")
	 * @param  Request $request
	 * @param  Campaign $campaign
	 */
	public function directConsultantAction(Request $request, RetriverDataService $retriverData, Campaign $campaign, $group) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getClient() != $campaign->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// get all messages
		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:Message u WHERE u.campaign = :campaign AND u.messagesGroup = :group ORDER BY u.messageDate DESC'
            )
			->setParameter('campaign', $campaign->getId())
			->setParameter('group', $group);

		$messages = $query->getResult();

		// check NumberIn exists in messages
		$getNumberIn = '';
		// try to get Lead Email too
		$getLeadEmail = '';
		foreach ($messages as $message) {
			if($message->getStatus() == Message::STATUS_SENT){
				$getLeadEmail = $message->getMessageTo();
			}
			if($message->getNumberIn()){
				$getNumberIn = $retriverData->checkPhoneNumberIn($message->getMessageText())[0];
			}
		}
		// get consultants
		$entityManager = $this->getDoctrine()->getManager();
		// get bot settings from client
		$botSettings = $entityManager->getRepository(ClientSettings::class)->findOneBy(["client" => $this->getUser()->getClient()]);
		// create new var for Direct Consultant to message
		$directConsultant = "";
		// get campaign for consultants
		$campaignConsultants = $entityManager->getRepository(Campaign::class)->findOneBy(array("id" => $campaign->getId(), "client" => $this->getUser()->getClient()));
		// get consultants from campaign join table
		$campaignConsultants = $campaignConsultants->getConsultant();

		// check consultants, which is not OFF and set this
		$clientConsultants = array();
		// check consultant is OFF from receive messages - if no, add him to list
		foreach ($campaignConsultants as $key => $consultant) {
			if($consultant->getOffUser() == Consultant::OFF_USER_NO){
				$clientConsultants[] = $consultant;
			}
		}
		$campaignConsultants = $clientConsultants;


		// check consultants are only 1 or more
		$howConsultant = count($campaignConsultants);
		if($howConsultant > 1){
			// if last direct consultant field is not empty
			if(!empty($botSettings->getLastDirectConsultant())){
				// for all consultants check which was directed last
				foreach ($campaignConsultants as $key => $consultant) {
					// if this consultant was last
					if($botSettings->getLastDirectConsultant() == $consultant){
						// check which is this consultant in list of consultants
						if($key == $howConsultant-1){
							// if this consultant was last - direct first consultant
							$directConsultant = $campaignConsultants[0];
						} else {
							// if was first or in middle of bigger list direct next
							$directConsultant = $campaignConsultants[$key+1];
						}
					}
				}
				// if actuall consultants are not in list of last assigned set first
				if(empty($directConsultant)){
					$directConsultant = $campaignConsultants[0];
				}
			} else {
				// set first consultant to this message
				$directConsultant = $campaignConsultants[0];
			}

		} elseif($howConsultant == 1){
			// set this consultant to this message
			$directConsultant = $campaignConsultants[0];
		} else {
			// not send - not consultants available
			$this->addFlash("error", "Brak dostępnych konsultantów!");
		}
		// if consultant was direct from list
		if(!empty($directConsultant)){
			// save to database last directed consultant for client
			$botSettings->setLastDirectConsultant($directConsultant);
			$entityManager->persist($botSettings);
			// clean database
			$entityManager->flush();

			// create message
			$message = \Swift_Message::newInstance()
				->setSubject('Przekazanie Leada')
		        ->setFrom($this->getParameter('mailer_user'))
		        ->setTo($directConsultant->getEmail())
		        ->setBody(
		            $this->renderView(
		                'Messages/directConsultant.html.twig',
		                array('getNumberIn' => $getNumberIn, 'messages' => $messages)
		            ),
		            'text/html'
		        );

			try {
				// check this Lead was direct
				$directLeads = $entityManager->getRepository(DirectLead::class)->findBy(array("campaign" => $campaign->getId(), "directLead" => $getLeadEmail));
				if(count($directLeads) > 0){
					$this->addFlash("error", "Ktoś wcześniej przekazał tego Leada do konsultanta");
				} else {
					// send mail
					$this->get('mailer')->send($message);

					// save this Direct to database DirectLead table

					// create new DirectLead
					$directLead = new DirectLead();
					// set fields in this directLead
					$directLead->setCampaign($campaign);
					$directLead->setDirectDate(new \DateTime());
					$directLead->setDirectWho($this->getUser()->getEmail());
					$directLead->setDirectLead($getLeadEmail);
					$directLead->setDirectTo($directConsultant->getEmail());
					// save to database
					$entityManager->persist($directLead);
					// clean database
					$entityManager->flush();

					$this->addFlash("success", "Przekazano do konsultanta");
				}
			}
			catch (\Swift_TransportException $e) {
				$this->addFlash("error", "Nie udało się przekazać do konsultanta");
			}
		}
		return $this->redirectToRoute("messages_campaign_show_tree", ["id" => $campaign->getId(), "group" => $group]);
	}

}
