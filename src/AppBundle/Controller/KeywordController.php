<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\KeywordMail;
use AppBundle\Entity\Keyword;
use AppBundle\Entity\Campaign;

use AppBundle\Form\KeywordType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Collections\ArrayCollection;

class KeywordController extends Controller
{

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/list", name="keyword_index")
	 * @param  Request $request
	 * @param Campaign $campaign
	 */
	public function indexAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		$keyword = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => null, "campaign" => $campaign, "client" => $this->getUser()->getClient()));

		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:Campaign u WHERE u.client = :client AND u.type != :type'
            )->setParameter('client', $this->getUser()->getClient())
			->setParameter('type', Campaign::TYPE_DELETED);
		$campaigns = $query->getResult();

		return $this->render("Keyword/index.html.twig", [
			"keyword" => $keyword,
			"campaign" => $campaign,
			"campaigns" => $campaigns
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/add", name="keyword_add")
	 * @param  Request $request
	 * @param Campaign $campaign
	 */
	public function addAction(Request $request, Campaign $campaign) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create new Keyword object
		$keyword = new Keyword();

		// create form to import
		$form = $this->createForm(KeywordType::class, $keyword);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {
				// find Client
				$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
				// add Client to Keyword
				$keyword->setClient($user);
				// add Campaign
				$keyword->setCampaign($campaign);
				// save to database
	            $entityManager->persist($keyword);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Słowo kluczowe dodane poprawnie.");
				return $this->redirectToRoute("keyword_index", ["campaign" => $campaign->getId()]);

	        }
			// add flash with error
			$this->addFlash("error", "Słowo kluczowe nie zostało dodane");
		}
		return $this->render("Keyword/add.html.twig", [
			"form" => $form->createView(),
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/edit/{id}", name="keyword_edit")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 */
	public function editAction(Request $request, Campaign $campaign, Keyword $keyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create form to import from Campaign
		$form = $this->createForm(KeywordType::class, $keyword);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {
				// save to database
	            $entityManager->persist($keyword);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Słowo kluczowe edytowane poprawnie.");
				return $this->redirectToRoute("keyword_index", ["campaign" => $campaign->getId()]);

	        }
			// add flash with error
			$this->addFlash("error", "Słowo kluczowe nie zostało edytowane");
		}
		return $this->render("Keyword/edit.html.twig", [
			"form" => $form->createView(),
			"campaign" => $campaign
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/delete/{id}", name="keyword_delete")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param  Keyword $keyword
	 */
	public function deleteAction(Request $request, Campaign $campaign, Keyword $keyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $keyword->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();

		// find and delete all subKeywords from db
		$subKeys = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $keyword->getId()));
		foreach ($subKeys as $key => $sub) {
			$entityManager->remove($sub);
		}

		// remove Keyword form database
		$entityManager->remove($keyword);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Słowo kluczowe");
		// return to keyword_index
		return $this->redirectToRoute("keyword_index", ["campaign" => $campaign->getId()]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/subs/{id}", name="keyword_subs")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param  Keyword $keyword
	 */
	public function subsAction(Request $request, Campaign $campaign, Keyword $keyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		$subsKeyword = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $keyword->getId(), "client" => $this->getUser()->getClient()));

		return $this->render("Keyword/subs.html.twig", [
			"subsKeyword" => $subsKeyword,
			"mainKeyword" => $keyword->getId(),
			"mainKeywordTitle" => $keyword->getName(),
			"campaign" => $campaign
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/subs/{id}/add", name="keyword_addSub")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param  Keyword $mainKeyword
	 */
	public function addSubAction(Request $request, Campaign $campaign, Keyword $mainKeyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create new Keyword object
		$keyword = new Keyword();

		// create form to import
		$form = $this->createForm(KeywordType::class, $keyword);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

                // check this subKeyword exists in database
				$query = $this->getDoctrine()->getEntityManager()
		            ->createQuery(
		                'SELECT u FROM AppBundle:Keyword u WHERE u.name = :name AND u.client = :client AND u.campaign = :campaign AND u.baseParent IS NOT NULL'
		            )->setParameter('name', $form->get('name')->getData())
					->setParameter('client', $this->getUser()->getClient())
					->setParameter('campaign', $campaign->getId());

				$kyExists = $query->getResult();
                if(count($kyExists) > 0){
                    $this->addFlash("error", "Pod-słowo o takiej nazwie już istnieje");
                } else {
					// find Client
					$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
					// add Client to Keyword
					$keyword->setClient($user);
					// add Campaign
					$keyword->setCampaign($campaign);
					// add baseParent
					$keyword->setBaseParent($mainKeyword->getId());
					// save to database
		            $entityManager->persist($keyword);
					// clean entityManager
		            $entityManager->flush();
		            // add Flash with success
		            $this->addFlash("success", "Pod-Słowo kluczowe dodane poprawnie.");
					return $this->redirectToRoute("keyword_subs", ["campaign" => $campaign->getId(), "id" => $mainKeyword->getId()]);
				}
	        } else {
				// add flash with error
				$this->addFlash("error", "Pod-Słowo nie zostało dodane");
			}
		}
		return $this->render("Keyword/add.html.twig", [
			"form" => $form->createView(),
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/ajaxsubs/{id}", name="keyword_ajax_subs")
	 * @Method("POST")
	 * @param Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 */
	public function ajaxSybsAction(Request $request, Campaign $campaign, Keyword $keyword)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
        // check request for using AJAX
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

		// get subs keywords from KeyWords
		$entityManager = $this->getDoctrine()->getManager();
		$subsKeyword = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $keyword->getId(), "client" => $this->getUser()->getClient()));
		if($subsKeyword){
			$keys = [];
			foreach ($subsKeyword as $item) {
				$key = [];
				$key['id'] = $item->getId();
				$key['name'] = $item->getName();
				$key['baseParent'] = $item->getBaseParent();
				$keys[] = $key;
			}

			// return success
			return new JsonResponse(array('message' => json_encode($keys, JSON_UNESCAPED_UNICODE)), 200);
		}
		// return error
		return new JsonResponse(array('message' => 'error'), 400);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/ajaxsubsmore/{keyword}/{keywordMail}", name="keyword_ajax_subsmore")
	 * @Method("POST")
	 * @param Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 * @param KeywordMail $keywordMail
	 */
	public function ajaxSubsMoreAction(Request $request, Campaign $campaign, Keyword $keyword, KeywordMail $keywordMail)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
        // check request for using AJAX
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
        }

		// get subs keywords from KeyWords
		$entityManager = $this->getDoctrine()->getManager();
		$subsKeyword = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $keyword->getId(), "client" => $this->getUser()->getClient()));

		// get selected keywords from join table
		$keywordMailKeyword = $keywordMail->getKeywords();

		if($subsKeyword){
			$all = [];
			$all['subs'] = [];
			$all['selected'] = [];
			foreach ($subsKeyword as $item) {
				$key = [];
				$key['id'] = $item->getId();
				$key['name'] = $item->getName();
				$key['baseParent'] = $item->getBaseParent();
				$all['subs'][] = $key;
			}
			foreach ($keywordMailKeyword as $item) {
				$all['selected'][] = $item->getId();
			}

			// return success
			return new JsonResponse(array('message' => json_encode($all, JSON_UNESCAPED_UNICODE)), 200);
		}
		// return error
		return new JsonResponse(array('message' => 'error'), 400);
	}


	/**
	 * @Route("/panel/keyword/ajaxcopy", name="keyword_ajax_copy")
	 * @Method("POST")
	 * @param Request $request
	 */
	public function ajaxCopyAction(Request $request)
	{
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// check request for using AJAX
		if (!$request->isXmlHttpRequest()) {
			return new JsonResponse(array('message' => 'You can access this only using Ajax!'), 400);
		}

		$content = $request->getContent();
        if (!empty($content)) {
            $data = json_decode($content, true);
			// get parameters
			$selectedItems = $data['selectedItems'];
			$campaignTo = $data['campaignTo'];

			if(!empty($selectedItems) && !empty($campaignTo)){

				// get subs keywords from KeyWords
				$entityManager = $this->getDoctrine()->getManager();
				// check exist campaignTo
				$checkCampaignToExists = $entityManager->getRepository(Campaign::class)->findOneBy(array("id" => $campaignTo));
				if(count($checkCampaignToExists) > 0){

					foreach ($selectedItems as $key => $value) {
						$keywordOriginal = $entityManager->getRepository(Keyword::class)->findOneBy(array("id" => $value));
						if(count($keywordOriginal) > 0){

							// General Keyword

							// duplicate this keyword to other campaign
							$duplicat = clone $keywordOriginal;
							// change campaign for selected
							$duplicat->setCampaign($checkCampaignToExists);
							// save to database
							$entityManager->persist($duplicat);
							// clean EntityManager
							$entityManager->flush();


							// SubKeywords
							$subKeys = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $value));
							if(count($subKeys) > 0){
								foreach ($subKeys as $itemSub) {
									// duplicate this SubKeyword to other campaign
									$subDuplicat = clone $itemSub;
									// change campaign for selected
									$subDuplicat->setCampaign($checkCampaignToExists);
									// change baseParent
									$subDuplicat->setBaseParent($duplicat->getId());
									// save to database
									$entityManager->persist($subDuplicat);
									// clean EntityManager
									$entityManager->flush();
								}
							}

							$subKeysArray = $entityManager->getRepository(Keyword::class)->findBy(array("baseParent" => $duplicat->getId()));

							// Mails
							$mailsKey = $entityManager->getRepository(KeywordMail::class)->findBy(array("keyword" => $keywordOriginal->getId()));
							if(count($mailsKey) > 0){
								foreach ($mailsKey as $itemMail) {
									// duplicate this SubKeyword to other campaign
									$mailDuplicat = clone $itemMail;
									// change keyword ID
									$mailDuplicat->setKeyword($duplicat);
									// save to database
									$entityManager->persist($mailDuplicat);
									// clean EntityManager
									$entityManager->flush();



									// get this mail after added to db
									$thisMail = $entityManager->getRepository(KeywordMail::class)->findOneBy(array("id" => $mailDuplicat->getId()));
									// get all Keywords and delete them
									foreach ($thisMail->getKeywords() as $stuffDel) {
										$thisMail->getKeywords()->removeElement($stuffDel);
									}
									// check old subkeywords in mail and check them again in new
									foreach ($subKeysArray as $item) {
										$keyIses = $itemMail->getKeywords();
										if(!empty($keyIses)){
											foreach ($keyIses as $stuffX) {
												if($stuffX->getName() == $item->getName()){
													$thisMail->setKeywords($item);
												}
											}
										}
									}
									// save to database
									$entityManager->persist($thisMail);
									// clean EntityManager
									$entityManager->flush();

								}
							}
						}
					}
				}
				// return success
				return new JsonResponse(array('message' => json_encode("success", JSON_UNESCAPED_UNICODE)), 200);
			}
		}
		// return error
		return new JsonResponse(array('message' => 'error'), 400);
	}

}
