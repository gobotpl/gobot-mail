<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\KeywordMail;
use AppBundle\Entity\Keyword;
use AppBundle\Entity\Campaign;

use AppBundle\Form\KeywordMailType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class KeywordMailController extends Controller
{

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/mails/{id}", name="keywordMail_index")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 */
	public function indexAction(Request $request, Campaign $campaign, Keyword $keyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		$keywordMails = $entityManager->getRepository(KeywordMail::class)->findBy(array("keyword" => $keyword->getId()));

		return $this->render("KeywordMail/index.html.twig", [
			"keywordMails" => $keywordMails,
			"mainKeyword" => $keyword->getId(),
			"mainKeywordTitle" => $keyword->getName(),
			"campaign" => $campaign
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/mails/{id}/add", name="keywordMail_add")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 */
	public function addAction(Request $request, Campaign $campaign, Keyword $keyword) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create new KeywordMail object
		$keywordMail = new KeywordMail();

		// create form to import
		$form = $this->createForm(KeywordMailType::class, $keywordMail);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {
				$keywordMail->setKeyword($keyword);
				// save to database
	            $entityManager->persist($keywordMail);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Mail dodany poprawnie.");
				return $this->redirectToRoute("keywordMail_index", ["campaign" => $campaign->getId(), "id" => $keyword->getId()]);

	        }
			// add flash with error
			$this->addFlash("error", "Mail nie został dodany");
		}
		return $this->render("KeywordMail/add.html.twig", [
			"form" => $form->createView(),
			"keyId" => $keyword->getId(),
			"mailId" => $keywordMail->getId(),
			"campaign" => $campaign
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/mails/{keyword}/edit/{keywordMail}", name="keywordMail_edit")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 * @param KeywordMail $keywordMail
	 */
	public function editAction(Request $request, Campaign $campaign, Keyword $keyword, KeywordMail $keywordMail) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		// run entityManager
		$entityManager = $this->getDoctrine()->getManager();

		// create form to import from Campaign
		$form = $this->createForm(KeywordMailType::class, $keywordMail);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {
				// save to database
	            $entityManager->persist($keywordMail);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Mail edytowany poprawnie.");
				return $this->redirectToRoute("keywordMail_index", ["campaign" => $campaign->getId(), "id" => $keyword->getId()]);

	        }
			// add flash with error
			$this->addFlash("error", "Mail nie został edytowany");
		}
		return $this->render("KeywordMail/edit.html.twig", [
			"form" => $form->createView(),
			"keyId" => $keyword->getId(),
			"mailId" => $keywordMail->getId(),
			"campaign" => $campaign
		]);
	}

	/**
	 * @Route("/panel/campaign/{campaign}/keyword/mails/{keyword}/delete/{keywordMail}", name="keywordMail_delete")
	 * @param  Request $request
	 * @param Campaign $campaign
	 * @param Keyword $keyword
	 * @param  KeywordMail $keywordMail
	 */
	public function deleteAction(Request $request, Campaign $campaign, Keyword $keyword, KeywordMail $keywordMail) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		if($this->getUser()->getClient() != $keyword->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// remove Keyword form database
		$entityManager = $this->getDoctrine()->getManager();
		$entityManager->remove($keywordMail);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się usunąć Mail");
		// return to keywordMail_index
		return $this->redirectToRoute("keywordMail_index", ["campaign" => $campaign->getId(), "id" => $keyword->getId()]);
	}

}
