<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\Consultant;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PanelController extends Controller
{

	/**
	 * @Route("/panel", name="panel_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");

		if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
			return $this->render("Panel/index.html.twig", []);
		} else {

			// if agent or KP show index - but if client check mailbase and redirect
			if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT || $this->getUser()->getTypeUser() == USER::TYPE_USER_KP){
				return $this->render("Panel/index.html.twig", []);
			} else {
				$entityManager = $this->getDoctrine()->getManager();
				$mailbase = $entityManager->getRepository(LeadImport::class)->findBy(array("client" => $this->getUser()->getClient()));
				if(count($mailbase) > 0){
					return $this->render("Panel/index.html.twig", []);
				} else {
					return $this->redirectToRoute("import_index");
				}
			}
		}
	}

	/**
	 * @Route("/panel/client/users", name="client_users")
	 * @param  Request $request
	 * @return Response
	 */
	public function usersAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();

		$kps = $entityManager->getRepository(User::class)->findBy(array("client" => $this->getUser()->getClient(), "typeUser" => USER::TYPE_USER_KP));
		$agents = $entityManager->getRepository(User::class)->findBy(array("client" => $this->getUser()->getClient(), "typeUser" => USER::TYPE_USER_AGENT));
		$consultants = $entityManager->getRepository(Consultant::class)->findBy(array("client" => $this->getUser()->getClient()));

		$dF_kps = array();
		foreach ($kps as $item) {
			$dF = $this->createFormBuilder()
				->setAction($this->generateUrl("user_kp_delete", ["id" => $item->getId()]))
				->setMethod(Request::METHOD_DELETE)
				->add("submit", SubmitType::class, ["label" => "Usuń", "attr" => ["onclick" => "return confirm('Czy na pewno chcesz usunąć?');"]])
				->getForm();
			$dF_kps[$item->getId()] = $dF->createView();
		}

		$dF_agents = array();
		foreach ($agents as $item) {
			$dF = $this->createFormBuilder()
				->setAction($this->generateUrl("user_delete", ["id" => $item->getId()]))
				->setMethod(Request::METHOD_DELETE)
				->add("submit", SubmitType::class, ["label" => "Usuń", "attr" => ["onclick" => "return confirm('Czy na pewno chcesz usunąć?');"]])
				->getForm();
			$dF_agents[$item->getId()] = $dF->createView();
		}

		$dF_consultants = array();
		foreach ($consultants as $item) {
			$dF = $this->createFormBuilder()
				->setAction($this->generateUrl("consultant_delete", ["id" => $item->getId()]))
				->setMethod(Request::METHOD_DELETE)
				->add("submit", SubmitType::class, ["label" => "Usuń", "attr" => ["onclick" => "return confirm('Czy na pewno chcesz usunąć?');"]])
				->getForm();
			$dF_consultants[$item->getId()] = $dF->createView();
		}

		return $this->render("Panel/Client/users.html.twig", [
			"kps" => $kps,
			"agents" => $agents,
			"consultants" => $consultants,
			"dF_kps" => $dF_kps,
			"dF_agents" => $dF_agents,
			"dF_consultants" => $dF_consultants,
		]);
	}

	/**
	 * @Route("/panel/admin/users", name="admin_users")
	 * @param  Request $request
	 * @return Response
	 */
	public function adminUsersAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.roles NOT LIKE :notRole'
            )->setParameter('role', '%"ROLE_CLIENT"%')
			->setParameter('notRole', '%"ROLE_ADMIN"%');
		$clients = $query->getResult();

		return $this->render("Panel/Admin/adminUsers.html.twig", [
			"clients" => $clients
		]);
	}

	/**
	 * @Route("/panel/admin/clientsusers", name="admin_clientsusers")
	 * @param  Request $request
	 * @return Response
	 */
	public function adminClientsUsersAction(Request $request) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

		$query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role AND u.roles NOT LIKE :notRole'
            )->setParameter('role', '%"ROLE_CLIENT"%')
			->setParameter('notRole', '%"ROLE_ADMIN"%');
		$clients = $query->getResult();

		$entityManager = $this->getDoctrine()->getManager();

		$kps = [];
		foreach ($clients as $client) {
			$kps[$client->getId()] = $entityManager->getRepository(User::class)->findBy(array("client" => $client->getId(), "typeUser" => USER::TYPE_USER_KP));
		}

		return $this->render("Panel/Admin/adminClientsUsers.html.twig", [
			"clients" => $clients,
			"kps" => $kps,
		]);
	}


	/**
	 * @Route("/panel/admin/clientsusers/active/{id}", name="admin_clientsusers_active")
	 * @param  Request $request
	 * @param  User $user
	 */
	public function activeAction(Request $request, User $user) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

		// active User can send reply for Lead in database
		$entityManager = $this->getDoctrine()->getManager();
		$user->setMessagesReply(User::MESSAGES_REPLY_ACTIVE);
		$entityManager->persist($user);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Aktywowano wysyłanie odpowiedzi");
		// return to list of clientsusers
		return $this->redirectToRoute("admin_clientsusers");
	}

	/**
	 * @Route("/panel/admin/clientsusers/stop/{id}", name="admin_clientsusers_stop")
	 * @param  Request $request
	 * @param  User $user
	 */
	public function stopAction(Request $request, User $user) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

		// stop User can send reply for Lead in database
		$entityManager = $this->getDoctrine()->getManager();
		$user->setMessagesReply(User::MESSAGES_REPLY_STOP);
		$entityManager->persist($user);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Wstrzymano wysyłanie odpowiedzi");
		// return to list of clientsusers
		return $this->redirectToRoute("admin_clientsusers");
	}

}
