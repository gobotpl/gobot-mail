<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Lead;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\LeadImport;
use AppBundle\Form\LeadImportType;
use AppBundle\Form\LeadImportEditType;
use AppBundle\Service\RetriverDataService;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\File;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class LeadImportController extends Controller
{

	/**
	 * @Route("/panel/mailbase", name="mailbase_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function indexAction(Request $request) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		$entityManager = $this->getDoctrine()->getManager();
		$mailbase = $entityManager->getRepository(LeadImport::class)->findBy(array("client" => $this->getUser()->getClient(), "typeBase" => LeadImport::TYPE_BASE_AVAILABLE), array('name' => 'ASC'));
		$mailbase_archived = $entityManager->getRepository(LeadImport::class)->findBy(array("client" => $this->getUser()->getClient(), "typeBase" => LeadImport::TYPE_BASE_ARCHIVED), array('name' => 'ASC'));

		$deleteForms = array();
		foreach ($mailbase as $item) {

			$campaigns = $entityManager->getRepository(Campaign::class)->findBy(array("lead_import" => $item->getId(), "client" => $this->getUser()->getClient(), "type" => Campaign::TYPE_ACTIVE));
			if(count($campaigns) > 0){
				$deleteForms[$item->getId()] = "none";
			} else {
				$deleteForm = $this->createFormBuilder()
					->setAction($this->generateUrl("import_delete", ["id" => $item->getId()]))
					->setMethod(Request::METHOD_DELETE)
					->add("submit", SubmitType::class, ["label" => "Archiwizuj"])
					->getForm();
				$deleteForms[$item->getId()] = $deleteForm->createView();
			}
		}

		return $this->render("Mailbase/index.html.twig", [
			"mailbase" => $mailbase,
			"mailbase_archived" => $mailbase_archived,
			"deleteForms" => $deleteForms,
		]);
	}

	/**
	 * @Route("/panel/mailbase/{id}", name="mailbase_show")
	 * @param  Request $request
	 * @param LeadImport $leadImport
	 */
	public function showAction(Request $request, RetriverDataService $retriverData, LeadImport $leadImport) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $leadImport->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$labels = $retriverData->getLabelsData($leadImport->getId());
		$leads = $this->getLeadData($leadImport->getId());

		$deleteForms = array();
		foreach ($leads as $lead) {
			$deleteForm = $this->createFormBuilder()
				->setAction($this->generateUrl("lead_delete", ["id" => $lead['id']]))
				->setMethod(Request::METHOD_DELETE)
				->add("LeadImportId", HiddenType::class, array('data' => $leadImport->getId()))
				->add("submit", SubmitType::class, ["label" => "Usuń"])
				->getForm();
			$deleteForms[$lead['id']] = $deleteForm->createView();
		}

		return $this->render("Mailbase/show.html.twig", [
			"labels" => $labels,
			"leads" => $leads,
			"deleteForms" => $deleteForms,
			"leadImportName" => $leadImport->getName(),
			"leadImportId" => $leadImport->getId(),
			"leadImportType" => $leadImport->getType(),
		]);
	}

	/**
	 * @Route("/panel/mailbase/{id}/add", name="lead_add")
	 * @param  Request $request
	 * @param LeadImport $leadImport
	 */
	public function addAction(Request $request, RetriverDataService $retriverData, LeadImport $leadImport) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $leadImport->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// labels for form generate
		$labels = $retriverData->getLabelsData($leadImport->getId());

		// create new Lead object
		$lead = new Lead();
		// create form to add Lead
		$form = $this->createFormBuilder($lead)
			->setAction($this->generateUrl("lead_add", ["id" => $leadImport->getId()]))
			->setMethod(Request::METHOD_POST)
			->getForm();
		// generate date format
		$data_labels = implode('{label}', array_map(function ($entry) {
			return implode('[labelData]', $entry);
		}, $labels));
		// add to hidden form value for labels fields
		$form->add('labels', HiddenType::class, ["label" => "Etykiety", "data" => $data_labels]);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				// get data from form
				$labels = $form->get('labels')->getData();
				// convert to database format
				$data_labels = "";
				$which = 0;
				foreach ($labels as $key => $value) {
					if($which == 0){
						$data_labels .= $key."[labelData]".$value;
					} else {
						$data_labels .= "{label}".$key."[labelData]".$value;
					}
					$which++;
				}
				// save in database
				$entityManager = $this->getDoctrine()->getManager();
				// save labels for this Lead
				$lead->setLabels($data_labels);
				// add LeadImport
				$lead->setLeadImport($leadImport);
				// save to database
	            $entityManager->persist($lead);
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Udało się dodać Lead'a.");
				return $this->redirectToRoute("mailbase_show", array('id' => $leadImport->getId()));
	        }
			// add flash with error
			$this->addFlash("error", "Nie powiodło się dodawanie Lead'a.");
		}
		return $this->render("Lead/add.html.twig", ["form" => $form->createView()]);
	}

	/**
	 * @Route("/panel/mailbase/{id}/edit/{lead}", name="lead_edit")
	 * @param  Request $request
	 * @param LeadImport $leadImport
	 * @param Lead $lead
	 */
	public function editAction(Request $request, RetriverDataService $retriverData, LeadImport $leadImport, Lead $lead) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $leadImport->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// labels for form generate
		$labels = $retriverData->getLabelsData($leadImport->getId());
		// labels data from Lead
		$data_lead_labels = $lead->getLabels();

		// create form to edit Lead
		$form = $this->createFormBuilder($lead)
			->setAction($this->generateUrl("lead_edit", ["id" => $leadImport->getId(), "lead" => $lead->getId()]))
			->setMethod(Request::METHOD_POST)
			->getForm();
		// generate date format
		$data_labels = implode('{label}', array_map(function ($entry) {
			return implode('[labelData]', $entry);
		}, $labels));
		// add to hidden form value for labels fields
		$form->add('labels', HiddenType::class, ["label" => "Etykiety", "data" => $data_labels]);
		// add hidden field with values of labels Lead
		$form->add('labels_data', HiddenType::class, ["mapped" => false, "data" => $data_lead_labels]);

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				// get data from form
				$labels = $form->get('labels')->getData();
				if(!empty($labels)){
					// convert to database format
					$data_labels = "";
					$which = 0;
					foreach ($labels as $key => $value) {
						if($which == 0){
							$data_labels .= $key."[labelData]".$value;
						} else {
							$data_labels .= "{label}".$key."[labelData]".$value;
						}
						$which++;
					}
					// save in database
					$entityManager = $this->getDoctrine()->getManager();
					// save labels for this Lead
					$lead->setLabels($data_labels);
					// save to database
		            $entityManager->persist($lead);
					// clean entityManager
		            $entityManager->flush();
		            // add Flash with success
		            $this->addFlash("success", "Udało edytować Lead'a.");
				} else {
					$this->addFlash("error", "Uzupełnij Etykiety");
   				}
				return $this->redirectToRoute("mailbase_show", array('id' => $leadImport->getId()));
	        }
			// add flash with error
			$this->addFlash("error", "Nie powiodło się edytowanie Lead'a.");
		}
		return $this->render("Lead/edit.html.twig", ["form" => $form->createView()]);
	}


	/**
	 * @Route("/panel/import/edit/{id}", name="import_edit")
	 * @param  Request $request
	 * @param LeadImport $lead_import
	 */
	public function importEditAction(Request $request, LeadImport $lead_import) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $lead_import->getClient()->getId()){
			throw new AccessDeniedException();
		}

		$form = $this->createForm(LeadImportEditType::class, $lead_import);

		if($request->isMethod("POST")){
			$form->handleRequest($request);

			if($form->isValid()){

				// get data from form
				$labels = $form->get('labels')->getData();
				if(!empty($labels)){
					$entityManager = $this->getDoctrine()->getManager();

					// implode data to database format
					$data_labels = implode('{label}', array_map(function ($entry) {
						return implode('[labelData]', $entry);
					}, $labels));
					// save in database
					$lead_import->setLabels($data_labels);

					// check is normal and change name for test LeadImports
					if($lead_import->getType() == LeadImport::TYPE_NORMAL){
						// get duplicates and change names
						$duplicates = $entityManager->getRepository(LeadImport::class)->findBy(array("type" => LeadImport::TYPE_TEST, "baseParent" => $lead_import->getId()));
						foreach ($duplicates as $key => $duplicat) {
							$number = explode("#", $duplicat->getName());
							$number = $number[count($number)-1];
							// set name
							$duplicat->setName($form->get('name')->getData()." - test #".$number);
							$entityManager->persist($duplicat);
						}
					}

					$entityManager->persist($lead_import);
					$entityManager->flush();

					$this->addFlash("success", "Edycja wykonana poprawnie");
				} else {
					$this->addFlash("error", "Uzupełnij Etykiety");
   				}
				return $this->redirectToRoute("mailbase_index");
			}
			$this->addFlash("error", "Nie powiodła się edycja");
		}

		return $this->render("LeadImport/edit.html.twig", [
			"form" => $form->createView(),
			"baseType" => $lead_import->getType()
		]);

	}

	/**
	 * @Route("/panel/import/delete/{id}", name="import_delete", methods={"DELETE"})
	 * @param  Request $request
	 * @param  LeadImport $lead
	 */
	public function deleteAction(Request $request, LeadImport $leadImport) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $leadImport->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// remove LeadImport form database - only archive
		$entityManager = $this->getDoctrine()->getManager();
		$leadImport->setTypeBase(LeadImport::TYPE_BASE_ARCHIVED);

		// find and delete all test LeadImports for this LeadImport original
		$duplicates = $entityManager->getRepository(LeadImport::class)->findBy(array("type" => LeadImport::TYPE_TEST, "baseParent" => $leadImport->getId()));
		foreach ($duplicates as $key => $duplicat) {
			$entityManager->remove($duplicat);
		}

		$entityManager->persist($leadImport);
		// $entityManager->remove($leadImport);
		$entityManager->flush();
		// show Flash success
		// $this->addFlash("success", "Udało się usunąć Bazę mailingową");
		$this->addFlash("success", "Udało się zarchiwizować Bazę mailingową");
		// return to mailbase_show
		return $this->redirectToRoute("mailbase_index");
	}

	/**
	 * @Route("/panel/import/unarchive/{id}", name="import_unarchive")
	 * @param  Request $request
	 * @param LeadImport $lead_import
	 */
	public function unarchiveAction(Request $request, LeadImport $leadImport) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $leadImport->getClient()->getId()){
			throw new AccessDeniedException();
		}

		// unarchive LeadImport in database
		$entityManager = $this->getDoctrine()->getManager();
		$leadImport->setTypeBase(LeadImport::TYPE_BASE_AVAILABLE);
		$entityManager->persist($leadImport);
		$entityManager->flush();
		// show Flash success
		$this->addFlash("success", "Udało się przywrócić Bazę mailingową");
		// return to mailbase_show
		return $this->redirectToRoute("mailbase_index");
	}

	/**
	 * @Route("/panel/import/test/{id}", name="import_test_create")
	 * @param  Request $request
	 * @param LeadImport $lead_import
	 */
	public function importTestAction(Request $request, LeadImport $lead_import) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $lead_import->getClient()->getId()){
			throw new AccessDeniedException();
		}

		if($lead_import->getType() == LeadImport::TYPE_TEST){
			$this->addFlash("error", "Nie powiodło się tworzenie bazy testowej");
		} else {
			$entityManager = $this->getDoctrine()->getManager();
			// count duplicates and set next
			$checkDuplicates = $entityManager->getRepository(LeadImport::class)->findBy(array("type" => LeadImport::TYPE_TEST, "baseParent" => $lead_import->getId()));

			// duplicate with custom name, date, type = test
			$duplicat = clone $lead_import;
			$duplicat->setName($lead_import->getName()." - test #".count($checkDuplicates));
			$duplicat->setType(LeadImport::TYPE_TEST);
			$duplicat->setTypeBase(LeadImport::TYPE_BASE_AVAILABLE);
			$duplicat->setDateOfImport(new \DateTime());
			// add BaseParent to Duplicate LeadImport
			$duplicat->setBaseParent($lead_import->getId());
			$entityManager->persist($duplicat);
			// $entityManager->flush();
			// create to 5 leads from parent - and count leads to 5
			$leadNum = 0;
			$leads = $entityManager->getRepository(Lead::class)->findBy(array("lead_import" => $duplicat->getId()), array('id' => 'ASC'), 5);
			foreach ($leads as $lead) {
				$duplicat_lead = clone $lead;
				$duplicat_lead->setLeadImport($duplicat);
				// change email address in all duplcate Leads
				$leadData = $this->getLeadData($duplicat->getId(), $lead->getId());
				// if $leadNum == 0 set owner email, but other set zmien ten email@gobot.pl
				if($leadNum == 0){
					$leadData[0]['labels']['email'] = $this->getUser()->getEmail();
				} else {
					$leadData[0]['labels']['email'] = "zmien-ten-email@gobot.pl";
				}
				// convert to database format
				$data_labels = "";
				$which = 0;
				foreach ($leadData[0]['labels'] as $key => $value) {
					if($which == 0){
						$data_labels .= $key."[labelData]".$value;
					} else {
						$data_labels .= "{label}".$key."[labelData]".$value;
					}
					$which++;
				}
				// seve Labels
				$duplicat_lead->setLabels($data_labels);
				// save duplicate to database
				$entityManager->persist($duplicat_lead);
				$leadNum++;
			}
			// clean EntityManager
			$entityManager->flush();

			$this->addFlash("success", "Duplikat utworzono poprawnie");
		}
		return $this->redirectToRoute("mailbase_index");
	}

	/**
	 * @Route("/panel/import", name="import_index")
	 * @param  Request $request
	 * @return Response
	 */
	public function importAction(Request $request) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}

		// create new LeadImport object
		$lead_import = new LeadImport();
		// create form to import
		$form = $this->createForm(LeadImportType::class, $lead_import);
		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

	            // get file and set filename
	            $file = $lead_import->getFile();
	            // create new file name from generated text and extension
	            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();
	            // moves the file to the directory where brochures are stored
	            $file->move($this->getParameter('import_directory'), $fileName);
	            // update file name for save file name
	            $lead_import->setFile($fileName);

	            // save LeadImport to database
	            $entityManager = $this->getDoctrine()->getManager();

				// find Client
				$user = $entityManager->getRepository(User::class)->findOneBy(array("id" => $this->getUser()->getClient()));
				// add Client to LeadImport
				$lead_import->setClient($user);
				// add type of LeadImport
				$lead_import->setType(LeadImport::TYPE_NORMAL);
				$lead_import->setTypeBase(LeadImport::TYPE_BASE_AVAILABLE);
				$lead_import->setStage(LeadImport::STAGE2);

				// save to database
	            $entityManager->persist($lead_import);

				// if file exists import data to database
				$filePath = $this->getParameter('import_directory').'/'.$fileName;
				if (file_exists($filePath)) {
					// read file content
					$file_open = fopen($filePath, "r");
					$contents = fread($file_open, filesize($filePath));
					fclose($file_open);
					// get data to easy array content
					$data = simplexml_load_string($contents, 'SimpleXMLElement', LIBXML_NOCDATA);
					$data = json_decode( json_encode($data) , 1);

					// list of bad emails of leads
					$bad_leads = [];

					// each lead add to database
					foreach ($data['lead'] as $lead_data) {
						if(filter_var($lead_data['email'], FILTER_VALIDATE_EMAIL)) {
							$lead = new Lead();
							$lead->setLeadImport($lead_import);

							// convert to string for database
							$data_labels = "";
							$which = 0;
							foreach ($lead_data as $key => $value) {
								if($which == 0){
									$data_labels .= $key."[labelData]".$value;
								} else {
									$data_labels .= "{label}".$key."[labelData]".$value;
								}
								$which++;
							}

							$lead->setLabels($data_labels);
				            $entityManager->persist($lead);
						} else {
							array_push($bad_leads, $lead_data);
						}
					}

					if(count($bad_leads) > 0){
						$this->sendImportRaport($bad_leads);
					}

				} else {
					$this->addFlash("error", "Plik importu nie istnieje.");
				}
				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Import pliku wykonany poprawnie.");
				return $this->redirectToRoute("import_stage2", [
					// "request" => $request,
					"id" => $lead_import->getId(),
				]);
	        }
			// add flash with error
			$this->addFlash("error", "Nie powiodło się importowanie.");
		}
		return $this->render("LeadImport/importStage1.html.twig", ["form" => $form->createView()]);
	}

	/**
	 * @Route("/panel/import/stage2/{id}", name="import_stage2")
	 * @param  Request $request
	 * @param LeadImport $lead_import
	 */
	public function importStage2Action(Request $request, LeadImport $lead_import) {
		// $this->denyAccessUnlessGranted("ROLE_CLIENT");
		$this->denyAccessUnlessGranted("ROLE_USER");
		// security
		if($this->getUser()->getTypeUser() == USER::TYPE_USER_AGENT){
			throw new AccessDeniedException();
		}
		// security
		if($this->getUser()->getClient() != $lead_import->getClient()->getId()){
			throw new AccessDeniedException();
		}

        // save LeadImport to database
        $entityManager = $this->getDoctrine()->getManager();

		// create form to import
		$form = $this->createFormBuilder($lead_import)
		   ->add('labels', HiddenType::class, array('label' => 'Etykiety'))
		   ->getForm();

		// load default labels to stage2
		$lead_labels = $entityManager->getRepository(Lead::class)->findOneBy(array("lead_import" => $lead_import->getId()));
		if(!empty($lead_labels->getLabels())){
			$default_labels = $lead_labels->getLabels();
		} else {
			$default_labels = [];
		}

		// check method
		if($request->isMethod("post")) {
			// form handle request
	        $form->handleRequest($request);
	        // check form for valid
	        if($form->isValid()) {

				// get data from form
				$labels = $form->get('labels')->getData();
				// implode data to database format
				$data_labels = implode('{label}', array_map(function ($entry) {
					return implode('[labelData]', $entry);
				}, $labels));
				// save in database
				$lead_import->setLabels($data_labels);

				$lead_import->setStage(LeadImport::STAGES_FINISH);

				// save to database
	            $entityManager->persist($lead_import);

				// clean entityManager
	            $entityManager->flush();
	            // add Flash with success
	            $this->addFlash("success", "Dodanie bazy mailingowej udało się");
				return $this->redirectToRoute("mailbase_index", []);
	        }
			// add flash with error
			$this->addFlash("error", "Nie powiodło się dodanie bazy mailingowej.");
		}
		return $this->render("LeadImport/importStage2.html.twig", [
			"form" => $form->createView(),
			"LeadImportId" => $lead_import->getId(),
			"default_labels" => $default_labels,
		]);
	}

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

	// function to receive data from Lead
	private function getLeadData($leadImportId = null, $LeadId = null){
		$entityManager = $this->getDoctrine()->getManager();
		if($LeadId){
			$query = $entityManager
	            ->createQuery(
	                'SELECT a FROM AppBundle:Lead a
					WHERE a.id = :id')
				->setParameter("id", $LeadId)
				->getArrayResult();
		} elseif($leadImportId){
			$query = $entityManager
	            ->createQuery(
	                'SELECT a FROM AppBundle:Lead a
					WHERE a.lead_import = :lead_import')
				->setParameter("lead_import", $leadImportId)
				->getArrayResult();
		}

		// get Leads data
		$result = [];
		foreach ($query as $key => $queryValue) {
			$result[$key] = [];
			foreach ($queryValue as $key_in => $labelValue) {

				if($key_in == "id"){
					$result[$key][$key_in] = $labelValue;
				}
				if($key_in == "labels"){
					$data = explode('{label}',$labelValue);
					$new_data = [];
					foreach ($data as $key_data => $value_data) {
						$exp_val = explode('[labelData]',$value_data);
						$field = "";
						foreach ($exp_val as $key_data_in => $value_data_in) {
							if($key_data_in == 0){
								$new_data[$value_data_in] = "";
								$field = $value_data_in;
							} else {
								$new_data[$field] = $value_data_in;
							}
						}
					}
					$result[$key][$key_in] = $new_data;
				}
			}
		}

		return $result;
	}

	// function to send mails for importRaport
	public function sendImportRaport($bad_leads)
	{
		$message = \Swift_Message::newInstance()
			->setSubject('Import Raport')
	        ->setFrom($this->getParameter('mailer_user'))
	        ->setTo($this->getUser()->getEmail())
	        ->setBody(
	            $this->renderView(
	                'LeadImport/importRaport.html.twig',
	                array('bad_leads' => $bad_leads)
	            ),
	            'text/html'
	        );
		$this->get('mailer')->send($message);
	}

}
