<?php

namespace AppBundle\Service;
use Doctrine\ORM\EntityManagerInterface;

class RetriverDataService {

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }


	// function to receive data from Lead
	public function getLeadData($LeadId = null){
		$entityManager=$this->entityManager;
		if($LeadId){
			$query = $entityManager
	            ->createQuery(
	                'SELECT a FROM AppBundle:Lead a
					WHERE a.id = :id')
				->setParameter("id", $LeadId)
				->getArrayResult();

    		// get Leads data
    		$result = [];
    		foreach ($query as $key => $queryValue) {
    			$result[$key] = [];
    			foreach ($queryValue as $key_in => $labelValue) {

    				if($key_in == "id"){
    					$result[$key][$key_in] = $labelValue;
    				}
    				if($key_in == "labels"){
    					$data = explode('{label}',$labelValue);
    					$new_data = [];
    					foreach ($data as $key_data => $value_data) {
    						$exp_val = explode('[labelData]',$value_data);
    						$field = "";
    						foreach ($exp_val as $key_data_in => $value_data_in) {
    							if($key_data_in == 0){
    								$new_data[$value_data_in] = "";
    								$field = $value_data_in;
    							} else {
    								$new_data[$field] = $value_data_in;
    							}
    						}
    					}
    					$result[$key][$key_in] = $new_data;
    				}
    			}
    		}
            return $result;
        }
		return [];
	}

	//function to receive data from LeadImport
	public function getLabelsData($leadImportId = null){
		// receive data from database
		$entityManager=$this->entityManager;
		// $entityManager = $this->getDoctrine()->getManager();
		if($leadImportId){
			$query = $entityManager
				->createQuery(
					'SELECT a FROM AppBundle:LeadImport a
					WHERE a.id = :id')
				->setParameter("id", $leadImportId)
				->getArrayResult();

            if(!empty($query[0]['labels'])){
    			$data = explode('{label}',$query[0]['labels']);
    			$new_data = [];
    			foreach ($data as $key => $value) {
    				$exp_val = explode('[labelData]',$value);
    				foreach ($exp_val as $key_in => $value) {
    					if($key_in == 0){
    						$new_data[$key]['name'] = $value;
    					} else {
    						$new_data[$key]['label'] = $value;
    					}
    				}
    			}
    			return $new_data;
            } else {
                return 0;
            }
		} else {
			return 0;
		}
	}

    //function to get Count of Leads in LeadImport for Campaign
	public function getCountLeads($data, $leadImportId = null, $notAjax = null, $count = true){
		// receive data from database
		$entityManager=$this->entityManager;
		// $entityManager = $this->getDoctrine()->getManager();
		if($data){
            // not AJAX counter
            if($notAjax > 0 && $leadImportId > 0){
                $leadImportId = $leadImportId;
                // generate data from conditions
                $data_new = [];
                $data_new['conditions'] = [];
                $data = explode('{if}',$data);
                foreach ($data as $key => $value) {
                    $value_val = explode('[value]',$value);
                    $value_if = explode('[if]',$value_val[0]);
                    $value_name = explode('[name]',$value_if[0]);
                    if(!empty($value_val[1])){
                        $val = $value_val[1];
                    } else {
                        $val = "";
                    }
                    if(!empty($value_if[1])){
                        $if = $value_if[1];
                    } else {
                        $if = "";
                    }
                    if(!empty($value_name[1])){
                        $name = $value_name[1];
                    } else {
                        $name = "";
                    }
                    // $val = $value_val[1];
                    // $if = $value_if[1];
                    // $name = $value_name[1];
                    // save to format like from AJAX query
                    $data_new['conditions'][$key][] = array('name' => 'name', 'value' => $name);
                    $data_new['conditions'][$key][] = array('name' => 'if', 'value' => $if);
                    $data_new['conditions'][$key][] = array('name' => 'value', 'value' => $val);
                }
                // change $data to new format data
                $data = $data_new;
            } else {
                // get lead_import from AJAX
                $leadImportId = $data['lead_import'][0]['value'];
            }

            if($leadImportId > 0){
                // Create Query to DB
                $query = $entityManager->createQuery('SELECT a FROM AppBundle:Lead a WHERE a.lead_import = '.$leadImportId)->getArrayResult();

                // get Leads data
                $result = [];
                foreach ($query as $key => $queryValue) {
                    $result[$key] = [];
                    foreach ($queryValue as $key_in => $labelValue) {

                        if($key_in == "id"){
                            $result[$key][$key_in] = $labelValue;
                        }
                        if($key_in == "labels"){
                            $data_in = explode('{label}',$labelValue);
                            $new_data = [];
                            foreach ($data_in as $key_data => $value_data) {
                                $exp_val = explode('[labelData]',$value_data);
                                $field = "";
                                foreach ($exp_val as $key_data_in => $value_data_in) {
                                    if($key_data_in == 0){
                                        $new_data[$value_data_in] = "";
                                        $field = $value_data_in;
                                    } else {
                                        $new_data[$field] = $value_data_in;
                                    }
                                }
                            }
                            $result[$key][$key_in] = $new_data;
                        }
                    }
                }

                // Leads after conditions
                $leads_after = [];
                // all Leads
                foreach ($result as $key_leads => $value_leads) {
                    // their labels for check
                    $ifChecker = 1;
                    foreach ($value_leads['labels'] as $key_leads_in => $value_leads_in) {
                        // all conditions form Campaign
                        foreach($data['conditions'] as $key_condition => $value_condition){
                            // check condition variables exists
                            if(!empty($value_condition[0]['value']) && !empty($value_condition[1]['value']) && !empty($value_condition[2]['value'])){
                                // check label equels condition Name
                                if($key_leads_in == $value_condition[0]['value']){
                                    $con = $value_condition[1]['value'];
                                    $con_val = $value_condition[2]['value'];
                                    if($con == "=="){
                                        if($value_leads_in == $con_val ? "" : $ifChecker = 0);
                                    } elseif($con == "!="){
                                        if($value_leads_in != $con_val ? "" : $ifChecker = 0);
                                    } elseif($con == ">"){
                                        if($value_leads_in > $con_val ? "" : $ifChecker = 0);
                                    } elseif($con == "<"){
                                        if($value_leads_in < $con_val ? "" : $ifChecker = 0);
                                    } elseif($con == ">="){
                                        if($value_leads_in >= $con_val ? "" : $ifChecker = 0);
                                    } elseif($con == "<="){
                                        if($value_leads_in <= $con_val ? "" : $ifChecker = 0);
                                    }
                                }
                            }
                        }
                    }
                    if($ifChecker == 1){
                        $leads_after[] = $value_leads['id'];
                    }
                }
                if($count){
                    return count($leads_after);
                } else {
                    return $leads_after;
                }
            }
		}
        if($count){
            return 0;
        } else {
            return [];
        }
	}

    public function checkPhoneNumberIn($text){
		// $phones = "
		// +48 232-323-343
		// +48 22 555 55 55
		// +48234567456
		// +48 234 234 565
		// +48 123123123
		// 0048 22 555 55 55
		// 0048 123456789
		// 0048 123-123-123
		// 0048123123123
		// 0048 123 123 123
		// 10 234 45 05
		// 10 1231231
		// 10 123-12-12
		// 10-123-12-12
		// 12-1231231
		// 999 324 324
		// 323456749
		// 123-132-123
		// ";

		// returns all results in array $matches
		$matchesPhoneNumbers = array();
		// numery +48123123123
		$regex = '[+][0-9]{2}[0-9]{9}';
		// numery +48 123123123
		$regex = '[+][0-9]{2}[\s][0-9]{9}';
		// numery 123123123
		$regex .= '|[0-9]{9}';
		// numery 123-123-123
		$regex .= '|[0-9]{3}[\-][0-9]{3}[\-][0-9]{3}';
		// numery 123 123 123
		$regex .= '|[0-9]{3}[\s][0-9]{3}[\s][0-9]{3}';
		// numery 10 123 12 12
		$regex .= '|[0-9]{2}[\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}';
		// numery 10 1231231
		$regex .= '|[0-9]{2}[\s][0-9]{7}';
		// numery 10 123-12-12
		$regex .= '|[0-9]{2}[\s][0-9]{3}[\-][0-9]{2}[\-][0-9]{2}';
		// numery 10-123-12-12
		$regex .= '|[0-9]{2}[\-][0-9]{3}[\-][0-9]{2}[\-][0-9]{2}';
		// numery 12-1231231
		$regex .= '|[0-9]{2}[\-][0-9]{7}';
		// numery +48123-123-123
		$regex .= '|[+][0-9]{2}[0-9]{3}[\-][0-9]{3}[\-][0-9]{3}';
		// numery +48 123-123-123
		$regex .= '|[+][0-9]{2}[\s][0-9]{3}[\-][0-9]{3}[\-][0-9]{3}';
		// numery +48 123 123 123
		$regex .= '|[+][0-9]{2}[\s][0-9]{3}[\s][0-9]{3}[\s][0-9]{3}';
		// numery +48 12 123 12 22
		$regex .= '|[+][0-9]{2}[\s][0-9]{2}[\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}';
		// numery 0048 12 123 12 22
		$regex .= '|[0-9]{4}[\s][0-9]{2}[\s][0-9]{3}[\s][0-9]{2}[\s][0-9]{2}';
		// numery 0048 123456789
		$regex .= '|[0-9]{4}[\s][0-9]{9}';
		// numery 0048 123-123-123
		$regex .= '|[0-9]{4}[\s][0-9]{3}[\-][0-9]{3}[\-][0-9]{3}';
		// numery 0048123123123
		$regex .= '|[0-9]{13}';
		// numery 0048 123 123 123
		$regex .= '|[0-9]{4}[\s][0-9]{3}[\s][0-9]{3}[\s][0-9]{3}';

		// preg_match_all('/'.$regex.'/', $phones, $matchesP);
		preg_match_all('/'.$regex.'/', $text, $matchesPhoneNumbers);

		$matchesPhoneNumbers = $matchesPhoneNumbers[0];
		// var_dump($matchesPhoneNumbers);

        // return $matchesPhoneNumbers[0];
        return $matchesPhoneNumbers;
    }

}
