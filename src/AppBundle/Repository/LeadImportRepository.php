<?php

namespace AppBundle\Repository;
use AppBundle\Entity\LeadImport;

use Doctrine\ORM\EntityRepository;

/**
 * LeadImportRepository
 */
class LeadImportRepository extends \Doctrine\ORM\EntityRepository
{
	public function getLeadImports($clientId) {
		$entityManager = $this->getEntityManager();
		$qb = $entityManager->createQueryBuilder()
		   ->select('u')
		   ->from('AppBundle:LeadImport', 'u')
		   ->andWhere('u.stage = :stage')
		   ->andWhere('u.typeBase = :typeBase')
		   ->andWhere('u.client = :client');
	   $qb->setParameter('stage', LeadImport::STAGES_FINISH);
	   $qb->setParameter('typeBase', LeadImport::TYPE_BASE_AVAILABLE);
	   $qb->setParameter('client', $clientId);
	   return $qb;
	}
}
