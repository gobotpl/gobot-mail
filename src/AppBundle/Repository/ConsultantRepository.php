<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Consultant;

use Doctrine\ORM\EntityRepository;

/**
 * ConsultantRepository
 */
class ConsultantRepository extends \Doctrine\ORM\EntityRepository
{
	public function getConsultants($clientId) {
		$entityManager = $this->getEntityManager();
		$qb = $entityManager->createQueryBuilder()
		   ->select('u')
		   ->from('AppBundle:Consultant', 'u')
		   ->andWhere('u.client = :client');
	   $qb->setParameter('client', $clientId);
	   return $qb;
	}
}
