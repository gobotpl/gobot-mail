<?php

namespace AppBundle\Repository;
use AppBundle\Entity\User;

use Doctrine\ORM\EntityRepository;

/**
 * UserRepository
 */
class UserRepository extends \Doctrine\ORM\EntityRepository
{
	public function getAgents($clientId) {
		$entityManager = $this->getEntityManager();
		$qb = $entityManager->createQueryBuilder()
		   ->select('u')
		   ->from('AppBundle:User', 'u')
		   ->andWhere('u.typeUser = :agent')
		   ->andWhere('u.client = :client');

	   $qb->setParameter('agent', USER::TYPE_USER_AGENT);
	   $qb->setParameter('client', $clientId);
	   return $qb;
	}
}
