<?php

namespace AppBundle\Form;

use AppBundle\Entity\Campaign;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\User;
use AppBundle\Entity\Consultant;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\ConsultantRepository;
use AppBundle\Repository\LeadImportRepository;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CampaignEditType extends AbstractType
{
	private $tokenStorage;

	public function __construct(TokenStorageInterface $tokenStorage)
	{
		$this->tokenStorage = $tokenStorage;
	}

	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$user = $this->tokenStorage->getToken()->getUser()->getClient();
		$builder
		->add("name", TextType::class, ["label" => "Nazwa kampanii"])
		->add("lead_import", EntityType::class, array(
			"label" => "Baza Mailingowa",
			"class" => LeadImport::class,
			'query_builder' => function(LeadImportRepository $repo) use ($user) {
				return $repo->getLeadImports($user);
			},
			"choice_label" => "name",
			'placeholder' => 'Wybierz bazę mailingową',
		))
		->add("agent", EntityType::class, array(
			"multiple" => true,
			"label" => "Agent",
			"class" => User::class,
			'query_builder' => function(UserRepository $repo) use ($user) {
				return $repo->getAgents($user);
			},
			"choice_label" => function (User $user) {
				// if(!empty($user->getName()) && !empty($user->getLastName())){
				if(!empty($user->getName())){
					return $user->getFullName();
				} else {
					return $user->getUsername();
				}
			},
			'placeholder' => 'Wybierz Agenta',
		))
		->add("consultant", EntityType::class, array(
			"multiple" => true,
			"label" => "Konsultant",
			"class" => Consultant::class,
			'query_builder' => function(ConsultantRepository $repo) use ($user) {
				return $repo->getConsultants($user);
			},
			"choice_label" => function (Consultant $user) {
				// if(!empty($user->getName()) && !empty($user->getLastName())){
				if(!empty($user->getName())){
					return $user->getFullName();
				} else {
					return $user->getName();
				}
			},
			'placeholder' => 'Wybierz Konsultanta',
		))
		->add("mail_active1", ChoiceType::class, ['choices' => ['Tak' => true, 'Nie' => false], "attr" => ["class" => "campaign_mail_active"], "label" => "Aktywny"])
		->add("mail_text1", TextAreaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		->add("mail_date1", DateTimeType::class, ["label" => "Data wysyłania", 'widget' => 'single_text'])

		->add("mail_active2", ChoiceType::class, ['choices' => ['Tak' => true, 'Nie' => false], "attr" => ["class" => "campaign_mail_active"], "label" => "Aktywny"])
		->add("mail_text2", TextAreaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		// ->add("mail_date2", DateTimeType::class, ["label" => "Data wysyłania"])
		->add("mail_date2_days", NumberType::class, ["label" => "Ile dni", "attr" => ["placeholder" => "0-15"]])
		->add("mail_date2_hours", NumberType::class, ["label" => "Ile godzin", "attr" => ["placeholder" => "0-23"]])

		->add("mail_active3", ChoiceType::class, ['choices' => ['Tak' => true, 'Nie' => false], "attr" => ["class" => "campaign_mail_active"], "label" => "Aktywny"])
		->add("mail_text3", TextAreaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		// ->add("mail_date3", DateTimeType::class, ["label" => "Data wysyłania"])
		->add("mail_date3_days", NumberType::class, ["label" => "Ile dni", "attr" => ["placeholder" => "0-15"]])
		->add("mail_date3_hours", NumberType::class, ["label" => "Ile godzin", "attr" => ["placeholder" => "0-23"]])

		->add("mail_active4", ChoiceType::class, ['choices' => ['Tak' => true, 'Nie' => false], "attr" => ["class" => "campaign_mail_active"], "label" => "Aktywny"])
		->add("mail_text4", TextAreaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		// ->add("mail_date4", DateTimeType::class, ["label" => "Data wysyłania"])
		->add("mail_date4_days", NumberType::class, ["label" => "Ile dni", "attr" => ["placeholder" => "0-15"]])
		->add("mail_date4_hours", NumberType::class, ["label" => "Ile godzin", "attr" => ["placeholder" => "0-23"]])

		->add("mail_active5", ChoiceType::class, ['choices' => ['Tak' => true, 'Nie' => false], "attr" => ["class" => "campaign_mail_active"], "label" => "Aktywny"])
		->add("mail_text5", TextAreaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		// ->add("mail_date5", DateTimeType::class, ["label" => "Data wysyłania"])
		->add("mail_date5_days", NumberType::class, ["label" => "Ile dni", "attr" => ["placeholder" => "0-15"]])
		->add("mail_date5_hours", NumberType::class, ["label" => "Ile godzin", "attr" => ["placeholder" => "0-23"]])

		->add("footer", TextAreaType::class, ["label" => "Treść Stopki", "attr" => ["placeholder" => "Treść stopki"]])

		->add("conditions", HiddenType::class, ["label" => "Warunki"]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => Campaign::class]);
	}

}
