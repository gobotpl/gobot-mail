<?php

namespace AppBundle\Form;

use AppBundle\Entity\LeadImport;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LeadImportType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add("name", TextType::class, ["label" => "Nazwa importowanej listy"])
		->add("description", TextareaType::class, ["label" => "Opis"])
		->add("file", FileType::class, array("label" => "Plik"));
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => LeadImport::class]);
	}
}
