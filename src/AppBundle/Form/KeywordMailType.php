<?php

namespace AppBundle\Form;

use AppBundle\Entity\Keyword;
use AppBundle\Entity\KeywordMail;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class KeywordMailType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add("title", TextType::class)
		->add("messageText", TextareaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść wiadomości"]])
		// ->add("keywords", HiddenType::class, ["label" => "Słowa kluczowe"]);
		->add("keywords", EntityType::class, array(
			"multiple" => true,
			"label" => "Słowa kluczowe",
			"class" => Keyword::class,
			"choice_label" => "name",
			'placeholder' => 'Wybierz słowa kluczowe',
		));
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => KeywordMail::class]);
	}
}
