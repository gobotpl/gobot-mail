<?php

namespace AppBundle\Form;

use AppBundle\Entity\ClientSettings;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientSettingsIMAPType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add('bot_server', TextType::class, ["label" => "Adres serwera"])
		->add('bot_imap_port', TextType::class, ["label" => "Port serwera IMAP"])
		->add("bot_imap_encryption", ChoiceType::class, array(
			'placeholder' => 'Wybierz szyfrowanie',
			'choices' => array(
				'TLS' => ClientSettings::ENCRYPTION_TLS,
				'SSL' => ClientSettings::ENCRYPTION_SSL,
			),
			'multiple'    => false,
			'required'    => false,
			'label'		  => "Szyfrowanie serwera IMAP",
		))
		->add('bot_email', EmailType::class, ["label" => "Email"])
		->add('bot_password', TextType::class, ["label" => "Hasło"]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => ClientSettings::class]);
	}
}
