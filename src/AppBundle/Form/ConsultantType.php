<?php

namespace AppBundle\Form;

use AppBundle\Entity\Consultant;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultantType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add("name", TextType::class)
		// ->add("lastName", TextType::class)
		->add('email', EmailType::class);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => Consultant::class]);
	}
}
