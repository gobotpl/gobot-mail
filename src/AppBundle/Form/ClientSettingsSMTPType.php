<?php

namespace AppBundle\Form;

use AppBundle\Entity\clientSettingsSMTP;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientSettingsSMTPType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add('bot_server', TextType::class, ["label" => "Adres serwera"])
		->add('bot_smtp_port', TextType::class, ["label" => "Port serwera SMTP"])
		->add("bot_smtp_encryption", ChoiceType::class, array(
			'placeholder' => 'Wybierz szyfrowanie',
			'choices' => array(
				'TLS' => clientSettingsSMTP::ENCRYPTION_TLS,
				'SSL' => clientSettingsSMTP::ENCRYPTION_SSL,
			),
			'multiple'    => false,
			'required'    => false,
			'label'		  => "Szyfrowanie serwera SMTP",
		))
		->add('bot_email', EmailType::class, ["label" => "Email"])
		->add('bot_password', TextType::class, ["label" => "Hasło"]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => clientSettingsSMTP::class]);
	}
}
