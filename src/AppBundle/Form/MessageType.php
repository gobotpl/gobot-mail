<?php

namespace AppBundle\Form;

use AppBundle\Entity\Message;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add("messageText", TextareaType::class, ["label" => "Treść wiadomości", "attr" => ["placeholder" => "Treść odpowiedzi do Lead'a"]])
		->add("submit", SubmitType::class, ["label" => "Wyślij wiadomość", "attr" => ["class" => "ok"]]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => Message::class]);
	}
}
