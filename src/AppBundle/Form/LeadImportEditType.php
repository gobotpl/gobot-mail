<?php

namespace AppBundle\Form;

use AppBundle\Entity\LeadImport;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LeadImportEditType extends AbstractType
{
	public function BuildForm(FormBuilderInterface $builder, array $options) {
		$builder
		->add("name", TextType::class, ["label" => "Nazwa importowanej listy"])
		->add("description", TextareaType::class, ["label" => "Opis"])
		->add("labels", HiddenType::class, ["label" => "Etykiety"]);
	}

	public function configureOptions(OptionsResolver $resolver) {
		$resolver->setDefaults(["data_class" => LeadImport::class]);
	}
}
