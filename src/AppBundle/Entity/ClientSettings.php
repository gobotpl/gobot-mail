<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\Consultant;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="client_settings")
 */
class ClientSettings
{

	const ENCRYPTION_TLS = "tls";
	const ENCRYPTION_SSL = "ssl";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="bot_email", type="string", nullable=true)
	 * @var string
	 */
	private $botEmail;

	/**
	 * @ORM\Column(name="bot_password", type="string", nullable=true)
	 * @var string
	 */
	private $botPassword;

	/**
	 * @ORM\Column(name="bot_imap_port", type="string", nullable=true)
	 * @var string
	 */
	private $botImapPort;

	/**
	 * @ORM\Column(name="bot_imap_encryption", type="string", nullable=true)
	 * @var string
	 */
	private $botImapEncryption;

	/**
	 * @ORM\Column(name="bot_server", type="string", nullable=true)
	 * @var string
	 */
	private $botServer;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="clientSettings")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="messages_lastAssignedAgent")
	 * @ORM\JoinColumn(name="lastAssignedAgent_id", referencedColumnName="id")
	 */
	private $lastAssignedAgent;

	/**
	 * @var Consultant
	 *
	 * @ORM\ManyToOne(targetEntity="Consultant", inversedBy="messagesLastDirectConsultant")
	 * @ORM\JoinColumn(name="lastDirectConsultant_id", referencedColumnName="id")
	 */
	private $lastDirectConsultant;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="company_footer", type="text", nullable=true)
	 */
	private $companyFooter;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getBotEmail() {
		return $this->botEmail;
	}

	/**
	 * @param string $botEmail
	 */
	public function setBotEmail($botEmail) {
		return $this->botEmail = $botEmail;

		return $botEmail;
	}

	/**
	 * @return string
	 */
	public function getBotPassword() {
		return $this->botPassword;
	}

	/**
	 * @param string $botPassword
	 */
	public function setBotPassword($botPassword) {
		return $this->botPassword = $botPassword;

		return $botPassword;
	}

	/**
	 * @return string
	 */
	public function getBotImapPort() {
		return $this->botImapPort;
	}

	/**
	 * @param string $botImapPort
	 */
	public function setBotImapPort($botImapPort) {
		return $this->botImapPort = $botImapPort;

		return $botImapPort;
	}

	/**
	 * @return string
	 */
	public function getBotImapEncryption() {
		return $this->botImapEncryption;
	}

	/**
	 * @param string $botImapEncryption
	 */
	public function setBotImapEncryption($botImapEncryption) {
		return $this->botImapEncryption = $botImapEncryption;

		return $botImapEncryption;
	}

	/**
	 * @return string
	 */
	public function getBotServer() {
		return $this->botServer;
	}

	/**
	 * @param string $botServer
	 */
	public function setBotServer($botServer) {
		return $this->botServer = $botServer;

		return $botServer;
	}

    /**
     * @param User $client
     *
     * @return $this
     */
    public function setClient(User $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient() {
        return $this->client;
    }

	/**
	 * @param User $lastAssignedAgent
	 *
	 * @return $this
	 */
	public function setLastAssignedAgent(User $lastAssignedAgent) {
		$this->lastAssignedAgent = $lastAssignedAgent;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getLastAssignedAgent() {
		return $this->lastAssignedAgent;
	}

	/**
	 * @param Consultant $lastDirectConsultant
	 *
	 * @return $this
	 */
	public function setLastDirectConsultant(Consultant $lastDirectConsultant) {
		$this->lastDirectConsultant = $lastDirectConsultant;

		return $this;
	}

	/**
	 * @return Consultant
	 */
	public function getLastDirectConsultant() {
		return $this->lastDirectConsultant;
	}

	/**
	 * @return string
	 */
	public function getCompanyFooter() {
		return $this->companyFooter;
	}

	/**
	 * @param string $companyFooter
	 */
	public function setCompanyFooter($companyFooter) {
		return $this->companyFooter = $companyFooter;

		return $companyFooter;
	}
}
