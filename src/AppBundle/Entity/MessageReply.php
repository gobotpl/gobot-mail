<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="message_reply")
 */
class MessageReply
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="messages_group", type="string", nullable=true)
	 * @var string
	 */
	private $messagesGroup;

	/**
	 * @ORM\Column(name="keyword_id", type="string", nullable=true)
	 * @var string
	 */
	private $keywordID;

	/**
	 * @ORM\Column(name="keyword_mail_id", type="string", nullable=true)
	 * @var string
	 */
	private $keyMailID;

	/**
	 * @ORM\Column(name="draft_ready", type="string", nullable=true)
	 * @var string
	 */
	private $draftReady;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getMessagesGroup() {
		return $this->messagesGroup;
	}

	/**
	 * @param string $messagesGroup
	 */
	public function setMessagesGroup($messagesGroup) {
		return $this->messagesGroup = $messagesGroup;

		return $messagesGroup;
	}

	/**
	 * @return string
	 */
	public function getKeywordID() {
		return $this->keywordID;
	}

	/**
	 * @param string $keywordID
	 */
	public function setKeywordID($keywordID) {
		return $this->keywordID = $keywordID;

		return $keywordID;
	}

	/**
	 * @return string
	 */
	public function getKeyMailID() {
		return $this->keyMailID;
	}

	/**
	 * @param string $keyMailID
	 */
	public function setKeyMailID($keyMailID) {
		return $this->keyMailID = $keyMailID;

		return $keyMailID;
	}

	/**
	 * @return string
	 */
	public function getDraftReady() {
		return $this->draftReady;
	}

	/**
	 * @param string $draftReady
	 */
	public function setDraftReady($draftReady) {
		return $this->draftReady = $draftReady;

		return $draftReady;
	}

}
