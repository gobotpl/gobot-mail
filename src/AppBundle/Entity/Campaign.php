<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\User;
use AppBundle\Entity\Consultant;
use AppBundle\Entity\Message;
use AppBundle\Entity\Keyword;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\DirectLead;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="campaign")
 */
class Campaign
{
	const TYPE_ACTIVE = "active";
	const TYPE_IN_BUILD = "inBuild";
	const TYPE_STOPED = "stoped";
	const TYPE_FINISHED = "finished";
	const TYPE_DELETED = "deleted";

	const TYPE_CAMP_NORMAL = "normal";
	const TYPE_CAMP_COPY = "copy";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="type", type="string", nullable=true)
	 * @var string
	 */
	private $type;

	/**
	 * @ORM\Column(name="typeCamp", type="string", nullable=true)
	 * @var string
	 */
	private $typeCamp;

	/**
	 * @ORM\Column(name="campParent", type="string", nullable=true)
	 * @var string
	 */
	private $campParent;

	/**
	 * @ORM\Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="mail_active1", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $mailActive1;

	/**
	 * @ORM\Column(name="mail_text1", type="text", nullable=true)
	 * @var string
	 */
	private $mailText1;

	/**
	 * @ORM\Column(name="mail_date1", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $mailDate1;

	/**
	 * @ORM\Column(name="mail_active2", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $mailActive2;

	/**
	 * @ORM\Column(name="mail_text2", type="text", nullable=true)
	 * @var string
	 */
	private $mailText2;

	// /**
	//  * @ORM\Column(name="mail_date2", type="datetime", nullable=true)
	//  * @var \DateTime
	//  */
	// private $mailDate2;


	/**
	 * @ORM\Column(name="mail_date2_days", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate2Days;
	/**
	 * @ORM\Column(name="mail_date2_hours", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate2Hours;

	/**
	 * @ORM\Column(name="mail_active3", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $mailActive3;

	/**
	 * @ORM\Column(name="mail_text3", type="text", nullable=true)
	 * @var string
	 */
	private $mailText3;

	// /**
	//  * @ORM\Column(name="mail_date3", type="datetime", nullable=true)
	//  * @var \DateTime
	//  */
	// private $mailDate3;

	/**
	 * @ORM\Column(name="mail_date3_days", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate3Days;
	/**
	 * @ORM\Column(name="mail_date3_hours", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate3Hours;

	/**
	 * @ORM\Column(name="mail_active4", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $mailActive4;

	/**
	 * @ORM\Column(name="mail_text4", type="text", nullable=true)
	 * @var string
	 */
	private $mailText4;

	// /**
	//  * @ORM\Column(name="mail_date4", type="datetime", nullable=true)
	//  * @var \DateTime
	//  */
	// private $mailDate4;

	/**
	 * @ORM\Column(name="mail_date4_days", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate4Days;
	/**
	 * @ORM\Column(name="mail_date4_hours", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate4Hours;

	/**
	 * @ORM\Column(name="mail_active5", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $mailActive5;

	/**
	 * @ORM\Column(name="mail_text5", type="text", nullable=true)
	 * @var string
	 */
	private $mailText5;

	// /**
	//  * @ORM\Column(name="mail_date5", type="datetime", nullable=true)
	//  * @var \DateTime
	//  */
	// private $mailDate5;

	/**
	 * @ORM\Column(name="mail_date5_days", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate5Days;
	/**
	 * @ORM\Column(name="mail_date5_hours", type="integer", nullable=true)
	 * @var int
	 */
	private $mailDate5Hours;

	/**
	 * @ORM\Column(name="footer", type="text", nullable=true)
	 * @var string
	 */
	private $footer;

	/**
	 * @ORM\Column(name="mailing_stage", type="text", nullable=true)
	 * @var string
	 */
	private $mailingStage;





	/**
	 * @ORM\Column(name="mail_date2_full", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $mailDate2Full;

	/**
	 * @ORM\Column(name="mail_date3_full", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $mailDate3Full;

	/**
	 * @ORM\Column(name="mail_date4_full", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $mailDate4Full;

	/**
	 * @ORM\Column(name="mail_date5_full", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $mailDate5Full;









	/**
	 * @ORM\Column(name="conditions", type="string", nullable=true)
	 * @var string
	 */
	private $conditions;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="campaigns")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

	// /**
	//  * @var User
	//  *
	//  * @ORM\ManyToOne(targetEntity="User", inversedBy="campaigns_agent")
	//  * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
	//  */
	// private $agent;
	//
	/**
	 * @var User
	 *
	 * @ORM\ManyToMany(targetEntity="User", inversedBy="campaigns_agent")
	 * @ORM\JoinTable(
     *  name="campaign_user",
     *  joinColumns={
     *      @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     *  }
     * )
     */
	private $agent;

	/**
	 * @var Consultant
	 *
	 * @ORM\ManyToMany(targetEntity="Consultant", inversedBy="campaigns_consultant")
	 * @ORM\JoinTable(
     *  name="campaign_consultant",
     *  joinColumns={
     *      @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="consultant_id", referencedColumnName="id")
     *  }
     * )
     */
	 // * @ORM\JoinColumn(name="consultant_id", referencedColumnName="id")
	 // */
	private $consultant;

	/**
	 * @var LeadImport
	 *
	 * @ORM\ManyToOne(targetEntity="LeadImport", inversedBy="campaigns_lead_import")
	 * @ORM\JoinColumn(name="lead_import_id", referencedColumnName="id")
	 */
	private $lead_import;

    /**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="campaign", cascade={"remove"})
     */
    private $messages;

	/**
	 * @var DirectLead[]
	 *
	 * @ORM\OneToMany(targetEntity="DirectLead", mappedBy="campaign", cascade={"remove"})
	 */
	private $directLeads;

    /**
     * @var Keyword[]
     *
     * @ORM\OneToMany(targetEntity="Keyword", mappedBy="campaign", cascade={"remove"})
     */
    private $keywords;

	public function __construct()
	{
	    $this->agent = new ArrayCollection();
	    $this->consultant = new ArrayCollection();
	    $this->messages = new ArrayCollection();
	    $this->directLeads = new ArrayCollection();
	    $this->keywords = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		return $this->type = $type;

		return $type;
	}

	/**
	 * @return string
	 */
	public function getTypeCamp() {
		return $this->typeCamp;
	}

	/**
	 * @param string $typeCamp
	 */
	public function setTypeCamp($typeCamp) {
		return $this->typeCamp = $typeCamp;

		return $typeCamp;
	}

	/**
	 * @return string
	 */
	public function getCampParent() {
		return $this->campParent;
	}

	/**
	 * @param string $campParent
	 */
	public function setCampParent($campParent) {
		return $this->campParent = $campParent;

		return $campParent;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		return $this->name = $name;

		return $name;
	}

	/**
	 * @return boolean
	 */
	public function getMailActive1() {
		return $this->mailActive1;
	}

	/**
	 * @param boolean $mailActive1
	 */
	public function setMailActive1($mailActive1) {
		return $this->mailActive1 = $mailActive1;

		return $mailActive1;
	}

	/**
	 * @return string
	 */
	public function getMailText1() {
		return $this->mailText1;
	}

	/**
	 * @param string $mailText1
	 */
	public function setMailText1($mailText1) {
		return $this->mailText1 = $mailText1;

		return $mailText1;
	}

	/**
	 * @return boolean
	 */
	public function getMailActive2() {
		return $this->mailActive2;
	}

	/**
	 * @param boolean $mailActive2
	 */
	public function setMailActive2($mailActive2) {
		return $this->mailActive2 = $mailActive2;

		return $mailActive2;
	}

	/**
	 * @return string
	 */
	public function getMailText2() {
		return $this->mailText2;
	}

	/**
	 * @param string $mailText2
	 */
	public function setMailText2($mailText2) {
		return $this->mailText2 = $mailText2;

		return $mailText2;
	}

	/**
	 * @return boolean
	 */
	public function getMailActive3() {
		return $this->mailActive3;
	}

	/**
	 * @param boolean $mailActive3
	 */
	public function setMailActive3($mailActive3) {
		return $this->mailActive3 = $mailActive3;

		return $mailActive3;
	}

	/**
	 * @return string
	 */
	public function getMailText3() {
		return $this->mailText3;
	}

	/**
	 * @param string $mailText3
	 */
	public function setMailText3($mailText3) {
		return $this->mailText3 = $mailText3;

		return $mailText3;
	}

	/**
	 * @return boolean
	 */
	public function getMailActive4() {
		return $this->mailActive4;
	}

	/**
	 * @param boolean $mailActive4
	 */
	public function setMailActive4($mailActive4) {
		return $this->mailActive4 = $mailActive4;

		return $mailActive4;
	}

	/**
	 * @return string
	 */
	public function getMailText4() {
		return $this->mailText4;
	}

	/**
	 * @param string $mailText4
	 */
	public function setMailText4($mailText4) {
		return $this->mailText4 = $mailText4;

		return $mailText4;
	}

	/**
	 * @return boolean
	 */
	public function getMailActive5() {
		return $this->mailActive5;
	}

	/**
	 * @param boolean $mailActive5
	 */
	public function setMailActive5($mailActive5) {
		return $this->mailActive5 = $mailActive5;

		return $mailActive5;
	}

	/**
	 * @return string
	 */
	public function getMailText5() {
		return $this->mailText5;
	}

	/**
	 * @param string $mailText5
	 */
	public function setMailText5($mailText5) {
		return $this->mailText5 = $mailText5;

		return $mailText5;
	}

	/**
	 * @return string
	 */
	public function getFooter() {
		return $this->footer;
	}

	/**
	 * @param string $footer
	 */
	public function setFooter($footer) {
		return $this->footer = $footer;

		return $footer;
	}

	/**
	 * @return string
	 */
	public function getMailingStage() {
		return $this->mailingStage;
	}

	/**
	 * @param string $mailingStage
	 */
	public function setMailingStage($mailingStage) {
		return $this->mailingStage = $mailingStage;

		return $mailingStage;
	}





	/**
	 * @return \DateTime
	 */
	public function getMailDate2Full() {
		return $this->mailDate2Full;
	}

	/**
	 * @param \DateTime $mailDate2Full;
	 * @return $this
	 */
	public function setMailDate2Full(\DateTime $mailDate2Full) {
		return $this->mailDate2Full = $mailDate2Full;

		return $mailDate2Full;
	}

	/**
	 * @return \DateTime
	 */
	public function getMailDate3Full() {
		return $this->mailDate3Full;
	}

	/**
	 * @param \DateTime $mailDate3Full;
	 * @return $this
	 */
	public function setMailDate3Full(\DateTime $mailDate3Full) {
		return $this->mailDate3Full = $mailDate3Full;

		return $mailDate3Full;
	}

	/**
	 * @return \DateTime
	 */
	public function getMailDate4Full() {
		return $this->mailDate4Full;
	}

	/**
	 * @param \DateTime $mailDate4Full;
	 * @return $this
	 */
	public function setMailDate4Full(\DateTime $mailDate4Full) {
		return $this->mailDate4Full = $mailDate4Full;

		return $mailDate4Full;
	}

	/**
	 * @return \DateTime
	 */
	public function getMailDate5Full() {
		return $this->mailDate5Full;
	}

	/**
	 * @param \DateTime $mailDate5Full;
	 * @return $this
	 */
	public function setMailDate5Full(\DateTime $mailDate5Full) {
		return $this->mailDate5Full = $mailDate5Full;

		return $mailDate5Full;
	}











	/**
	 * @return \DateTime
	 */
	public function getMailDate1() {
		return $this->mailDate1;
	}

	/**
	 * @param \DateTime $mailDate1
	 * @return $this
	 */
	public function setMailDate1(\DateTime $mailDate1) {
		return $this->mailDate1 = $mailDate1;

		return $mailDate1;
	}

	// /**
	//  * @return \DateTime
	//  */
	// public function getMailDate2() {
	// 	return $this->mailDate2;
	// }
	//
	// /**
	//  * @param \DateTime $mailDate2
	//  * @return $this
	//  */
	// public function setMailDate2(\DateTime $mailDate2) {
	// 	return $this->mailDate2 = $mailDate2;
	//
	// 	return $mailDate2;
	// }
	//
	// /**
	//  * @return \DateTime
	//  */
	// public function getMailDate3() {
	// 	return $this->mailDate3;
	// }
	//
	// /**
	//  * @param \DateTime $mailDate3
	//  * @return $this
	//  */
	// public function setMailDate3(\DateTime $mailDate3) {
	// 	return $this->mailDate3 = $mailDate3;
	//
	// 	return $mailDate3;
	// }
	//
	// /**
	//  * @return \DateTime
	//  */
	// public function getMailDate4() {
	// 	return $this->mailDate4;
	// }
	//
	// /**
	//  * @param \DateTime $mailDate4
	//  * @return $this
	//  */
	// public function setMailDate4(\DateTime $mailDate4) {
	// 	return $this->mailDate4 = $mailDate4;
	//
	// 	return $mailDate4;
	// }
	//
	// /**
	//  * @return \DateTime
	//  */
	// public function getMailDate5() {
	// 	return $this->mailDate5;
	// }
	//
	// /**
	//  * @param \DateTime $mailDate5
	//  * @return $this
	//  */
	// public function setMailDate5(\DateTime $mailDate5) {
	// 	return $this->mailDate5 = $mailDate5;
	//
	// 	return $mailDate5;
	// }

	/**
	 * @return int
	 */
	public function getMailDate2Days() {
		return $this->mailDate2Days;
	}
	/**
	 * @param int $mailDate2Days
	 */
	public function setMailDate2Days($mailDate2Days) {
		return $this->mailDate2Days = $mailDate2Days;

		return $mailDate2Days;
	}
	/**
	 * @return int
	 */
	public function getMailDate2Hours() {
		return $this->mailDate2Hours;
	}
	/**
	 * @param int $mailDate2Hours
	 */
	public function setMailDate2Hours($mailDate2Hours) {
		return $this->mailDate2Hours = $mailDate2Hours;

		return $mailDate2Hours;
	}

	/**
	 * @return int
	 */
	public function getMailDate3Days() {
		return $this->mailDate3Days;
	}
	/**
	 * @param int $mailDate3Days
	 */
	public function setMailDate3Days($mailDate3Days) {
		return $this->mailDate3Days = $mailDate3Days;

		return $mailDate3Days;
	}
	/**
	 * @return int
	 */
	public function getMailDate3Hours() {
		return $this->mailDate3Hours;
	}
	/**
	 * @param int $mailDate3Hours
	 */
	public function setMailDate3Hours($mailDate3Hours) {
		return $this->mailDate3Hours = $mailDate3Hours;

		return $mailDate3Hours;
	}

	/**
	 * @return int
	 */
	public function getMailDate4Days() {
		return $this->mailDate4Days;
	}
	/**
	 * @param int $mailDate4Days
	 */
	public function setMailDate4Days($mailDate4Days) {
		return $this->mailDate4Days = $mailDate4Days;

		return $mailDate4Days;
	}
	/**
	 * @return int
	 */
	public function getMailDate4Hours() {
		return $this->mailDate4Hours;
	}
	/**
	 * @param int $mailDate4Hours
	 */
	public function setMailDate4Hours($mailDate4Hours) {
		return $this->mailDate4Hours = $mailDate4Hours;

		return $mailDate4Hours;
	}

	/**
	 * @return int
	 */
	public function getMailDate5Days() {
		return $this->mailDate5Days;
	}
	/**
	 * @param int $mailDate5Days
	 */
	public function setMailDate5Days($mailDate5Days) {
		return $this->mailDate5Days = $mailDate5Days;

		return $mailDate5Days;
	}
	/**
	 * @return int
	 */
	public function getMailDate5Hours() {
		return $this->mailDate5Hours;
	}
	/**
	 * @param int $mailDate5Hours
	 */
	public function setMailDate5Hours($mailDate5Hours) {
		return $this->mailDate5Hours = $mailDate5Hours;

		return $mailDate5Hours;
	}


	/**
	 * @return string
	 */
	public function getConditions() {
		return $this->conditions;
	}

	/**
	 * @param string $conditions
	 */
	public function setConditions($conditions) {
		return $this->conditions = $conditions;

		return $conditions;
	}

    /**
     * @param User $client
     *
     * @return $this
     */
    public function setClient(User $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient() {
        return $this->client;
    }
	//
    // /**
    //  * @param User $agent
    //  *
    //  * @return $this
    //  */
    // public function setAgent(User $agent) {
    //     $this->agent = $agent;
	//
    //     return $this;
    // }
	//
    // /**
    //  * @return User
    //  */
    // public function getAgent() {
    //     return $this->agent;
    // }

    /**
     * @param User $agent
     *
     * @return $this
     */
    public function setAgent(User $agent) {
        $this->agent[] = $agent;

        return $this;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getAgent() {
        return $this->agent;
    }

    /**
     * @param Consultant $consultant
     *
     * @return $this
     */
    public function setConsultant(Consultant $consultant) {
        $this->consultant[] = $consultant;

        return $this;
    }

    /**
     * @return Consultant[]|ArrayCollection
     */
    public function getConsultant() {
        return $this->consultant;
    }

    /**
     * @param LeadImport $lead_import
     *
     * @return $this
     */
    public function setLeadImport(LeadImport $lead_import) {
        $this->lead_import = $lead_import;

        return $this;
    }

    /**
     * @return LeadImport
     */
    public function getLeadImport() {
        return $this->lead_import;
    }

	/**
	 * @return Message[]|ArrayCollection
	 */
	public function getMessages() {
		return $this->messages;
	}

	/**
	 * @param Message $messages
	 *
	 * @return $this
	 */
	public function setMessages(Message $messages) {
		$this->messages[] = $messages;

		return $this;
	}


	/**
	 * @return DirectLead[]|ArrayCollection
	 */
	public function getDirectLeads() {
		return $this->directLeads;
	}

	/**
	 * @param DirectLead $directLeads
	 *
	 * @return $this
	 */
	public function setDirectLeads(DirectLead $directLeads) {
		$this->directLeads[] = $directLeads;

		return $this;
	}

	/**
	 * @return Keyword[]|ArrayCollection
	 */
	public function getKeywords() {
		return $this->keywords;
	}

	/**
	 * @param Keyword $keywords
	 *
	 * @return $this
	 */
	public function setKeywords(Keyword $keywords) {
		$this->keywords[] = $keywords;

		return $this;
	}

}
