<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\LeadImport;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="lead")
 */
class Lead
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="labels", type="string", nullable=true)
	 * @var string
	 */
	private $labels;


	/**
	 * @var LeadImport
	 *
	 * @ORM\ManyToOne(targetEntity="LeadImport", inversedBy="leads")
	 * @ORM\JoinColumn(name="lead_import_id", referencedColumnName="id")
	 */
	private $lead_import;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getLabels() {
		return $this->labels;
	}

	/**
	 * @param string $labels
	 */
	public function setLabels($labels) {
		return $this->labels = $labels;

		return $labels;
	}

    /**
     * @param LeadImport $lead_import
     *
     * @return $this
     */
    public function setLeadImport(LeadImport $lead_import) {
        $this->lead_import = $lead_import;

        return $this;
    }

    /**
     * @return LeadImport
     */
    public function getLeadImport() {
        return $this->lead_import;
    }

}
