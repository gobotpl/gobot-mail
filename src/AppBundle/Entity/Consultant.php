<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\LeadImport;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="consultant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ConsultantRepository")
 */
class Consultant
{

	const OFF_USER_YES = "yes";
	const OFF_USER_NO = "no";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="offUser", type="string", nullable=true)
	 * @var string
	 */
	private $offUser;

	/**
	 * @ORM\Column(name="email", type="string", nullable=true)
	 * @var string
	 * @Assert\NotBlank(
	 * 	message="Email can't be empty"
	 * )
	 */
	private $email;

	/**
	 * @ORM\Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="lastName", type="string", nullable=true)
	 * @var string
	 */
	private $lastName;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="consultants")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

    /**
     * @var Campaign[]
     *
     * @ORM\ManyToMany(targetEntity="Campaign", mappedBy="consultant")
     */
    private $campaigns_consultant;

	/**
	 * @var ClientSettings[]
	 *
	 * @ORM\OneToMany(targetEntity="ClientSettings", mappedBy="lastDirectConsultant")
	 */
	private $messagesLastDirectConsultant;


	public function __construct()
	{
	    $this->campaigns_consultant = new ArrayCollection();
	    $this->messagesLastDirectConsultant = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getOffUser() {
		return $this->offUser;
	}

	/**
	 * @param string $offUser
	 */
	public function setOffUser($offUser) {
		return $this->offUser = $offUser;

		return $offUser;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		return $this->email = $email;

		return $email;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		return $this->name = $name;

		return $name;
	}

	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		return $this->lastName = $lastName;

		return $lastName;
	}

    /**
     * @param User $client
     *
     * @return $this
     */
    public function setClient(User $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaignsConsultant() {
        return $this->campaigns_consultant;
    }

    /**
     * @param Campaign $campaigns_consultant
     *
     * @return $this
     */
    public function setCampaignsConsultant(Campaign $campaigns_consultant) {
        $this->campaigns_consultant[] = $campaigns_consultant;

        return $this;
    }

	/**
	 * @return ClientSettings[]|ArrayCollection
	 */
	public function getMessagesLastDirectConsultant() {
		return $this->messages_lastDirectConsultant;
	}

	/**
	 * @param ClientSettings $messages_lastDirectConsultant
	 *
	 * @return $this
	 */
	public function setMessagesLastDirectConsultant(ClientSettings $messages_lastDirectConsultant) {
		$this->messages_lastDirectConsultant[] = $messages_lastDirectConsultant;

		return $this;
	}


	/**
	* @return string
	*/
	public function getFullName(){
	 // return $this->name .' '. $this->lastName;
	 return $this->name;
	}

}
