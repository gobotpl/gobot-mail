<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Consultant;
use AppBundle\Entity\Message;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\Keyword;
use AppBundle\Entity\LeadImport;
use AppBundle\Entity\ClientSettings;
use AppBundle\Entity\ClientSettingsSMTP;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
	const TYPE_USER_AGENT = "agent";
	const TYPE_USER_KP = "kp";

	const MESSAGES_REPLY_ACTIVE = "active";
	const MESSAGES_REPLY_STOP = "stop";

	const OFF_USER_YES = "yes";
	const OFF_USER_NO = "no";

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(name="typeUser", type="string", nullable=true)
	 * @var string
	 */
	private $typeUser;

	/**
	 * @ORM\Column(name="offUser", type="string", nullable=true)
	 * @var string
	 */
	private $offUser;

	/**
	 * @ORM\Column(name="companyName", type="string", nullable=true)
	 * @var string
	 */
	private $companyName;

	/**
	 * @ORM\Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="lastName", type="string", nullable=true)
	 * @var string
	 */
	private $lastName;

	/**
	 * @ORM\Column(name="phone", type="string", nullable=true)
	 * @var string
	 */
	private $phone;

	/**
	 * @ORM\Column(name="address", type="string", nullable=true)
	 * @var string
	 */
	private $address;

	/**
	 * @ORM\Column(name="postCode", type="string", nullable=true)
	 * @var string
	 */
	private $postCode;

	/**
	 * @ORM\Column(name="city", type="string", nullable=true)
	 * @var string
	 */
	private $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="personDescription", type="text", nullable=true)
	 */
	private $personDescription;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="footer", type="text", nullable=true)
	 */
	private $footer;

    /**
     * @var Consultant[]
     *
     * @ORM\OneToMany(targetEntity="Consultant", mappedBy="client")
     */
    private $consultants;

	/**
     * @var ClientSettings[]
     *
     * @ORM\OneToMany(targetEntity="ClientSettings", mappedBy="client")
     */
    private $clientSettings;

    /**
     * @var LeadImport[]
     *
     * @ORM\OneToMany(targetEntity="LeadImport", mappedBy="client")
     */
    private $lead_imports;

    /**
     * @var Campaign[]
     *
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="client")
     */
    private $campaigns;

    /**
     * @var Keyword[]
     *
     * @ORM\OneToMany(targetEntity="Keyword", mappedBy="client")
     */
    private $keyword;

    /**
     * @var Campaign[]
     *
     * @ORM\ManyToMany(targetEntity="Campaign", mappedBy="agent")
     */
    private $campaigns_agent;
	// * @ORM\OneToMany(targetEntity="Campaign", mappedBy="agent")

    /**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="agent")
     */
    private $messages_agent;

	/**
     * @var Message[]
     *
     * @ORM\OneToMany(targetEntity="Message", mappedBy="assignedAgent")
     */
    private $messages_assignedAgent;

	/**
	 * @var ClientSettings[]
	 *
	 * @ORM\OneToMany(targetEntity="ClientSettings", mappedBy="lastAssignedAgent")
	 */
	private $messages_lastAssignedAgent;

	/**
	 * @var ClientSettingsSMTP[]
	 *
	 * @ORM\OneToMany(targetEntity="ClientSettingsSMTP", mappedBy="client")
	 */
	private $clientSettingsSMTP;

	/**
	 * @ORM\Column(name="messagesReply", type="string", nullable=true)
	 * @var string
	 */
	private $messagesReply;

	/**
	 * @ORM\Column(name="client", type="string", nullable=true)
	 * @var string
	 */
	private $client;

	/**
     * @Assert\Regex(
     *  pattern="/(?=.*[0-9]).{6,}/",
     *  message="Password must be six or more characters long and contain at least one digit."
     * )
     */
    protected $plainPassword;


	public function __construct()
	{
	    parent::__construct();
	    // your own logic
	    $this->consultants = new ArrayCollection();
	    $this->clientSettings = new ArrayCollection();
	    $this->lead_imports = new ArrayCollection();
	    $this->campaigns = new ArrayCollection();
	    $this->keyword = new ArrayCollection();
	    $this->campaigns_agent = new ArrayCollection();
	    $this->messages_agent = new ArrayCollection();
	    $this->messages_assignedAgent = new ArrayCollection();
	    $this->messages_lastAssignedAgent = new ArrayCollection();
	    $this->clientSettingsSMTP = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getOffUser() {
		return $this->offUser;
	}

	/**
	 * @param string $offUser
	 */
	public function setOffUser($offUser) {
		return $this->offUser = $offUser;

		return $offUser;
	}

	/**
	 * @return string
	 */
	public function getCompanyName() {
		return $this->companyName;
	}

	/**
	 * @param string $name
	 */
	public function setCompanyName($companyName) {
		return $this->companyName = $companyName;

		return $companyName;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		return $this->name = $name;

		return $name;
	}


	/**
	 * @return string
	 */
	public function getLastName() {
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName) {
		return $this->lastName = $lastName;

		return $lastName;
	}


	/**
	* @param string $phone
	*
	* @return $this
	*/
	public function setPhone($phone) {
	   $this->phone = $phone;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getPhone() {
	   return $this->phone;
	}


	/**
	* @param string $address
	*
	* @return $this
	*/
	public function setAddress($address) {
	   $this->address = $address;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getAddress() {
	   return $this->address;
	}


	/**
	* @param string $postCode
	*
	* @return $this
	*/
	public function setPostCode($postCode) {
	   $this->postCode = $postCode;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getPostCode() {
	   return $this->postCode;
	}


	/**
	* @param string $city
	*
	* @return $this
	*/
	public function setCity($city) {
	   $this->city = $city;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getCity() {
	   return $this->city;
	}

	/**
	* @param string $personDescription
	*/
	public function setPersonDescription($personDescription)
	{
	   $this->personDescription = $personDescription;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getPersonDescription()
	{
	   return $this->personDescription;
	}

	/**
	* @param string $footer
	*/
	public function setFooter($footer)
	{
	   $this->footer = $footer;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getFooter()
	{
	   return $this->footer;
	}

	/**
	* @return boolean
	*/
	public function getAdminUser(){
	   return $this->adminUser;
	}

	/**
	* @param boolean $adminUser
	*
	* @return $this
	*/
	public function setAdminUser($adminUser) {
	  $this->adminUser = $adminUser;

	  return $this;
	}

	/**
	* @param string $typeUser
	*
	* @return $this
	*/
	public function setTypeUser($typeUser) {
	   $this->typeUser = $typeUser;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getTypeUser() {
	   return $this->typeUser;
	}

    /**
     * @return Consultant[]|ArrayCollection
     */
    public function getConsultants() {
        return $this->consultants;
    }

    /**
     * @param Consultant $consultant
     *
     * @return $this
     */
    public function setConsultant(Consultant $consultant) {
        $this->consultants[] = $consultant;

        return $this;
    }

    /**
     * @return ClientSettings[]|ArrayCollection
     */
    public function getClientSettings() {
        return $this->clientSettings;
    }

    /**
     * @param ClientSettings $clientSettings
     *
     * @return $this
     */
    public function setClientSettings(ClientSettings $clientSettings) {
        $this->clientSettings[] = $clientSettings;

        return $this;
    }

    /**
     * @return LeadImport[]|ArrayCollection
     */
    public function getLeadImports() {
        return $this->lead_imports;
    }

    /**
     * @param LeadImport $lead_imports
     *
     * @return $this
     */
    public function setLeadImports(LeadImport $lead_imports) {
        $this->lead_imports[] = $lead_imports;

        return $this;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaigns() {
        return $this->campaigns;
    }

    /**
     * @param Campaign $campaigns
     *
     * @return $this
     */
    public function setCampaigns(Campaign $campaigns) {
        $this->campaigns[] = $campaigns;

        return $this;
    }

	/**
     * @return Keyword[]|ArrayCollection
     */
    public function getKeyword() {
        return $this->keyword;
    }

    /**
     * @param Keyword $keyword
     *
     * @return $this
     */
    public function setKeyword(Keyword $keyword) {
        $this->keyword[] = $keyword;

        return $this;
    }

    /**
     * @return Campaign[]|ArrayCollection
     */
    public function getCampaignsAgent() {
        return $this->campaigns_agent;
    }

    /**
     * @param Campaign $campaigns_agent
     *
     * @return $this
     */
    public function setCampaignsAgent(Campaign $campaigns_agent) {
        $this->campaigns_agent[] = $campaigns_agent;

        return $this;
    }

	/**
	 * @return Message[]|ArrayCollection
	 */
	public function getMessagesAgent() {
		return $this->messages_agent;
	}

	/**
	 * @param Message $messages_agent
	 *
	 * @return $this
	 */
	public function setMessagesAgent(Message $messages_agent) {
		$this->messages_agent[] = $messages_agent;

		return $this;
	}

	/**
	 * @return Message[]|ArrayCollection
	 */
	public function getMessages_assignedAgent() {
		return $this->messages_assignedAgent;
	}

	/**
	 * @param Message $messages_assignedAgent
	 *
	 * @return $this
	 */
	public function setMessages_assignedAgent(Message $messages_assignedAgent) {
		$this->messages_assignedAgent[] = $messages_assignedAgent;

		return $this;
	}

	/**
	 * @return ClientSettings[]|ArrayCollection
	 */
	public function getMessages_lastAssignedAgent() {
		return $this->messages_lastAssignedAgent;
	}

	/**
	 * @param ClientSettings $messages_lastAssignedAgent
	 *
	 * @return $this
	 */
	public function setMessages_lastAssignedAgent(ClientSettings $messages_lastAssignedAgent) {
		$this->messages_lastAssignedAgent[] = $messages_lastAssignedAgent;

		return $this;
	}

	/**
	* @param string $messagesReply
	*
	* @return $this
	*/
	public function setMessagesReply($messagesReply) {
	   $this->messagesReply = $messagesReply;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getMessagesReply() {
	   return $this->messagesReply;
	}

    /**
     * @return ClientSettingsSMTP[]|ArrayCollection
     */
    public function getClientSettingsSMTP() {
        return $this->clientSettingsSMTP;
    }

    /**
     * @param ClientSettingsSMTP $clientSettingsSMTP
     *
     * @return $this
     */
    public function setClientSettingsSMTP(ClientSettingsSMTP $clientSettingsSMTP) {
        $this->clientSettingsSMTP[] = $clientSettingsSMTP;

        return $this;
    }

	/**
	* @param string $client
	*
	* @return $this
	*/
	public function setClient($client) {
	   $this->client = $client;

	   return $this;
	}

	/**
	* @return string
	*/
	public function getClient() {
	   return $this->client;
	}

	/**
	* @return string
	*/
	public function getFullName(){
	 // return $this->name .' '. $this->lastName;
	 return $this->name;
	}
}
