<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="message")
 */
class Message
{

	const STATUS_SENT = "sent";
	const STATUS_RECEIVED = "received";
	const INBOX_TYPE_INBOX = "inbox";
	const INBOX_TYPE_OTHER = "other";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="message_id", type="text", nullable=true)
	 * @var string
	 */
	private $messageId;

	/**
	 * @ORM\Column(name="messages_group", type="string", nullable=true)
	 * @var string
	 */
	private $messagesGroup;

	/**
	 * @ORM\Column(name="message_text", type="text", nullable=true)
	 * @var string
	 */
	private $messageText;

	/**
	 * @ORM\Column(name="message_date", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $messageDate;

	/**
	 * @ORM\Column(name="message_from", type="string", nullable=true)
	 * @var string
	 */
	private $messageFrom;

	/**
	 * @ORM\Column(name="message_to", type="string", nullable=true)
	 * @var string
	 */
	private $messageTo;

	/**
	 * @ORM\Column(name="inbox_type", type="string", nullable=true)
	 * @var string
	 */
	private $inboxType;

	/**
	 * @ORM\Column(name="status", type="string", nullable=true)
	 * @var string
	 */
	private $status;

	/**
	 * @ORM\Column(name="number_in", type="boolean", nullable=true)
	 * @var boolean
	 */
	private $numberIn;

    /**
	 * @var Campaign
	 * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="messages")
	 * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="messages_agent")
	 * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
	 */
	private $agent;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="messages_assignedAgent")
	 * @ORM\JoinColumn(name="assignedAgent_id", referencedColumnName="id")
	 */
	private $assignedAgent;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getMessageId() {
		return $this->messageId;
	}

	/**
	 * @param string $messageId
	 */
	public function setMessageId($messageId) {
		return $this->messageId = $messageId;

		return $messageId;
	}

	/**
	 * @return string
	 */
	public function getMessagesGroup() {
		return $this->messagesGroup;
	}

	/**
	 * @param string $messagesGroup
	 */
	public function setMessagesGroup($messagesGroup) {
		return $this->messagesGroup = $messagesGroup;

		return $messagesGroup;
	}

	/**
	 * @return string
	 */
	public function getMessageText() {
		return $this->messageText;
	}

	/**
	 * @param string $messageText
	 */
	public function setMessageText($messageText) {
		return $this->messageText = $messageText;

		return $messageText;
	}

	/**
	 * @return \DateTime
	 */
	public function getMessageDate() {
		return $this->messageDate;
	}

	/**
	 * @param \DateTime $messageDate
	 * @return $this
	 */
	public function setMessageDate(\DateTime $messageDate) {
		return $this->messageDate = $messageDate;

		return $messageDate;
	}

	/**
	 * @return string
	 */
	public function getMessageFrom() {
		return $this->messageFrom;
	}

	/**
	 * @param string $messageFrom
	 */
	public function setMessageFrom($messageFrom) {
		return $this->messageFrom = $messageFrom;

		return $messageFrom;
	}

	/**
	 * @return string
	 */
	public function getMessageTo() {
		return $this->messageTo;
	}

	/**
	 * @param string $messageTo
	 */
	public function setMessageTo($messageTo) {
		return $this->messageTo = $messageTo;

		return $messageTo;
	}

	/**
	 * @return string
	 */
	public function getInboxType() {
		return $this->inboxType;
	}

	/**
	 * @param string $inboxType
	 */
	public function setInboxType($inboxType) {
		return $this->inboxType = $inboxType;

		return $inboxType;
	}

	/**
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status) {
		return $this->status = $status;

		return $status;
	}

	/**
	 * @return boolean
	 */
	public function getNumberIn() {
		return $this->numberIn;
	}

	/**
	 * @param boolean $numberIn
	 */
	public function setNumberIn($numberIn) {
		return $this->numberIn = $numberIn;

		return $numberIn;
	}

    /**
     * @param Campaign $campaign
     *
     * @return $this
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

	/**
	 * @param User $agent
	 *
	 * @return $this
	 */
	public function setAgent(User $agent) {
		$this->agent = $agent;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getAgent() {
		return $this->agent;
	}

	/**
	 * @param User $assignedAgent
	 *
	 * @return $this
	 */
	public function setAssignedAgent(User $assignedAgent) {
		$this->assignedAgent = $assignedAgent;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getAssignedAgent() {
		return $this->assignedAgent;
	}
}
