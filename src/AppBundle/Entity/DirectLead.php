<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="direct_lead")
 */
class DirectLead
{

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="direct_who", type="string", nullable=true)
	 * @var string
	 */
	private $directWho;

	/**
	 * @ORM\Column(name="direct_lead", type="string", nullable=true)
	 * @var string
	 */
	private $directLead;

	/**
	 * @ORM\Column(name="direct_to", type="string", nullable=true)
	 * @var string
	 */
	private $directTo;

	/**
	 * @ORM\Column(name="direct_date", type="datetime", nullable=true)
	 * @var \DateTime
	 */
	private $directDate;

    /**
	 * @var Campaign
	 * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="directLeads")
	 * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return \DateTime
	 */
	public function getDirectDate() {
		return $this->directDate;
	}

	/**
	 * @param \DateTime $directDate
	 * @return $this
	 */
	public function setDirectDate(\DateTime $directDate) {
		return $this->directDate = $directDate;

		return $directDate;
	}

	/**
	 * @return string
	 */
	public function getDirectWho() {
		return $this->directWho;
	}

	/**
	 * @param string $directWho
	 */
	public function setDirectWho($directWho) {
		return $this->directWho = $directWho;

		return $directWho;
	}

	/**
	 * @return string
	 */
	public function getDirectLead() {
		return $this->directLead;
	}

	/**
	 * @param string $directLead
	 */
	public function setDirectLead($directLead) {
		return $this->directLead = $directLead;

		return $directLead;
	}

	/**
	 * @return string
	 */
	public function getDirectTo() {
		return $this->directTo;
	}

	/**
	 * @param string $directTo
	 */
	public function setDirectTo($directTo) {
		return $this->directTo = $directTo;

		return $directTo;
	}

    /**
     * @param Campaign $campaign
     *
     * @return $this
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }
}
