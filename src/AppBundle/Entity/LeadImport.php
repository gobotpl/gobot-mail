<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Lead;
use AppBundle\Entity\User;
use AppBundle\Entity\Campaign;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="lead_import")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LeadImportRepository")
 */
class LeadImport
{
	const TYPE_NORMAL = "normal";
	const TYPE_TEST = "test";
	const STAGE2 = "stage2";
	const STAGES_FINISH = "finish";

	const TYPE_BASE_AVAILABLE = "available";
	const TYPE_BASE_ARCHIVED = "archived";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="type", type="string", nullable=true)
	 * @var string
	 */
	private $type;

	/**
	 * @ORM\Column(name="typeBase", type="string", nullable=true)
	 * @var string
	 */
	private $typeBase;

	/**
	 * @ORM\Column(name="baseParent", type="string", nullable=true)
	 * @var string
	 */
	private $baseParent;

	/**
	 * @ORM\Column(name="stage", type="string", nullable=true)
	 * @var string
	 */
	private $stage;

	/**
	 * @ORM\Column(name="name", type="string", nullable=true)
	 * @var string
	 * @Assert\NotBlank(
	 * 	message="Name can't be empty"
	 * )
	 */
	private $name;

	/**
	 * @ORM\Column(name="description", type="text", nullable=true)
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\Column(name="labels", type="string", nullable=true)
	 * @var string
	 */
	private $labels;

    /**
     * @var Lead[]
     *
     * @ORM\OneToMany(targetEntity="Lead", mappedBy="lead_import", cascade={"remove"})
     */
    private $leads;

	/**
	 * @ORM\Column(name="dateOfImport", type="datetime", nullable=true)
	 * @Gedmo\Timestampable(on="create")
	 * @var \DateTime
	 */
	private $dateOfImport;

	/**
	 * @ORM\Column(name="file", type="string", nullable=true)
	 * @var string
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"text/xml", "application/xml"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes like XML are allowed.",
     *     groups = {"create"}
     * )
	 */
	private $file;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="lead_imports")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

    /**
     * @var Campaign[]
     *
     * @ORM\OneToMany(targetEntity="Campaign", mappedBy="lead_import")
     */
    private $campaigns_lead_import;

    /**
     * Project constructor.
     */
    public function __construct() {
        $this->leads = new ArrayCollection();
	    $this->campaigns_lead_import = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type) {
		return $this->type = $type;

		return $type;
	}

	/**
	 * @return string
	 */
	public function getTypeBase() {
		return $this->typeBase;
	}

	/**
	 * @param string $typeBase
	 */
	public function setTypeBase($typeBase) {
		return $this->typeBase = $typeBase;

		return $typeBase;
	}

	/**
	 * @return string
	 */
	public function getBaseParent() {
		return $this->baseParent;
	}

	/**
	 * @param string $baseParent
	 */
	public function setBaseParent($baseParent) {
		return $this->baseParent = $baseParent;

		return $baseParent;
	}

	/**
	 * @return string
	 */
	public function getStage() {
		return $this->stage;
	}

	/**
	 * @param string $stage
	 */
	public function setStage($stage) {
		return $this->stage = $stage;

		return $stage;
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		return $this->name = $name;

		return $name;
	}

	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		return $this->description = $description;

		return $description;
	}

	/**
	 * @return string
	 */
	public function getLabels() {
		return $this->labels;
	}

	/**
	 * @param string $labels
	 */
	public function setLabels($labels) {
		return $this->labels = $labels;

		return $labels;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateOfImport() {
		return $this->dateOfImport;
	}

	/**
	 * @param \DateTime $dateOfImport
	 * @return $this
	 */
	public function setDateOfImport(\DateTime $dateOfImport) {
		return $this->dateOfImport = $dateOfImport;

		return $dateOfImport;
	}

    /**
     * @return Lead[]|ArrayCollection
     */
    public function getLeads() {
        return $this->leads;
    }

	/**
	 * @param Lead $leads
	 *
	 * @return $this
	 */
	public function setLeads(Campaign $leads) {
		$this->leads[] = $leads;

		return $this;
	}

    /**
     * @param Lead $lead
     *
     * @return $this
     */
    public function setTask(Lead $lead) {
        $this->leads[] = $lead;

        return $this;
    }

	/**
	 * @return string
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * @param string $file
	 */
	public function setFile($file) {
		return $this->file = $file;

		return $file;
	}

    /**
     * @param User $client
     *
     * @return $this
     */
    public function setClient(User $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient() {
        return $this->client;
    }

	/**
	 * @return Campaign[]|ArrayCollection
	 */
	public function getCampaignsLeadImport() {
		return $this->campaigns_lead_import;
	}

	/**
	 * @param Campaign $campaigns_lead_import
	 *
	 * @return $this
	 */
	public function setCampaignsLeadImport(Campaign $campaigns_lead_import) {
		$this->campaigns_lead_import[] = $campaigns_lead_import;

		return $this;
	}


}
