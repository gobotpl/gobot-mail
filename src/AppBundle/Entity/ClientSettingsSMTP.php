<?php

namespace AppBundle\Entity;

use AppBundle\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\LeadImport;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="client_settings_smtp")
 */
class ClientSettingsSMTP
{

	const ENCRYPTION_TLS = "tls";
	const ENCRYPTION_SSL = "ssl";

	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="bot_email", type="string", nullable=true)
	 * @var string
	 */
	private $botEmail;

	/**
	 * @ORM\Column(name="bot_password", type="string", nullable=true)
	 * @var string
	 */
	private $botPassword;

	/**
	 * @ORM\Column(name="bot_smtp_port", type="string", nullable=true)
	 * @var string
	 */
	private $botSmtpPort;

	/**
	 * @ORM\Column(name="bot_smtp_encryption", type="string", nullable=true)
	 * @var string
	 */
	private $botSmtpEncryption;

	/**
	 * @ORM\Column(name="bot_server", type="string", nullable=true)
	 * @var string
	 */
	private $botServer;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="clientSettingsSMTP")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getBotEmail() {
		return $this->botEmail;
	}

	/**
	 * @param string $botEmail
	 */
	public function setBotEmail($botEmail) {
		return $this->botEmail = $botEmail;

		return $botEmail;
	}

	/**
	 * @return string
	 */
	public function getBotPassword() {
		return $this->botPassword;
	}

	/**
	 * @param string $botPassword
	 */
	public function setBotPassword($botPassword) {
		return $this->botPassword = $botPassword;

		return $botPassword;
	}

	/**
	 * @return string
	 */
	public function getBotSmtpPort() {
		return $this->botSmtpPort;
	}

	/**
	 * @param string $botSmtpPort
	 */
	public function setBotSmtpPort($botSmtpPort) {
		return $this->botSmtpPort = $botSmtpPort;

		return $botSmtpPort;
	}

	/**
	 * @return string
	 */
	public function getBotSmtpEncryption() {
		return $this->botSmtpEncryption;
	}

	/**
	 * @param string $botSmtpEncryption
	 */
	public function setBotSmtpEncryption($botSmtpEncryption) {
		return $this->botSmtpEncryption = $botSmtpEncryption;

		return $botSmtpEncryption;
	}

	/**
	 * @return string
	 */
	public function getBotServer() {
		return $this->botServer;
	}

	/**
	 * @param string $botServer
	 */
	public function setBotServer($botServer) {
		return $this->botServer = $botServer;

		return $botServer;
	}

    /**
     * @param User $client
     *
     * @return $this
     */
    public function setClient(User $client) {
        $this->client = $client;

        return $this;
    }

    /**
     * @return User
     */
    public function getClient() {
        return $this->client;
    }

}
