<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Campaign;
use AppBundle\Entity\KeywordMail;
use AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="keyword")
 */
class Keyword
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="name", type="string", nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(name="baseParent", type="string", nullable=true)
	 * @var string
	 */
	private $baseParent;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(targetEntity="User", inversedBy="keyword")
	 * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
	 */
	private $client;

	/**
	 * @var KeywordMail[]
	 *
	 * @ORM\OneToMany(targetEntity="KeywordMail", mappedBy="keyword", cascade={"remove"})
	 */
	private $keywordMails;

	/**
     * @var KeywordMail[]
     *
     * @ORM\ManyToMany(targetEntity="KeywordMail", mappedBy="keywords")
     */
	private $keywordMailsKeyword;

    /**
	 * @var Campaign
	 * @ORM\ManyToOne(targetEntity="Campaign", inversedBy="keywords")
	 * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;

	/**
	 * Project constructor.
	 */
	public function __construct() {
		$this->keywordMails = new ArrayCollection();
	    $this->keywordMailsKeyword = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name) {
		return $this->name = $name;

		return $name;
	}

	/**
	 * @return string
	 */
	public function getBaseParent() {
		return $this->baseParent;
	}

	/**
	 * @param string $baseParent
	 */
	public function setBaseParent($baseParent) {
		return $this->baseParent = $baseParent;

		return $baseParent;
	}

	/**
	 * @param User $client
	 *
	 * @return $this
	 */
	public function setClient(User $client) {
		$this->client = $client;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getClient() {
		return $this->client;
	}

	/**
	 * @return KeywordMail[]|ArrayCollection
	 */
	public function getKeywordMails() {
		return $this->keywordMails;
	}

	/**
	 * @param KeywordMail $keywordMails
	 *
	 * @return $this
	 */
	public function setKeywordMails(KeywordMail $keywordMails) {
		$this->keywordMails[] = $keywordMails;

		return $this;
	}

	/**
     * @return KeywordMail[]|ArrayCollection
     */
    public function getKeywordMailsKeyword() {
        return $this->keywordMailsKeyword;
    }

    /**
     * @param KeywordMail $keywordMailsKeyword
     *
     * @return $this
     */
    public function setKeywordMailsKeyword(KeywordMail $keywordMailsKeyword) {
        $this->keywordMailsKeyword[] = $keywordMailsKeyword;

        return $this;
    }

	/**
     * @param Campaign $campaign
     *
     * @return $this
     */
    public function setCampaign(Campaign $campaign) {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() {
        return $this->campaign;
    }

}
