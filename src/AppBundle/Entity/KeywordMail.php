<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Keyword;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="keyword_mail")
 */
class KeywordMail
{
	/**
	 * @var int
	 *
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(name="title", type="string", nullable=true)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(name="message_text", type="text", nullable=true)
	 * @var string
	 */
	private $messageText;

	/**
	 * @var Keyword
	 *
	 * @ORM\ManyToOne(targetEntity="Keyword", inversedBy="keywordMails")
	 * @ORM\JoinColumn(name="keyword_id", referencedColumnName="id")
	 */
	private $keyword;

	/**
	 * @var Keyword
	 *
	 * @ORM\ManyToMany(targetEntity="Keyword", inversedBy="keywordMailsKeyword", cascade={"remove"})
	 * @ORM\JoinTable(
     *  name="keywordmail_keyword",
     *  joinColumns={
     *      @ORM\JoinColumn(name="keyword_mail_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="keyword_id", referencedColumnName="id")
     *  }
     * )
     */
	private $keywords;


	public function __construct()
	{
	    $this->keywords = new ArrayCollection();
	}


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		return $this->title = $title;

		return $title;
	}

	/**
	 * @return string
	 */
	public function getMessageText() {
		return $this->messageText;
	}

	/**
	 * @param string $messageText
	 */
	public function setMessageText($messageText) {
		return $this->messageText = $messageText;

		return $messageText;
	}

    /**
     * @param Keyword $keyword
     *
     * @return $this
     */
    public function setKeyword(Keyword $keyword) {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return Keyword
     */
    public function getKeyword() {
        return $this->keyword;
    }

	/**
     * @param Keyword $keywords
     *
     * @return $this
     */
    public function setKeywords(Keyword $keywords) {
        $this->keywords[] = $keywords;

        return $this;
    }

    /**
     * @return Keyword[]|ArrayCollection
     */
    public function getKeywords() {
        return $this->keywords;
    }

}
