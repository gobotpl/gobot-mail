/**
 * Polish translation for bootstrap-datetimepicker
 * Michal Lan <michal.lan@gobot.pl>
 */
;(function($){
	$.fn.datetimepicker.dates['pl-PL'] = {
		days: ["Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela"],
		daysShort: ["Nd", "Pn", "Wt", "Śr", "Czw", "Pt", "Sob", "Nd"],
		daysMin: ["Nd", "Pn", "Wt", "Śr", "Czw", "Pt", "Sob", "Nd"],
		months: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
		monthsShort: ["Sty", "Lut", "Mar", "Kwi", "Maj", "Cze", "Lip", "Sie", "Wrz", "Paź", "Lis", "Gru"],
		today: "Dzisiaj"
	};
}(jQuery));
