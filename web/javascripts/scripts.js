function asideHeight() {
	var windowHeight = window.innerHeight;
	var menuTopHeight = $(".aside-menu").offset().top;
	var footerHeight = $('.footer-text').height();
	var menuAsideHeight = windowHeight - menuTopHeight - footerHeight;

	$(".aside-menu").css('height', menuAsideHeight);
}

$(document).ready(function(){

	$('.open-menu').on('click', function(){
		$('.main').toggleClass('main-full');
	});

	$('.enableEdit').on('click', function(){
		var classVal = $(this).parent().attr('for');
		classVal = '#'+classVal;
		console.log(classVal);
		$(classVal).attr('readonly', false);
		$(classVal).prop('disabled', false);
	});

	asideHeight();

	$(window).on('resize', function () {
	    asideHeight();
	});


	// turn on ToolTip
    $( document ).tooltip();

    // register form Email to Login
    $('.register-email').keyup(function(){
        $('.register-login').val($(this).val());
    });


	// add label to form
	$('.addLabel').click(function(){

		if(parseInt($( ".labels_inner .label_box" ).last().attr('alt')) >= 0){
			var num = parseInt($( ".labels_inner .label_box" ).last().attr('alt')) + 1;
		} else {
			var num = 0;
		}

		if ($(".lead_import_edit").length){
			var Ninp = 'lead_import_edit';
		} else {
			var Ninp = 'form';
		}

		var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="'+Ninp+'[labels]['+num+'][name]" required="required"> Etykieta: <input type="text" name="'+Ninp+'[labels]['+num+'][label]" required="required" readonly="readonly"></div>';

		// var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="'+Ninp+'[labels]['+num+'][name]" required="required"> Etykieta: <input type="text" name="'+Ninp+'[labels]['+num+'][label]" required="required"> <span class="ok delete_label" alt="'+num+'">Usuń</span></div>';

		$( ".labels_inner" ).append( new_label );
	});

	// remove label from form
	$('.labels_inner').on('click',".delete_label", function(){
		var label_id = $(this).attr('alt');

		$('.label_box').each(function(index) {
		    if($(this).attr('alt') == label_id){
				$(this).remove();
			}
		});
	});

	// if stage2 load default_labels
	if ($(".import_stage2").length){
		// load variable form labels
		var labels_value = $('#default_labels').val();
		// clean hidden input
		$('#default_labels').val('');
		// data to array convert
		var data = labels_value.split('{label}');

		if(data.length > 0){
			$('.label_box').remove();

			// generate labels
			$.each(data, function(key, value) {
				var num = key;
				// set values
				value = value.split('[labelData]');
				// append each label from data
				var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="form[labels]['+num+'][name]" value="'+value[0]+'" required="required"> Etykieta: <input type="text" name="form[labels]['+num+'][label]" value="'+value[0]+'" required="required"  readonly="readonly"> Przykład danych: <input type="text" value="'+value[1]+'" readonly="readonly"></div>';

				// var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="form[labels]['+num+'][name]" value="'+value[0]+'" required="required"> Etykieta: <input type="text" name="form[labels]['+num+'][label]" value="'+value[0]+'" required="required"> <span class="ok delete_label" alt="'+num+'">Usuń</span></div>';

				$( ".labels_inner" ).append( new_label );
			});
		}
	}

	// if edit form label is current use
	if ($(".lead_import_edit").length){
		// load variable form labels
		var labels_value = $('#lead_import_edit_labels').val();
		// clean hidden input
		$('#lead_import_edit_labels').val('');
		// data to array convert
		var data = labels_value.split('{label}');
		var new_data = generateData(data);

		if(new_data.length > 0){
			$('.label_box').remove();

			// generate labels
			$.each(new_data, function(key, value) {
				var num = key;
				// append each label from data
				var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="lead_import_edit[labels]['+num+'][name]" value="'+value['name']+'" required="required"> Etykieta: <input type="text" name="lead_import_edit[labels]['+num+'][label]" value="'+value['label']+'" required="required" readonly="readonly"></div>';

				// var new_label = '<div class="label_box" alt="'+num+'">Nazwa: <input type="text" name="lead_import_edit[labels]['+num+'][name]" value="'+value['name']+'" required="required"> Etykieta: <input type="text" name="lead_import_edit[labels]['+num+'][label]" value="'+value['label']+'" required="required"> <span class="ok delete_label" alt="'+num+'">Usuń</span></div>';

				$( ".labels_inner" ).append( new_label );
			});
		}
	}


	// if add new lead form label load
	if ($(".lead_add").length){
		// load variable form labels
		var labels_value = $('#form_labels').val();
		// clean hidden input
		$('#form_labels').val('');
		// data to array convert
		var data = labels_value.split('{label}');
		var new_data = generateData(data);

		if(new_data.length > 0){
			$('.label_box').remove();

			// generate labels
			$.each(new_data, function(key, value) {
				var num = key;
				// append each label from data
				var new_label = '<div class="label_box" alt="'+num+'"><label class="label">'+value['name']+'</label><input type="text" name="form[labels]['+value['label']+']" value="" required="required"></div>';

				$( ".labels_inner" ).append( new_label );
			});
		}
	}

	// if edit lead form label load
	if ($(".lead_edit").length){
		// load names form labels
		var labels_names = $('#form_labels').val();
		// load variable form labels
		var labels_value = $('#form_labels_data').val();
		// delete values field
		$('#form_labels_data').remove();
		// clean hidden input
		$('#form_labels').val('');

		// data to array convert for names
		var data = labels_names.split('{label}');
		var new_data_names = generateData(data);

		// data to array convert for values
		var data = labels_value.split('{label}');
		var new_data = generateData(data);

		// check schema exists and remove label for new labels generate
		if(new_data.length > 0 && new_data_names.length > 0){
			$('.label_box').remove();

			// generate labels
			$.each(new_data, function(key, value) {
				var num = key;
				var label = value['name'];

				$.each(new_data_names, function(key_in, value_in) {
					if(label == value_in['label']){
						// append each label from data
						var new_label = '<div class="label_box" alt="'+num+'"><label class="label">'+value_in['name']+'</label><input type="text" name="form[labels]['+value['name']+']" value="'+value['label']+'" required="required"></div>';

						$( ".labels_inner" ).append( new_label );
					}
				});
			});
		}
	}

	// generate Data for Labels
	function generateData(data){
		var new_data = [];
		$.each(data, function(key, value) {
			value = value.split('[labelData]');
			new_data[key] = {};
			$.each(value, function(key_in, value_in) {
				if(key_in == 0){
					new_data[key]['name'] = value_in;
				} else {
					new_data[key]['label'] = value_in;
				}
			});
		});
		// return array
		return new_data;
	}


	// open and close modal in campaign
	$('.open-modal').click(function(){
		var data = $(this).attr('modal-data');
		$('.modal-box').each(function(index) {
			if($(this).attr('modal-name') == data){
				$(this).show();
			}
		});
	});
	$('.modal-close').click(function(){
		$('.modal-box').each(function(index) {
			$(this).hide();
		});
	});
	// clean debugger for require display none field
	$('.modal-box').each(function(index) {
		$(this).find('textarea').removeAttr('required');
	});
    // disable error debugger for Days and Hours inputs
    $('.mails_box').each(function(index) {
        $('.mail_date_dh').find('input').removeAttr('required');
    });
	// add label to campaign mail text
	// $('.campaign-labels').on('click',".campaign-label", function(){
	// 	var data = $(this).html();
	// 	var textarea = $(this).closest('.modal-box').find('textarea');
    //
	// 	var cursorPos = textarea.prop('selectionStart');
	// 	var v = textarea.val();
	// 	var textBefore = v.substring(0,  cursorPos);
	// 	var textAfter  = v.substring(cursorPos, v.length);
    //
	// 	textarea.focus().val(textBefore + data + textAfter);
	// });

	// after select load labels for mailing
	$('#campaign_lead_import, #campaign_edit_lead_import').on('change', function() {

		// if selected "none" - remove older items and add default
		if($(this).val() == ""){
            // hide mailEditors
            $(".mailsEditors").hide();
			$(".label_select_name option").each(function(){
				$(this).remove();
			});
			$(".label_select_name").each(function(){
				$(this).append('<option>Wybierz Etykietę</option>');
			});
		} else {
			var labels_data = $('.campaign-labels');
			var labels_select_data = $('.label_select_name');
			// clean labels data
			labels_data.html("");
			// clean select labels
			labels_select_data.html("");
			// load data
			$.ajax({
	            type: "post",
	            url: "/panel/campaign/ajaxlabels/"+($(this).val()).toString(),
	            // data: $(this).serialize(),
				data: "",
				processData: false,
				contentType: false,
				cache: false,
				dataType:"json",
	        })
	        .done(function (data) {
				// if done, check message and show success
	            if (typeof data.message !== 'undefined') {
					var labels = $.parseJSON(data.message);
					// add all labels like li
					$.each(labels, function (index, value) {
						var new_label = '<li class="campaign-label" title="'+value['name']+'">{{'+value['label']+'}}</li>';
						labels_data.append( new_label );
					});
					// add labels to select in conditions
					$.each(labels, function (index, value) {
						var new_label = '<option value="'+value['label']+'">'+value['label']+' ('+value['name']+')</option>';
						labels_select_data.append( new_label );
					});

                    // when labels are loaded load TinyMCE for all editor and show mailEditors box
                    // generate buttons from labels
                    var buttonsNames = "";
                    var buttons = "";
                    $.each(labels, function (index, value) {
                        if(index == 0){
                            buttonsNames = '{\{'+value['label']+'}}';
                        } else {
                            buttonsNames = buttonsNames + ' | {\{'+value['label']+'}}';
                        }

                        buttons = buttons + "editor.addButton('{\{"+value['label']+"}}', { text: '{\{"+value['label']+"}}', icon: false, tooltip: '"+value['name']+"', onclick: function () { editor.insertContent(' {\{"+value['label']+"}} '); } }); ";
					});
                    // load TinyMCE
                    loadTiny("#campaign_mail_text1", buttonsNames, buttons);
                    loadTiny("#campaign_mail_text2", buttonsNames, buttons);
                    loadTiny("#campaign_mail_text3", buttonsNames, buttons);
                    loadTiny("#campaign_mail_text4", buttonsNames, buttons);
                    loadTiny("#campaign_mail_text5", buttonsNames, buttons);
                    // show mailEditors
                    $(".mailsEditors").show();

	            } else {
					labels_data.html("Problem z ładowaniem etykiet");
				}
	        })
			 .fail(function (jqXHR, textStatus, errorThrown) {
				$('.countLeads').html("Problem z ładowaniem etykiet, możliwe że nie są uzupełnione!");
				labels_data.html("Problem z ładowaniem etykiet");
	        });
	        // .fail(function (jqXHR, textStatus, errorThrown) {
			// 	// after fail check response and show error message
	        //     if (typeof jqXHR.responseJSON !== 'undefined') {
	        //         $('.countLeads').html(jqXHR.responseJSON.message);
	        //     } else {
			// 		$('.countLeads').html(errorThrown);
	        //     }
	        // });
		}
	});

	// add condition to campaign
	$('.addCondition').click(function(){
		// check number of condition
		if(parseInt($( ".labels_inner .label_box" ).last().attr('alt')) >= 0){
			var num = parseInt($( ".labels_inner .label_box" ).last().attr('alt')) + 1;
		} else {
			var num = 0;
		}

		if ($(".campaign_edit").length){
			var Ninp = 'campaign_edit';
		} else {
			var Ninp = 'campaign';
		}
		// add labels to select options
		var labels = '';
		// $(".select_name_base option").each(function(){
		$(".label_select_name:first option").each(function(){
			labels += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
		});
		// add condition box
		var new_label = '<div class="label_box" alt="'+num+'"><select class="label_select_name" name="'+Ninp+'[conditions]['+num+'][name]">'+labels+'</select> <select name="'+Ninp+'[conditions]['+num+'][if]" class="select_if"><option value="==">jest równy</option><option value="!=">nie jest równy</option><option value=">">jest większy</option><option value="<">jest mniejszy</option><option value=">=">jest większy, bądź równy</option><option value="<=">jest mniejszy, bądź równy</option></select> Wartość: <input type="text" name="'+Ninp+'[conditions]['+num+'][value]" placeholder="Uzupełnij wartość np. Łukasz"> <span class="ok delete_condition" alt="'+num+'">Usuń</span></div>';
        // var new_label = '<div class="label_box" alt="'+num+'"><select class="label_select_name" name="'+Ninp+'[conditions]['+num+'][name]" required="required">'+labels+'</select> <select name="'+Ninp+'[conditions]['+num+'][if]" class="select_if" required="required"><option value="==">jest równy</option><option value="!=">nie jest równy</option><option value=">">jest większy</option><option value="<">jest mniejszy</option><option value=">=">jest większy, bądź równy</option><option value="<=">jest mniejszy, bądź równy</option></select> Wartość: <input type="text" name="'+Ninp+'[conditions]['+num+'][value]" required="required" placeholder="Uzupełnij wartość np. Łukasz"> <span class="ok delete_condition" alt="'+num+'">Usuń</span></div>';

		$( ".labels_inner" ).append( new_label );
	});

	// remove label from form
	$('.labels_inner').on('click',".delete_condition", function(){
		var label_id = $(this).attr('alt');

		$('.label_box').each(function(index) {
		    if($(this).attr('alt') == label_id){
				$(this).remove();
			}
		});
	});


	// change for active this mail in campaign - show text and date edit
	$('.campaign_mail_active').on('change', function() {
		if($(this).val() == 1){
			$(this).closest('.mails_box').find('.open-modal, .mail_date').css("display", "inline-block");
		} else {
			$(this).closest('.mails_box').find('.open-modal, .mail_date').hide();
		}
	});
	// add mail - display block only
	$('.addMail').click(function(){
		var MailNum = 1;
		$('.mails_box').each(function(index) {
			if($(this).css("display") == "block"){
				MailNum = parseInt($(this).attr('alt')) + 1;
			}
		});
		$('.mails_box').each(function(index) {
			if($(this).attr('alt') == MailNum){
				$(this).show();
			}
		});
		if(MailNum >= 5){
			$('.addMail').hide();
		}
		if(MailNum > 1){
			$('.removeMail').show();
		}
	});
	// remove mail - display none
	$('.removeMail').click(function(){
		var MailNum = 1;
		$('.mails_box').each(function(index) {
			if($(this).css("display") == "block"){
				MailNum = parseInt($(this).attr('alt'));
			}
		});
		$('.mails_box').each(function(index) {
			if($(this).attr('alt') == MailNum){
				$(this).hide();
				// hide and set NO for active
				$(this).find('.campaign_mail_active').val('0');
				$(this).closest('.mails_box').find('.open-modal, .mail_date').hide();
			}
		});
		if(MailNum <= 5){
			$('.addMail').show();
		}
		if(MailNum <= 2){
			$('.removeMail').hide();
		}
	});

	// activate first child
	$('.mails_box').each(function(index) {
		if($(this).attr('alt') == 1){
			$(this).show();
		}
		$('.removeMail').hide();
	});

	function countLeads(){
		if($('#campaign_lead_import, #campaign_edit_lead_import').val() != ""){

			var countLeads = $('.countLeads');
			// clean counter
			countLeads.html('');
			// ask server for leads
			var conditions = {};
			$('.conditions_box .label_box').each(function(index) {
				conditions[$(this).attr('alt')] = $(this).find('select, textarea, input').serializeArray();
			});

			var data = {
				'conditions': conditions,
				'lead_import': $('.campaign_add #campaign_lead_import, .campaign_edit #campaign_edit_lead_import').serializeArray(),
			}

			$.ajax({
				type: "post",
				url: "/panel/campaign/ajaxleads",
				data: JSON.stringify(data),
				processData: false,
				contentType: false,
				cache: false,
				dataType:"json",
			})
			.done(function (data) {
				// if done, check message and show success
				if (typeof data.message !== 'undefined') {
					var leads = $.parseJSON(data.message);
					countLeads.html('Dla wybranych warunków, znaleziono '+leads+' leadów.');
				} else {
					// countLeads.html("Problem z ładowaniem ilości leadów");
				}
			});
			// .fail(function (jqXHR, textStatus, errorThrown) {
			// 	// after fail check response and show error message
			// 	if (typeof jqXHR.responseJSON !== 'undefined') {
			// 		countLeads.html(jqXHR.responseJSON.message);
			// 	} else {
			// 		countLeads.html(errorThrown);
			// 	}
			// });
		}
	}

	// after out from condition or change select condition or Label - count Leads
	$(".conditions_box .labels_inner").on("focusout", "input", function() {
		countLeads();
	});
	$('.conditions_box .label_select_name, .conditions_box .select_if').on('change', function() {
		countLeads();
	});


	/*** Hover Label and show Name ***/

	// $('.campaign-labels').on('mouseover',".campaign-label", function(){
	// 	console.log($(this).attr('alt'));
	// });


	/*** ADD Campaign ***/
	// if edit lead form label load
	if ($(".campaign_add").length){
        // find not test Mailbase and set font-weight
        $("#campaign_lead_import option").each(function(){
            if($(this).val() != ""){
                if($(this).html().indexOf("- test") == -1){
                    $(this).css("font-weight", "700");
                }
            }
        });
    }

	/*** Edit Campaign ***/
	// if edit lead form label load
	if ($(".campaign_edit").length){

        // find not test Mailbase and set font-weight
        $("#campaign_edit_lead_import option").each(function(){
            if($(this).val() != ""){
                if($(this).html().indexOf("- test") == -1){
                    $(this).css("font-weight", "700");
                }
            }
        });

		// open all mails
		$('.campaign_mail_active').each(function(index) {
			if($(this).val() == 1){
				$(this).closest('.mails_box').show();
				$(this).closest('.mails_box').find('.open-modal, .mail_date').css("display", "inline-block");
			}
		});
		// load labels in mail editor
		var LeadImportVal = $('#campaign_edit_lead_import').val();
		var labels_data = $('.campaign-labels');
		var labels_select_data = $('.label_select_name');
		// clean labels data
		labels_data.html("");
		// clean select labels
		labels_select_data.html("");
		// load data
		$.ajax({
			type: "post",
			url: "/panel/campaign/ajaxlabels/"+(LeadImportVal).toString(),
			// data: $(this).serialize(),
			data: "",
			processData: false,
			contentType: false,
			cache: false,
			dataType:"json",
		})
		.done(function (data) {
			// if done, check message and show success
			if (typeof data.message !== 'undefined') {
				var labels = $.parseJSON(data.message);
				// add all labels like li
				$.each(labels, function (index, value) {
					var new_label = '<li class="campaign-label" title="'+value['name']+'">{{'+value['label']+'}}</li>';
					labels_data.append( new_label );
				});
				// add labels to select in conditions
				$.each(labels, function (index, value) {
					var new_label = '<option value="'+value['label']+'">'+value['label']+' ('+value['name']+')</option>';
					labels_select_data.append( new_label );
				});

                // when labels are loaded load TinyMCE for all editor and show mailEditors box
                // generate buttons from labels
                var buttonsNames = "";
                var buttons = "";
                $.each(labels, function (index, value) {
                    if(index == 0){
                        buttonsNames = '{\{'+value['label']+'}}';
                    } else {
                        buttonsNames = buttonsNames + ' | {\{'+value['label']+'}}';
                    }

                    buttons = buttons + "editor.addButton('{\{"+value['label']+"}}', { text: '{\{"+value['label']+"}}', icon: false, tooltip: '"+value['name']+"', onclick: function () { editor.insertContent(' {\{"+value['label']+"}} '); } }); ";
				});
                // load TinyMCE
                loadTiny("#campaign_edit_mail_text1", buttonsNames, buttons);
                loadTiny("#campaign_edit_mail_text2", buttonsNames, buttons);
                loadTiny("#campaign_edit_mail_text3", buttonsNames, buttons);
                loadTiny("#campaign_edit_mail_text4", buttonsNames, buttons);
                loadTiny("#campaign_edit_mail_text5", buttonsNames, buttons);
                // show mailEditors
                $(".mailsEditors").show();




				/*** After Load Conditions Names ***/

				// change conditions from Campaign field
				// load variable form labels
				var labels_value = $('#campaign_edit_conditions').val();
				// clean hidden input
				$('#campaign_edit_conditions').val('');
				// data to array convert for values
				var data = labels_value.split('{if}');
				var new_data = [];
				// each condition get values
				$.each(data, function(key, value) {
					var value_val = value.split('[value]');
					var value_if = value_val[0].split('[if]');
					var value_name = value_if[0].split('[name]');
					var cVal = value_val[1];
					var cIf = value_if[1];
					var cName = value_name[1];
					new_data[key] = [];
					new_data[key].push(cName);
					new_data[key].push(cIf);
					new_data[key].push(cVal);
				});

				// check schema exists and remove label for new labels generate
				if(new_data.length > 0){
					var Ninp = 'campaign_edit';
					// generate labels
					$.each(new_data, function(key, value) {
						var num = key;
						// add labels to select options
						var labels = '';
						$(".select_name_base option").each(function(){
							if($(this).val() == value[0]){
								labels += '<option value="'+$(this).val()+'" selected="selected">'+$(this).text()+'</option>';
							} else {
								labels += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
							}
						});
						var ifs = '';
						$(".select_if:first option").each(function(){
							if($(this).val() == value[1]){
								ifs += '<option value="'+$(this).val()+'" selected="selected">'+$(this).text()+'</option>';
							} else {
								ifs += '<option value="'+$(this).val()+'">'+$(this).text()+'</option>';
							}
						});
						var deleteButton = '';
						if(num > 0){
							deleteButton = '<span class="ok delete_condition" alt="'+num+'">Usuń</span>';
						}
						// add condition box
						var new_label = '<div class="label_box" alt="'+num+'"><select class="label_select_name" name="'+Ninp+'[conditions]['+num+'][name]">'+labels+'</select> <select name="'+Ninp+'[conditions]['+num+'][if]" class="select_if">'+ifs+'</select> Wartość: <input type="text" name="'+Ninp+'[conditions]['+num+'][value]" placeholder="Uzupełnij wartość np. Łukasz" value="'+value[2]+'"> '+deleteButton+'</div>';
						// var new_label = '<div class="label_box" alt="'+num+'"><select class="label_select_name" name="'+Ninp+'[conditions]['+num+'][name]" required="required">'+labels+'</select> <select name="'+Ninp+'[conditions]['+num+'][if]" class="select_if" required="required">'+ifs+'</select> Wartość: <input type="text" name="'+Ninp+'[conditions]['+num+'][value]" required="required" placeholder="Uzupełnij wartość np. Łukasz" value="'+value[2]+'"> '+deleteButton+'</div>';

						$( ".labels_inner" ).append( new_label );

					});

					// delete defaults conditions
					$('.select_name_base').closest('.label_box').remove();
				}

			} else {
				labels_data.html("Problem z ładowaniem etykiet");
			}
		})
		.fail(function (jqXHR, textStatus, errorThrown) {
		   $('.countLeads').html("Problem z ładowaniem etykiet, możliwe że nie są uzupełnione!");
		   labels_data.html("Problem z ładowaniem etykiet");
	   });

	}


	/* Block And allow to edit ClientServersSMTP */
	$('.clientSettingsSMTPServers input, .clientSettingsSMTPServers select').each(function(index) {
		$(this).prop('disabled', true);
	});
	$('.hidden_edit_button').click(function(){
		$(this).closest('tr').find('.hidden_edit_button').hide();
		$(this).closest('tr').find('.hidden_save_button').css("cssText", "display: inline-block !important");
		$(this).closest('tr').find('.deleteForm').css("cssText", "display: inline-block !important");
		$(this).closest('tr').find('input, select').each(function(index) {
			$(this).prop('disabled', false);
		});
	});

    // show client after select in message active
	$('.clientActiveMessageSelect').on('change', function() {
        $('.clientActiveMessageHide').hide();
        var selectedId = $(this).val();
		$('.clientActiveMessageHide').each(function(index) {
		    if($(this).attr('alt') == selectedId){
    			$(this).show();
            }
		});
	});






    // function to Load TinyMCE !
    function loadTiny(element, buttonsNames = null, buttons = null){
        // check load TinyMCE is for mail text or other
        if(buttonsNames == null && buttons == null){
            var initConfig = {
                selector: element,
                language: 'pl',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern imagetools"
                    ],
                toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link     image",
                toolbar2: "print media | forecolor backcolor emoticons | preview",
                image_advtab: true,
            };
        } else {
            // can add to plugin "autoresize " for autoresize editor height
            var initConfig = {
                selector: element,
                language: 'pl',
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern imagetools"
                    ],
                toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link     image",
                toolbar2: "print media | forecolor backcolor emoticons | preview",
                toolbar3: buttonsNames,
                image_advtab: true,
                setup: function (editor) { eval(buttons); }
            };
        }
        tinymce.init(initConfig);
    }
    // clean datetime function
    // function getFormattedDate(date) {
    //     var day = date.getDate();
    //     var month = date.getMonth() + 1;
    //     var year = date.getFullYear().toString().slice(2);
    //     return day + '-' + month + '-' + year;
    // }
	// if add campaign form
	if ($(".campaign_add").length){

		// repair error in Chrome for calendar
		$('input[type="datetime-local"]').each(function(){
			$(this).attr('type','text');
		});

        // disable require form textarea in add campaign
        document.getElementById("campaign_footer").required = false;
        document.getElementById("campaign_mail_text1").required = false;
        document.getElementById("campaign_mail_text2").required = false;
        document.getElementById("campaign_mail_text3").required = false;
        document.getElementById("campaign_mail_text4").required = false;
        document.getElementById("campaign_mail_text5").required = false;
        loadTiny("#campaign_footer");

        // DateTimePicker
		$('#datetimepicker1').datetimepicker({
		  language: 'pl-PL',
          pickSeconds: false,
          // format:'yyyy-MM-dd HH:mm:ss',
          // minDate: getFormattedDate(new Date())
		});
        // clean format in input after load - set new date without format and timezone
        var forDate = new Date($.now());
        var forDate_day = forDate.getDate();
        var forDate_month = (forDate.getMonth()+1);
        var forDate_hours = forDate.getHours();
        var forDate_minutes = forDate.getMinutes();
        var forDate_seconds = forDate.getSeconds();
        if (forDate_day < 10) { forDate_day = "0" + forDate_day; }
        if (forDate_month < 10) { forDate_month = "0" + forDate_month; }
        if (forDate_hours < 10) { forDate_hours = "0" + forDate_hours; }
        if (forDate_minutes < 10) { forDate_minutes = "0" + forDate_minutes; }
        if (forDate_seconds < 10) { forDate_seconds = "0" + forDate_seconds; }
        forDate = forDate.getFullYear() +"-"+ forDate_month +"-"+ forDate_day +" "+ forDate_hours +":"+ forDate_minutes +":"+ forDate_seconds;

        $('#campaign_mail_date1').val(forDate);
        // $('#campaign_mail_date1').val($('#campaign_mail_date1').val().replace("T", " ").replace("Z", ""));
    }
	// if edit campaign form
	if ($(".campaign_edit").length){

        // disable require form textarea in add campaign
        document.getElementById("campaign_edit_footer").required = false;
        document.getElementById("campaign_edit_mail_text1").required = false;
        document.getElementById("campaign_edit_mail_text2").required = false;
        document.getElementById("campaign_edit_mail_text3").required = false;
        document.getElementById("campaign_edit_mail_text4").required = false;
        document.getElementById("campaign_edit_mail_text5").required = false;
        loadTiny("#campaign_edit_footer");


		// repair error in Chrome for calendar
		$('input[type="datetime-local"]').each(function(){
			$(this).attr('type','text');
		});

        // DateTimePicker
		$('#datetimepicker1').datetimepicker({
		  language: 'pl-PL',
          pickSeconds: false,
          // format:'yyyy-MM-dd HH:mm:ss',
          // minDate: getFormattedDate(new Date())
	  	});

        // clean format in input after load
        $('#campaign_edit_mail_date1').val($('#campaign_edit_mail_date1').val().replace("T", " ").replace("Z", ""));
    }




    /** Validator for Days and Hours **/
    $('.mail_date_dh input').keyup(function(){
        // get value
        var value = parseInt($(this).val());
        // get placeholder with min-max of assert
        var assert_in = $(this).attr('placeholder');
        var assert = assert_in.split('-');
        // get min and max to each vars
        var minVar = parseInt(assert[0]);
        var maxVar = parseInt(assert[1]);

        // get this field error span and clean it
        var errorSpan = $(this).prev('.error');
        errorSpan.hide();
        errorSpan.removeClass("errorDaysHours");
        errorSpan.text("");

        // get button to Submit
        var submit = $('.submitSaveButtom');
        submit.attr('disabled', 'disabled');

        if(value.length === 0 || isNaN(value)){
            // console.log("empty");
            errorSpan.show();
            errorSpan.addClass("errorDaysHours");
            errorSpan.text("Uzupełnij to pole");
        } else if($.isNumeric($(this).val()) == false){
            // console.log("not int");
            errorSpan.show();
            errorSpan.addClass("errorDaysHours");
            errorSpan.text("Błędny format - wpisz liczbę");
        } else {
            if(value < minVar || value > maxVar){
                // console.log("error");
                errorSpan.show();
                errorSpan.addClass("errorDaysHours");
                errorSpan.text("Wartość powinna być z zakresu " + assert_in.toString());
            } else {
                // console.log("ok");
            }
        }

        // if none errors accept submit
        if($('.mails_box .mail_date_dh .errorDaysHours').length <= 0){
            submit.removeAttr('disabled');
        }

    });

    // refresh button
    $('#reloadButton').click(function() {
        location.reload();
    });
	// if refresh button set refresh 30 sec
	if ($("#reloadButton").length){
        setInterval(function(){
            location.reload();
        },30000);
    }


    // if accept text for 1 message -> show calendar
    $(".calendar-before-text").click(function(){
        $("#datetimepicker1").css("cssText", "display: inline-block !important");
    });


    // get button to Submit -> check actuall date and if correct
    // $('.submitSaveButtom').click(function(e){
    //     // hide errors
    //     $('.errorDateCheck').hide();
    //     // create new date and input - check difference
    //     var inputDate = $('#campaign_edit_mail_date1, #campaign_mail_date1').val().split("-");
    //     var restInputDate = inputDate[2].split(" ");
    //     var dateInputDate = restInputDate[1].split(":");
    //     var start = new Date($.now());
    //     var end   = new Date(inputDate[0], inputDate[1]-1, restInputDate[0], dateInputDate[0], dateInputDate[1], dateInputDate[2]);
    //     // var days = (end - start) / (1000 * 60 * 60 * 24);
    //     // var hours = (end - start) / (1000 * 60 * 60);
    //     var mins = (end - start) / (1000 * 60);
    //     // if minutes are less and equel than 0 accept submit
    //     if(Math.round(mins) <= 0){
    //         // cancel action from form and show error
    //         e.preventDefault();
    //         $('.errorDateCheck').show();
    //         $('.errorDateCheck').text("Data ustawiona jest błędnie");
    //     }
    // });

    // after write letters in name of Campaign check it exists in DB
	if($(".campaign_add").length || $(".campaign_edit").length){
        $('#campaign_name, #campaign_edit_name').keyup(function(){
            // block submit and check
            var submit = $('.submitSaveButtom');
            submit.attr('disabled', 'disabled');
            // remove class - clean color
            $('.nameError').removeClass('nameErrorGood');
            // get value of input
            var inpVal = $(this).val();
            // ask DB and show results
            $.ajax({
                type: "post",
                url: "/panel/campaign/ajaxcheckname/"+inpVal.toString(),
                // data: $(this).serialize(),
                data: "",
                processData: false,
                contentType: false,
                cache: false,
                dataType:"json",
            })
            .done(function (data) {
                // if done, check message and show success
                if (typeof data.message !== 'undefined') {
                    var response = $.parseJSON(data.message);
                    if(response == "found"){
                        $('.nameError').html("Taka nazwa już istnieje w bazie danych");
                    } else {
                        $('.nameError').html("Nazwa jest unikalna - poprawnie");
                        $('.nameError').addClass('nameErrorGood');
                        submit.removeAttr('disabled');
                    }
                } else {
                    $('.nameError').html("Problem z procesem sprawdzania nazwy");
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $('.nameError').html("Problem z procesem sprawdzania nazwy");
            });
        });
    }


    // keywords add to mails - which is added for this mail
	if($(".keywordMail_add").length){
		// delete hidden field
		$('#keyword_mail_keywords').remove();
		// get keyword ID
		var keyID = $('.keyId').val();
		var campaignID = $('.campaignId').val();
		// check keywords for this main keyword
		$.ajax({
			type: "post",
			url: "/panel/campaign/"+campaignID+"/keyword/ajaxsubs/"+keyID,
			processData: false,
			contentType: false,
			cache: false,
			dataType:"json",
		})
		.done(function (data) {
			// if done, check message and show success
			if (typeof data.message !== 'undefined') {
				var keywords = $.parseJSON(data.message);
				// add to select this keywords
				var select = $('.keywordsOk');
				var keys = '<option value="" selected="selected">Wybierz słowa kluczowe</option>';
				$.each(keywords, function(key, value) {
					keys += '<option value="'+value['id']+'">'+value['name']+'</option>';
				});
				// add to select
				select.append( keys );
			} else {
				// select.html("Problem z ładowaniem");
			}
		});
	}


    // keywords add to mails - which is added for this mail
	if($(".keywordMail_edit").length){
		// delete hidden field
		$('#keyword_mail_keywords').remove();
		// get keyword ID
		var keyID = $('.keyId').val();
		var mailID = $('.mailId').val();
		var campaignID = $('.campaignId').val();
		// check keywords for this main keyword
		$.ajax({
			type: "post",
			url: "/panel/campaign/"+campaignID+"/keyword/ajaxsubsmore/"+keyID+"/"+mailID,
			processData: false,
			contentType: false,
			cache: false,
			dataType:"json",
		})
		.done(function (data) {
			// if done, check message and show success
			if (typeof data.message !== 'undefined') {
				var data = $.parseJSON(data.message);

				// add to select this keywords
				var select = $('.keywordsOk');
				if(data['selected'].length > 0){
					var keys = '<option value="">Wybierz słowa kluczowe</option>';
				} else {
					var keys = '<option value="" selected="selected">Wybierz słowa kluczowe</option>';
				}
				$.each(data['subs'], function(key, value) {
					if(jQuery.inArray( value['id'], data['selected']) !== -1){
						keys += '<option value="'+value['id']+'" selected="selected">'+value['name']+'</option>';
					} else {
						keys += '<option value="'+value['id']+'">'+value['name']+'</option>';
					}
				});
				// add to select
				select.append( keys );
			} else {
				// select.html("Problem z ładowaniem");
			}
		});
	}








	if($(".copy_keywords").length){
		// run copy keywords methods
		$('.copy_keywords').click(function(){
			if($('.copy_keywords_menu').css('display') == "none"){
				$('.copy_keywords_menu').show();
				$('.select_th').css("display", "table-cell");
				$('.select_checkbox').css("display", "table-cell");
			} else {
				$('.copy_keywords_menu, .select_th, .select_checkbox').hide();
			}
		});

		// select all checkboxes
		var selectedAll = 1;
		$('.select_all').click(function(){
			var items = $('.select_checkbox');
			items.each(function( index, item ) {
				var item_box = $(item).find('input');
				if(selectedAll == 1){
					$(item_box).prop('checked', false);
				} else {
					$(item_box).prop('checked', true);
				}
			});
			// if selected all set 0 or it not 1
			if(selectedAll == 1){ selectedAll = 0; } else { selectedAll = 1; }
		});

		$('.copy_agree').click(function(){
			$(".info").hide();

			var campaignTo = $('.copy_campaignTo').val();

			var selectedItems = [];
			var items = $('.select_checkbox');
			items.each(function( index, item ) {
				var item_box = $(item).find('input');
				if($(item_box).prop('checked')){
					selectedItems.push($(item_box).val());
				}
			});

			var data = {
				'selectedItems': selectedItems,
				'campaignTo': campaignTo,
			}

			$.ajax({
				type: "post",
				url: "/panel/keyword/ajaxcopy",
				data: JSON.stringify(data),
				processData: false,
				contentType: false,
				cache: false,
				dataType:"json",
			})
			.done(function (data) {
				console.log(data);
				// if done, check message and show success
				if (typeof data.message !== 'undefined') {
					var data = $.parseJSON(data.message);

					if(data == 'success'){
						$(".info").html("Udało się wykonać kopię");
						$(".info").show();
					} else {
						$(".info").html("Nie udało się wykonać kopii");
						$(".info").show();
					}

				} else {
					// select.html("Problem z ładowaniem");
				}
			});
		});
	}



});
