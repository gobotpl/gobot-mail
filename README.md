gobot-mail
==========

A Symfony project created on April 23, 2018, 8:43 am.



Uruchomienie na localu:

- zaciągasz GIT'em na komputer
- uruchamiasz w katalogu projektu u siebie: composer install
- update bazy u siebie: php bin/console doctrine:schema:update --force
- uruchomienie: php bin/console server:run
